package com.lukesoft.teparisimu;

import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.koushikdutta.async.AsyncServer;
import com.koushikdutta.async.AsyncServerSocket;
import com.koushikdutta.async.AsyncSocket;
import com.koushikdutta.async.ByteBufferList;
import com.koushikdutta.async.DataEmitter;
import com.koushikdutta.async.callback.CompletedCallback;
import com.koushikdutta.async.callback.DataCallback;
import com.koushikdutta.async.callback.ListenCallback;

import java.util.Locale;
import java.util.Random;


import static com.koushikdutta.async.AsyncServer.LOGTAG;

public class MainActivity extends AppCompatActivity {

    String TAG = "TepariSimulator";

    int TCP_SERVER_PORT = 2000;
    AsyncSocket asyncClient;
    AsyncServer server;

    TextView tcpStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tcpStatus = (TextView) findViewById(R.id.tcp_status);

        setupTcp();
        setupBluetooth();
    }


    void setupTcp() {
        findViewById(R.id.tcp_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        createTcpServer();
    }

    void createTcpServer() {
        server = new AsyncServer();
        server.listen(null, TCP_SERVER_PORT, new ListenCallback() {
            @Override
            public void onAccepted(AsyncSocket socket) {

                updateTcpStatus("Connected");

                if (asyncClient != null) {
                    asyncClient.close();
                }

                asyncClient = socket;

                sendTcpData("*HELLO*");

                asyncClient.setDataCallback(new DataCallback() {
                    @Override
                    public void onDataAvailable(DataEmitter emitter, ByteBufferList bb) {

                        final byte[] byteCmd = bb.getAllByteArray();
                        final String cmd = new String(byteCmd);

                        Log.i(TAG, "Data received: " + cmd);

                        if (byteCmd != null && byteCmd.length > 0) {
                            if (byteCmd[0] == (byte) 0xA6) {
                                String response = "Farm1,Farm2,Farm3,Farm4,#  3,#  6,# 10,#143,Farm5,Farm6,#  9,#  4,";
                                sendTcpData(response);
                            }
                        }
                    }
                });

                asyncClient.setClosedCallback(new CompletedCallback() {
                    @Override
                    public void onCompleted(Exception ex) {
                        updateTcpStatus("Listening");
                        asyncClient = null;
                        Log.i(LOGTAG, "Client socket closed");
                    }
                });
            }

            @Override
            public void onListening(AsyncServerSocket socket) {
                updateTcpStatus("Listening");
            }

            @Override
            public void onCompleted(Exception ex) {
                updateTcpStatus("Closed");
            }
        });
    }

    void updateTcpStatus(final String status) {
        new Handler(getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                tcpStatus.setText(status);
            }
        });
    }

    public void sendTcpData(String message) {
        asyncClient.write(new ByteBufferList(message.getBytes()));
        Log.i(LOGTAG, "Data sent: " + message);
    }

    void setupBluetooth() {

//        findViewById(R.id.btn_send_draw).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Random random = new Random(System.currentTimeMillis());
//                int w1 = random.nextInt(999);
//                int w2 = random.nextInt(10);
//                String out = "[" + w1 + "." + w2 + "]";
////                weightServer.sendData(out.getBytes());
//            }
//        });
//
//        findViewById(R.id.btn_send_final).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Random random = new Random(System.currentTimeMillis());
//                String w1 = "" + random.nextInt(9999);
//                int w2 = random.nextInt(10);
//
//                String out = "<WoL";
//                for (int i = 0; i < 4 - w1.length(); i++) {
//                    out = out + "0";
//                }
//
//                out = out + w1 + "." + w2 + ">";
//
////                weightServer.sendData("<".getBytes());
//            }
//        });
//
//        // Trutest
//        findViewById(R.id.btn_send_trutest).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Random random = new Random(System.currentTimeMillis());
//                int w1 = random.nextInt(999);
//                int w2 = random.nextInt(10);
//                String out = "[U" + w1 + "." + w2 + "]";
////                weightServer.sendData(out.getBytes());
//            }
//        });
//
//        findViewById(R.id.btn_send_trutest_final).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Random random = new Random(System.currentTimeMillis());
//                int w1 = random.nextInt(999);
//                int w2 = random.nextInt(10);
//                String out = "[" + w1 + "." + w2 + "]";
////                weightServer.sendData(out.getBytes());
//            }
//        });
    }

    void sleep() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    void showMessage(String message) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(message);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });

            builder.create().show();
        } catch (Exception e) {
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
