package com.tepari.iscale;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.tepari.tpc.R;

public class Fragment_Preference extends PreferenceFragment {

    public static final String FRAGMENT_ID = "fragment_preference";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        Preference ipAddressPreference = findPreference("ipAddress");
        ipAddressPreference.setOnPreferenceChangeListener(onPreferenceChangeListener);

        Preference portNumberPreference = findPreference("portNumber");
        portNumberPreference.setOnPreferenceChangeListener(onPreferenceChangeListener);

        Preference timeoutPreference = findPreference("timeout");
        timeoutPreference.setOnPreferenceChangeListener(onPreferenceChangeListener);

        Preference fileFormatPreference = findPreference("fileFormat");
        fileFormatPreference.setOnPreferenceChangeListener(onPreferenceChangeListener);

        Preference nonEidPreference = findPreference("removeNonEid");
        nonEidPreference.setOnPreferenceChangeListener(onPreferenceChangeListener);

        Preference delimiterPreference = findPreference("csvDelimiter");
        nonEidPreference.setOnPreferenceChangeListener(onPreferenceChangeListener);

        Preference unitPreference = findPreference("unit");
        unitPreference.setOnPreferenceChangeListener(onPreferenceChangeListener);

        Preference eidPreference = findPreference("eidFormat");
        eidPreference.setOnPreferenceChangeListener(onPreferenceChangeListener);
    }

    private Preference.OnPreferenceChangeListener onPreferenceChangeListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            String prefKey = preference.getKey();
            switch (prefKey) {
                case "ipAddress":
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Fragment_Preference.this.getActivity());
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean("reconnect", true);
                    editor.commit();
                    break;
                case "portNumber":
                    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Fragment_Preference.this.getActivity());
                    editor = sharedPreferences.edit();
                    editor.putBoolean("reconnect", true);
                    editor.commit();
                    break;
                case "timeout":
                    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Fragment_Preference.this.getActivity());
                    editor = sharedPreferences.edit();
                    editor.putBoolean("reconnect", true);
                    editor.commit();
                    break;
                case "fileFormat":
                    break;
                case "removeNonEid":
                    break;
                case "csvDelimiter":
                    break;
                case "unit":
                    break;
                case "eid_format":
                    break;
            }
            return true;
        }
    };
}