package com.tepari.iscale;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.FileProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tepari.iscale.Helper.Constants;
import com.tepari.iscale.Helper.Files_Content;
import com.tepari.iscale.database.TepariDatabase;
import com.tepari.tpc.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Fragment_CSV extends Fragment {

    public static final String FRAGMENT_ID = "fragment_csv";

    ViewGroup contn;
    ListView list_csv_title;
    ListView list_csv;

    private TepariDatabase tepariDatabase;

    private List<Files_Content> listContentFile = new ArrayList<>();

    public int getDataId(){
        return ((Activity_Parent) getActivity()).dataId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_csv, container, false);

        tepariDatabase = new TepariDatabase(getActivity());

        setHasOptionsMenu(true);
        contn = container;

        String filename = ((Activity_Parent) getActivity()).csv_filename;
        ((Activity_Parent) getActivity()).actionBar.setTitle(filename);

        list_csv_title = (ListView) rootView.findViewById(R.id.list_csv_title);
        list_csv = (ListView) rootView.findViewById(R.id.list_csv);

        TextView btn_email = (TextView) rootView.findViewById(R.id.btn_email);
        btn_email.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                emailFiles(((Activity_Parent) getActivity()).csv_filename);
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        display_list();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.menu_csv, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_rename_file:
                alertRenameFile(contn);
                return true;
            case R.id.menu_delete_single:
                confirmDelete(((Activity_Parent) getActivity()).csv_filename);
                return true;
            case R.id.menu_open_external:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                File externalFile = new File(((Activity_Parent) getActivity()).file_location + ((Activity_Parent) getActivity()).csv_filename);
                Uri path;
                //    Make sure we're running on O or higher to use FileProvider APIs
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    path = FileProvider.getUriForFile((Activity_Parent) getActivity(), "com.tepari.tpc.provider", externalFile);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                else{
                    path = Uri.fromFile(externalFile);
                }
                intent.setDataAndType(path, "application/vnd.ms-excel");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getActivity(), getString(R.string.notify_spreadsheets), Toast.LENGTH_SHORT).show();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Display either csv or minda list
    private void display_list() {
        listContentFile = ((Activity_Parent) getActivity()).readContentFile();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String fileType = prefs.getString("fileFormat", Constants.FILE_TYPE_CSV);
        String unitType = prefs.getString("unit", Constants.UNIT_TYPE_KG);

        //Check Stream Data
        if(listContentFile != null && !listContentFile.isEmpty() && listContentFile.get(0).getFileFormat().equals(Constants.FILE_TYPE_STREAM)) {
            display_list_stream(listContentFile);
            return;
        }

        if (fileType.equals(Constants.FILE_TYPE_CSV)) {
            if (unitType.equals(Constants.UNIT_TYPE_KG)) {
                display_list_csv_kg(listContentFile);
            }
            else if (unitType.equals(Constants.UNIT_TYPE_LB)) {
                display_list_csv_lb(listContentFile);
            }
        } else {
            display_list_minda(listContentFile);
        }
    }

    // Display csv list in kg
    private void display_list_csv_kg(List<Files_Content> lst_content) {
        ArrayList<Files_Content> fst = new ArrayList<>();
        String[] rec = (getString(R.string.csv_title_bar_view).replaceAll("\n", "")).split(",");
        // Save according to the data structure
        Files_Content csv;
        if (getDataId() == 1) {
            csv = new Files_Content(Constants.FILE_TYPE_CSV, "", rec[0], rec[1], rec[2], "", rec[4], rec[5], rec[6], rec[7], rec[8], rec[9], rec[10], rec[11], "", rec[21], rec[13], "", rec[15], rec[20]);
        }
        else {
            csv = new Files_Content(Constants.FILE_TYPE_CSV, "", rec[0], rec[1], rec[2], rec[3], rec[4], rec[5], rec[6], rec[7], rec[8], rec[9], rec[10], rec[11], "", rec[21], rec[13], "", rec[15], rec[20]);
        }
        csv.setWeight_gain("WEIGHT GAIN KG");
        csv.setWeight_gain_lb("");
        csv.setWeight_gain_total_kg("WEIGHT GAIN TOTAL KG");
        csv.setWeight_gain_total_lb("");
        fst.add(csv);
        ListAdapter_Csv dataAdapter;
        dataAdapter = new ListAdapter_Csv(getActivity(), R.layout.list_csv, fst, true);
        list_csv_title.setAdapter(dataAdapter);
        dataAdapter = new ListAdapter_Csv(getActivity(), R.layout.list_csv, lst_content, false);
        list_csv.setAdapter(dataAdapter);
    }

    // Display csv list in lb
    private void display_list_csv_lb(List<Files_Content> lst_content) {
        ArrayList<Files_Content> fst = new ArrayList<>();
        String[] rec = (getString(R.string.csv_title_bar_view).replaceAll("\n", "")).split(",");
        // Save according to the data structure
        Files_Content csv;
        if (getDataId() == 1) {
            csv = new Files_Content(Constants.FILE_TYPE_CSV, "", rec[0], rec[1], rec[2], "", rec[4], rec[5], rec[6], rec[7], rec[8], rec[9], rec[10], "", rec[12], rec[21],"", rec[14], rec[15], rec[20]);
        }
        else {
            csv = new Files_Content(Constants.FILE_TYPE_CSV, "", rec[0], rec[1], rec[2], rec[3], rec[4], rec[5], rec[6], rec[7], rec[8], rec[9], rec[10], "", rec[12], rec[21],"", rec[14], rec[15], rec[20]);
        }
        csv.setWeight_gain("");
        csv.setWeight_gain_lb("WEIGHT GAIN LB");
        csv.setWeight_gain_total_kg("");
        csv.setWeight_gain_total_lb("WEIGHT GAIN TOTAL LB");
        fst.add(csv);
        ListAdapter_Csv dataAdapter;
        dataAdapter = new ListAdapter_Csv(getActivity(), R.layout.list_csv, fst, true);
        list_csv_title.setAdapter(dataAdapter);
        dataAdapter = new ListAdapter_Csv(getActivity(), R.layout.list_csv, lst_content, false);
        list_csv.setAdapter(dataAdapter);
    }

    // Display minda list
    private void display_list_minda(List<Files_Content> lst_content) {
        ArrayList<Files_Content> fst = new ArrayList<>();
        String[] rec = (getString(R.string.csv_title_minda_bar).replaceAll("\n", "")).split(",");
        Files_Content minda = new Files_Content(Constants.FILE_TYPE_MINDA, "", "", rec[0], rec[2], "", "", "", "", "", "", "", "", rec[3], "", rec[1], "", "", "", "");
        fst.add(minda);
        ListAdapter_Minda dataAdapter;
        dataAdapter = new ListAdapter_Minda(getActivity(), R.layout.list_minda, fst, true);
        list_csv_title.setAdapter(dataAdapter);
        dataAdapter = new ListAdapter_Minda(getActivity(), R.layout.list_minda, lst_content, false);
        list_csv.setAdapter(dataAdapter);
    }

    private void display_list_stream(List<Files_Content> lst_content) {
        ArrayList<String[]> fst = new ArrayList<>();
        String[] header = lst_content.get(0).getListColumnName().split(",");
        fst.add(header);

        ListAdapter_Stream dataAdapter;
        dataAdapter = new ListAdapter_Stream(getActivity(), R.layout.list_upload, fst, true);
        list_csv_title.setAdapter(dataAdapter);

        //For Value
        ArrayList<String[]> entry = new ArrayList<>();
        for(Files_Content temp : lst_content) {
            String[] value = temp.getListColumnValue().split(",");
            entry.add(value);
        }


        dataAdapter = new ListAdapter_Stream(getActivity(), R.layout.list_upload, entry, false);
        list_csv.setAdapter(dataAdapter);
    }

    // Email selected files
    private void emailFiles(String filename) {
        ArrayList<Uri> uris = new ArrayList<>();
        String exportFile = ((Activity_Parent) getActivity()).generateEmailExportFileContent(filename, listContentFile);

        Intent email = new Intent(Intent.ACTION_SEND_MULTIPLE);
        //    Make sure we're running on O or higher to use FileProvider APIs
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            uris.add(FileProvider.getUriForFile((Activity_Parent) getActivity(), "com.tepari.tpc.provider", new File(((Activity_Parent) getActivity()).exportFileLocation + exportFile)));
            email.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        else{
            uris.add(Uri.fromFile(new File(((Activity_Parent) getActivity()).exportFileLocation + exportFile)));
        }
        email.setType("text/plain");
        email.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        try {
            startActivity(Intent.createChooser(email, getString(R.string.notify_email_choose)));
        } catch (ActivityNotFoundException exception) {
            Toast.makeText(getActivity(), getString(R.string.toast_email_err), Toast.LENGTH_LONG).show();
        }
    }

    // Deletes the currently opened file and returns to ui_records intent
    private void confirmDelete(String filename) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder
                .setTitle(getString(R.string.delete_confirm))
                .setMessage(getString(R.string.s_delete_file_message, filename))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String filename = ((Activity_Parent) getActivity()).csv_filename;
                        File fileToDelete = new File(((Activity_Parent) getActivity()).file_location + filename);
                        //Delete Database
                        tepariDatabase.deleteTepariFile(fileToDelete.getName());

                        boolean deleted = fileToDelete.delete();
                        if (deleted) {
                            Toast.makeText(getActivity(), getString(R.string.s_filename_delete, filename), Toast.LENGTH_SHORT).show();
                            ((Activity_Parent) getActivity()).getFiles();
                            getActivity().getFragmentManager().popBackStack("fragment_summary", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.s_unable_to_delete_file, filename), Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    // Alert to ask for file rename
    private void alertRenameFile(final ViewGroup contn) {
        LayoutInflater li = LayoutInflater.from(getActivity());
        View promptsView = li.inflate(R.layout.alert_rename, contn, false); // set alert_rename.xml to alert dialog builder
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptsView);
        final EditText userInput = (EditText) promptsView.findViewById(R.id.editTextDialogUserInput);
        alertDialogBuilder
                .setTitle(getString(R.string.rename_file))
                        .setMessage(getString(R.string.alert_rename_as))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.rename), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (((Activity_Parent) getActivity()).csv_filename.contains(".csv")) {
                                    String newFilename = userInput.getText().toString() + ".csv";
                                    ((Activity_Parent) getActivity()).checkLocalExistence(newFilename);
                                    if (((Activity_Parent) getActivity()).checkLocalExistence(newFilename)) {
                                        Toast.makeText(getActivity(), getString(R.string.toast_file_exist), Toast.LENGTH_LONG).show();
                                    } else {
                                        renameFile(newFilename);
                                    }
                                } else if (((Activity_Parent) getActivity()).csv_filename.contains(".minda")) {
                                    String newFilename = userInput.getText().toString() + ".minda";
                                    ((Activity_Parent) getActivity()).checkLocalExistence(newFilename);
                                    if (((Activity_Parent) getActivity()).checkLocalExistence(newFilename)) {
                                        Toast.makeText(getActivity(), getString(R.string.toast_file_exist), Toast.LENGTH_LONG).show();
                                    } else {
                                        renameFile(newFilename);
                                    }
                                } else {
                                    Toast.makeText(getActivity(), getString(R.string.toast_invalid_name), Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        // create alert dialog and show it
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    // Rename file
    private void renameFile(String newFilename) {
        File from = new File(((Activity_Parent) getActivity()).file_location, ((Activity_Parent) getActivity()).csv_filename);
        File to = new File(((Activity_Parent) getActivity()).file_location, newFilename);
        if (from.renameTo(to)) {
            ((Activity_Parent) getActivity()).csv_filename = newFilename;
            ((Activity_Parent) getActivity()).actionBar.setTitle(((Activity_Parent) getActivity()).csv_filename);
        } else {
            Toast.makeText(getActivity(), getString(R.string.toast_error_rename_file), Toast.LENGTH_LONG).show();
        }
    }

    private class ListAdapter_Csv extends ArrayAdapter<Files_Content> {
        int resource;
        List<Files_Content> fileList;
        boolean title_bar;
        LayoutInflater inflater;

        public ListAdapter_Csv(Context context, int resource, List<Files_Content> fileList, boolean title_bar) {
            super(context, resource, fileList);
            this.resource = resource;
            this.fileList = fileList;
            this.title_bar = title_bar;
            inflater = LayoutInflater.from(getActivity());
        }

        private class ViewHolder {
            LinearLayout csv_bkgnd;
            TextView num;
            TextView filename;
            TextView eid;
            TextView vid;
            TextView pid;
            TextView health1_type;
            TextView health1_dose;
            TextView health2_type;
            TextView health2_dose;
            TextView code1;
            TextView code2;
            TextView code3;
            TextView weightkg;
            TextView weightlb;
            TextView date;
            TextView prev_weightkg;
            TextView prev_weightlb;
            TextView gain;
            TextView draft;
            TextView weight_gain;
            TextView weight_gain_lb;
            TextView weight_gain_total_kg;
            TextView weight_gain_total_lb;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            final ViewHolder holder;
            Files_Content file = fileList.get(position);
            if (convertView == null) {
                holder = new ViewHolder();
                view = inflater.inflate(resource, parent, false);
                holder.csv_bkgnd = (LinearLayout) view.findViewById(R.id.csv_bkgnd);
                holder.num = (TextView) view.findViewById(R.id.csv_num);
                holder.filename = (TextView) view.findViewById(R.id.csv_00);
                holder.eid = (TextView) view.findViewById(R.id.csv_01);
                holder.vid = (TextView) view.findViewById(R.id.csv_02);
                holder.pid = (TextView) view.findViewById(R.id.csv_03);
                holder.health1_type = (TextView) view.findViewById(R.id.csv_04);
                holder.health1_dose = (TextView) view.findViewById(R.id.csv_05);
                holder.health2_type = (TextView) view.findViewById(R.id.csv_06);
                holder.health2_dose = (TextView) view.findViewById(R.id.csv_07);
                holder.code1 = (TextView) view.findViewById(R.id.csv_08);
                holder.code2 = (TextView) view.findViewById(R.id.csv_09);
                holder.code3 = (TextView) view.findViewById(R.id.csv_10);
                holder.weightkg = (TextView) view.findViewById(R.id.csv_11);
                holder.weightlb = (TextView) view.findViewById(R.id.csv_12);

                holder.prev_weightkg = (TextView) view.findViewById(R.id.csv_13);
                holder.prev_weightlb = (TextView) view.findViewById(R.id.csv_14);
                holder.gain = (TextView) view.findViewById(R.id.csv_15);

                holder.weight_gain = (TextView) view.findViewById(R.id.csv_16);
                holder.weight_gain_lb = (TextView) view.findViewById(R.id.csv_17);
                holder.draft = (TextView) view.findViewById(R.id.csv_18);
                holder.date = (TextView) view.findViewById(R.id.csv_19);

                holder.weight_gain_total_kg = (TextView) view.findViewById(R.id.csv_20);
                holder.weight_gain_total_lb = (TextView) view.findViewById(R.id.csv_21);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            if (title_bar) {
                holder.csv_bkgnd.setBackgroundColor(getResources().getColor(R.color.tepari_blue));
                int col = getResources().getColor(R.color.tepari_white);
                holder.filename.setTextColor(col);
                holder.eid.setTextColor(col);
                holder.vid.setTextColor(col);
                holder.pid.setTextColor(col);
                holder.health1_type.setTextColor(col);
                holder.health1_dose.setTextColor(col);
                holder.health2_type.setTextColor(col);
                holder.health2_dose.setTextColor(col);
                holder.code1.setTextColor(col);
                holder.code2.setTextColor(col);
                holder.code3.setTextColor(col);
                holder.weightkg.setTextColor(col);
                holder.weightlb.setTextColor(col);
                holder.date.setTextColor(col);
                holder.prev_weightkg.setTextColor(col);
                holder.prev_weightlb.setTextColor(col);
                holder.gain.setTextColor(col);
                holder.draft.setTextColor(col);
                holder.weight_gain.setTextColor(col);
                holder.weight_gain_lb.setTextColor(col);

                holder.weight_gain_total_kg.setTextColor(col);
                holder.weight_gain_total_lb.setTextColor(col);
            } else {
                int col = (Integer.parseInt(file.getNum()) % 2 == 1) ? getResources().getColor(R.color.tepari_csv_grey) : getResources().getColor(R.color.tepari_white);
                holder.csv_bkgnd.setBackgroundColor(col);
            }

            holder.num.setText(file.getNum());
            holder.filename.setText(file.getFilename());
            holder.eid.setText(file.getEid());
            holder.vid.setText(file.getVid());
            holder.pid.setText(file.getPid());
            holder.health1_type.setText(file.getHealth1_type());
            holder.health1_dose.setText(file.getHealth1_dose());
            holder.health2_type.setText(file.getHealth2_type());
            holder.health2_dose.setText(file.getHealth2_dose());
            holder.code1.setText(file.getCode1());
            holder.code2.setText(file.getCode2());
            holder.code3.setText(file.getCode3());
            holder.weightkg.setText(file.getWeightkg());
            holder.weightlb.setText(file.getWeightlb());
            holder.date.setText(file.getDate());
            holder.prev_weightkg.setText(file.getPrev_weightkg());
            holder.prev_weightlb.setText(file.getPrev_weightlb());
            holder.gain.setText(file.getGain());
            holder.draft.setText(file.getDraft());
            holder.weight_gain.setText(file.getWeight_gain());
            holder.weight_gain_lb.setText(file.getWeight_gain_lb());

            holder.weight_gain_total_kg.setText(file.getWeight_gain_total_kg());
            holder.weight_gain_total_lb.setText(file.getWeight_gain_total_lb());
            return view;
        }
    }

    private class ListAdapter_Minda extends ArrayAdapter<Files_Content> {
        int resource;
        List<Files_Content> fileList;
        boolean title_bar;
        LayoutInflater inflater;

        public ListAdapter_Minda(Context context, int resource, List<Files_Content> fileList, boolean title_bar) {
            super(context, resource, fileList);
            this.resource = resource;
            this.fileList = fileList;
            this.title_bar = title_bar;
            inflater = LayoutInflater.from(getActivity());
        }

        private class ViewHolder {
            LinearLayout csv_bkgnd;
            TextView num;
            TextView eid;
            TextView date;
            TextView vid;
            TextView weight;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            final ViewHolder holder;
            Files_Content file = fileList.get(position);
            if (convertView == null) {
                holder = new ViewHolder();
                view = inflater.inflate(resource, parent, false);
                holder.csv_bkgnd = (LinearLayout) view.findViewById(R.id.minda_bkgnd);
                holder.num = (TextView) view.findViewById(R.id.minda_num);
                holder.eid = (TextView) view.findViewById(R.id.minda_00);
                holder.date = (TextView) view.findViewById(R.id.minda_01);
                holder.vid = (TextView) view.findViewById(R.id.minda_02);
                holder.weight = (TextView) view.findViewById(R.id.minda_03);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            if (title_bar) {
                holder.csv_bkgnd.setBackgroundColor(getResources().getColor(R.color.tepari_blue));
                int col = getResources().getColor(R.color.tepari_white);
                holder.eid.setTextColor(col);
                holder.date.setTextColor(col);
                holder.vid.setTextColor(col);
                holder.weight.setTextColor(col);
            } else {
                int col = (Integer.parseInt(file.getNum()) % 2 == 1) ? getResources().getColor(R.color.tepari_csv_grey) : getResources().getColor(R.color.tepari_white);
                holder.csv_bkgnd.setBackgroundColor(col);
            }

            holder.num.setText(file.getNum());
            holder.eid.setText(file.getEid());
            holder.date.setText(file.getDate());
            holder.vid.setText(file.getVid());
            holder.weight.setText(file.getWeightkg());
            return view;
        }
    }


    private class ListAdapter_Stream extends ArrayAdapter<String[]> {
        int resource;
        List<String[]> fileList;
        boolean title_bar;
        LayoutInflater inflater;

        public ListAdapter_Stream(Context context, int resource, List<String[]> fileList, boolean title_bar) {
            super(context, resource, fileList);
            this.resource = resource;
            this.fileList = fileList;
            this.title_bar = title_bar;
            inflater = LayoutInflater.from(getActivity());
        }

        private class ViewHolder {
            LinearLayout csv_bkgnd;

            TextView field0;
            TextView field1;
            TextView field2;
            TextView field3;
            TextView field4;
            TextView field5;
            TextView field6;
            TextView field7;
            TextView field8;
            TextView field9;
            TextView field10;
            TextView field11;

            TextView field12;
            TextView field13;


        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            final ViewHolder holder;
            String[] file = fileList.get(position);
            if (convertView == null) {
                holder = new ViewHolder();
                view = inflater.inflate(resource, parent, false);
                holder.csv_bkgnd = (LinearLayout) view.findViewById(R.id.csv_bkgnd);

                holder.field0 = (TextView) view.findViewById(R.id.csv_00);
                holder.field1 = (TextView) view.findViewById(R.id.csv_01);
                holder.field2 = (TextView) view.findViewById(R.id.csv_02);
                holder.field3 = (TextView) view.findViewById(R.id.csv_03);
                holder.field4 = (TextView) view.findViewById(R.id.csv_04);
                holder.field5 = (TextView) view.findViewById(R.id.csv_05);
                holder.field6 = (TextView) view.findViewById(R.id.csv_06);
                holder.field7 = (TextView) view.findViewById(R.id.csv_07);
                holder.field8 = (TextView) view.findViewById(R.id.csv_08);
                holder.field9 = (TextView) view.findViewById(R.id.csv_09);
                holder.field10 = (TextView) view.findViewById(R.id.csv_10);
                holder.field11 = (TextView) view.findViewById(R.id.csv_11);
                holder.field12 = (TextView) view.findViewById(R.id.csv_12);
                holder.field13 = (TextView) view.findViewById(R.id.csv_13);

                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            if (title_bar) {
                holder.csv_bkgnd.setBackgroundColor(getResources().getColor(R.color.tepari_blue));
                int col = getResources().getColor(R.color.tepari_white);
                holder.field0.setTextColor(col);
                holder.field1.setTextColor(col);
                holder.field2.setTextColor(col);
                holder.field3.setTextColor(col);
                holder.field4.setTextColor(col);
                holder.field5.setTextColor(col);
                holder.field6.setTextColor(col);
                holder.field7.setTextColor(col);
                holder.field8.setTextColor(col);
                holder.field9.setTextColor(col);
                holder.field10.setTextColor(col);
                holder.field11.setTextColor(col);
                holder.field12.setTextColor(col);
                holder.field13.setTextColor(col);

            } else {
                int col = (position % 2 == 1) ? getResources().getColor(R.color.tepari_csv_grey) : getResources().getColor(R.color.tepari_white);
                holder.csv_bkgnd.setBackgroundColor(col);
            }

            if(file != null) {
                if(file.length > 0) {
                    holder.field0.setText(file[0]);
                    holder.field0.setVisibility(View.VISIBLE);
                }
                if(file.length > 1) {
                    holder.field1.setText(file[1]);
                    holder.field1.setVisibility(View.VISIBLE);
                }
                if(file.length > 2) {
                    holder.field2.setText(file[2]);
                    holder.field2.setVisibility(View.VISIBLE);
                }
                if(file.length > 3) {
                    holder.field3.setText(file[3]);
                    holder.field3.setVisibility(View.VISIBLE);
                }
                if(file.length > 4) {
                    holder.field4.setText(file[4]);
                    holder.field4.setVisibility(View.VISIBLE);
                }
                if(file.length > 5) {
                    holder.field5.setText(file[5]);
                    holder.field5.setVisibility(View.VISIBLE);
                }
                if(file.length > 6) {
                    holder.field6.setText(file[6]);
                    holder.field6.setVisibility(View.VISIBLE);
                }
                if(file.length > 7) {
                    holder.field7.setText(file[7]);
                    holder.field7.setVisibility(View.VISIBLE);
                }
                if(file.length > 8) {
                    holder.field8.setText(file[8]);
                    holder.field8.setVisibility(View.VISIBLE);
                }
                if(file.length > 9) {
                    holder.field9.setText(file[9]);
                    holder.field9.setVisibility(View.VISIBLE);
                }
                if(file.length > 10) {
                    holder.field10.setText(file[10]);
                    holder.field10.setVisibility(View.VISIBLE);
                }

                if(file.length > 11) {
                    holder.field11.setText(file[11]);
                    holder.field11.setVisibility(View.VISIBLE);
                }

                if(file.length > 12) {
                    holder.field12.setText(file[12]);
                    holder.field12.setVisibility(View.VISIBLE);
                }
                if(file.length > 13) {
                    holder.field13.setText(file[13]);
                    holder.field13.setVisibility(View.VISIBLE);
                }
            }


            return view;
        }
    }
}