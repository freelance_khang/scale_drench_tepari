package com.tepari.iscale;

import android.content.Context;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

/**
 * Created by 2fDesign1 on 25/11/2016.
 */

public class FileSettings {

    public boolean FirmwareAvailable(){
        String path = "EGFirmware.plist";
        if(path.equals("")) // Check if file exists
            return TRUE;
        return FALSE;
    }

    // Get firmware version
    public String getFirmwareVersion(Context context){
        String path = "EGFirmware.plist";
        InputStream is = null;
        ByteArrayOutputStream bos = null;

        try {   // Open EGFirmware.plist to read
            is = context.openFileInput(path);
            bos = new ByteArrayOutputStream();

            // Read in blocks of 1kB and store in an array
            byte[] bytes = new byte[26];
            int bytesRead;
            Log.d("getFirmwareVersion", "start reading");
            while ((bytesRead = is.read(bytes)) != -1) {
                bos.write(bytes, 0, bytesRead);
            }
            Log.d("getFirmwareVersion", bos.toString());
            return bos.toString();

        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
        finally {
            try {
                is.close();
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // Get firmware B0
    public byte[] getFirmwareFileB0(Context context){
        String filename = "EG_B0.bin";
        InputStream is = null;
        ByteArrayOutputStream bos = null;

        try {   // Open EG_B0.bin to read
            is = context.openFileInput(filename);
            bos = new ByteArrayOutputStream();

            // Read in blocks of 1kB and store in an array
            byte[] bytes = new byte[1024];
            int bytesRead;
            while ((bytesRead = is.read(bytes)) != -1) {
                bos.write(bytes, 0, bytesRead);
            }
            return bos.toByteArray();

        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
        finally {
            try {
                is.close();
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // Get firmware B1
    public byte[] getFirmwareFileB1(Context context){
     //   String filename = "EG_B1.bin";
        String filename = "UPGRADE_B1.bin";
        InputStream is = null;
        ByteArrayOutputStream bos = null;

        try {   // Open UPGRADE_B1.bin to read
            is = context.openFileInput(filename);
            bos = new ByteArrayOutputStream();

            // Read in blocks of 1kB and store in an array
            byte[] bytes = new byte[1024];
            int bytesRead;
            while ((bytesRead = is.read(bytes)) != -1) {
                bos.write(bytes, 0, bytesRead);
            }
            return bos.toByteArray();

        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
        finally {
            try {
                is.close();
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    // Get firmware B2
    public byte[] getFirmwareFileB2(Context context){

        String filename = "UPGRADE_B2.bin";
        InputStream is = null;
        ByteArrayOutputStream bos = null;

        try {   // Open UPGRADE_B2.bin to read
            is = context.openFileInput(filename);
            bos = new ByteArrayOutputStream();

            // Read in blocks of 1kB and store in an array
            byte[] bytes = new byte[1024];
            int bytesRead;
            while ((bytesRead = is.read(bytes)) != -1) {
                bos.write(bytes, 0, bytesRead);
            }
            return bos.toByteArray();

        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
        finally {
            try {
                is.close();
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void saveFirmwareFileB0(byte[] mdata, Context context){
        String filename = "EG_B0.bin";
        FileOutputStream outputStream = null;

        // Write to file
        try{
            outputStream = context.openFileOutput(filename, MODE_PRIVATE);
            Log.d("FileSetting", context.getFilesDir().toString());
            outputStream.write(mdata);
            outputStream.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void saveFirmwareFileB1(byte[] mdata, Context context){
        String filename = "UPGRADE_B1.bin";
        FileOutputStream outputStream = null;

        // Write to file
        try{
            outputStream = context.openFileOutput(filename, MODE_PRIVATE);
            Log.d("FileSetting", context.getFilesDir().toString());
            outputStream.write(mdata);
            outputStream.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void saveFirmwareFileB2(byte[] mdata, Context context){
        String filename = "UPGRADE_B2.bin";
        FileOutputStream outputStream = null;

        // Write to file
        try{
            outputStream = context.openFileOutput(filename, MODE_PRIVATE);
            Log.d("FileSetting", context.getFilesDir().toString());
            outputStream.write(mdata);
            outputStream.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public String saveFirmwareInfo(InputStream is, Context context){
        String filename = "EGFirmware.plist";
        FileOutputStream outputStream = null;

        // Instantiate the parser
        String plist = null;
        try {
            // Extract firmware version number
            plist = xmlParser(is);

            // Write to file
            outputStream = context.openFileOutput(filename, MODE_PRIVATE);
            Log.d("FileSetting", context.getFilesDir().toString());
            outputStream.write(plist.getBytes());   // convert the data into bytes to save it to file
            outputStream.close();
            return plist;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private String xmlParser(InputStream in) throws XmlPullParserException, IOException {
        try{
            XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
            XmlPullParser myParser = xmlFactoryObject.newPullParser();

            myParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            myParser.setInput(in, null);
            String result = parseXML(myParser);
            Log.d("xmlParser", "The result is: " + result);
            return result;
        } finally {
            in.close();
        }
    }

    private String parseXML(XmlPullParser myParser) {

        int event;
        String text = null;
        String result = new String();

        try {
            event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = myParser.getName();

                switch (event) {
                    case XmlPullParser.START_TAG:
                        break;
                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        //get firmware version number
                        if (name.equals("string")) {
                            result = text;
                        }
                        break;
                }
                event = myParser.next();
            }
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
