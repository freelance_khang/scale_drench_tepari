package com.tepari.iscale;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import android.view.View;

import com.tepari.tpc.R;
/**
 * First page after splash screen
 * Created by 2fDesign1 on 14/11/2016.
 */
public class Activity_Home extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homeview);
    }

    // Opens Activity_Parent class when Weigh Scale Connect button is pressed
    public void weighScale(View view) {
        startActivity(new Intent(Activity_Home.this, Activity_Parent.class));
    }

    public void drenchGun(View view) {
        startActivity(new Intent(Activity_Home.this, Activity_DrenchGun.class));

    }

/*    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            this.finish();
        } else {
            getFragmentManager().popBackStack();
        }
    }*/
}
