package com.tepari.iscale;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.tepari.tpc.R;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class Activity_Splash extends Activity {

    private final static int splash_delay = 5000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);

        TextView txtVersion = (TextView) findViewById(R.id.txtVersion);
        String versionName = "1.0";
        try {
            versionName = getPackageManager()
                    .getPackageInfo(getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        txtVersion.setText(getString(R.string.version_copyright, versionName));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
         //       startActivity(new Intent(Activity_Splash.this, Activity_Parent.class));
                startActivity(new Intent(Activity_Splash.this, Activity_Home.class));
                finish();
            }
        }, splash_delay);
    }
}