package com.tepari.iscale;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


import com.tepari.iscale.wifi.ConnectWifiTask;
import com.tepari.iscale.wifi.NetworkHelper;
import com.tepari.tpc.R;

import java.util.ArrayList;
import java.util.List;


/**
 * This Activity appears as a dialog. It lists any paired devices and
 * devices detected in the area after discovery. When a device is chosen
 * by the user, the MAC address of the device is sent back to the parent
 * Activity in the result Intent.
 */
public class Activity_PickWifi extends Activity {
    String TAG = getClass().getSimpleName();

    TextView tvHeader;
    ListView deviceListView;
    View notFoundContainer;

    private ArrayAdapter<String> mDevicesArrayAdapter;
    private List<ScanResult> mResultList;

    private WifiManager mWifiManager;
    WifiInfoReceiver br;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_list);

        initView(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            unregisterReceiver(br);
        }catch (Exception e){}
    }

    protected void initView(Bundle savedInstanceState) {
        setTitle(R.string.pick_wifi_title);

        tvHeader = findViewById(R.id.tv_header);
        deviceListView = findViewById(R.id.devices);
        notFoundContainer = findViewById(R.id.not_found_container);
        View btnRetry = findViewById(R.id.btn_retry);
        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startScan();
            }
        });

        mDevicesArrayAdapter = new ArrayAdapter<String>(this, R.layout.device_name, R.id.tv_name);
        deviceListView.setAdapter(mDevicesArrayAdapter);
        deviceListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                connectToDeviceWifi(mResultList.get(i).SSID);
            }
        });

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if(checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 87);
            } else {
                startScan();
            }
        } else {
            startScan();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        for (int i = 0; i < grantResults.length; i ++) {
            int result = grantResults[i];
            if (result == PackageManager.PERMISSION_GRANTED) {
                startScan();
            }
        }
    }

    void startScan() {
        showLoading();

        mResultList = null;

        IntentFilter i = new IntentFilter();
        br = new WifiInfoReceiver();

        i.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        i.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        i.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(br, i);

        mWifiManager = (WifiManager) getApplicationContext().getSystemService (Context.WIFI_SERVICE);
        boolean startScanSuccess = mWifiManager.startScan();
        Log.d(TAG, "startScanSuccess: " + startScanSuccess);

    }

    void processWifiScanResult() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService (Context.WIFI_SERVICE);
        mResultList = new ArrayList<>();
        List<ScanResult> scanResults = wifiManager.getScanResults();
        Log.d(TAG, "processWifiScanResult, result size: " + scanResults.size());

        List<String> deviceNameList = new ArrayList<>();
        for(int i = scanResults.size() - 1; i >= 0; i--) {
            ScanResult scanResult = scanResults.get(i);
            Log.d(TAG, "wifi: " + scanResult.SSID + ", isOpenWifi: " + NetworkHelper.isOpenWifi(scanResult.capabilities) + ", " + scanResult.capabilities);
//            if (NetworkHelper.isOpenWifi(scanResult.capabilities))
            {
                mResultList.add(0, scanResult);
                deviceNameList.add(0, scanResult.SSID);
            }
        }

        updateDeviceList(deviceNameList);
    }

    void updateDeviceList(List<String> deviceNameList) {
        hideLoading();

        mDevicesArrayAdapter.clear();
        mDevicesArrayAdapter.addAll(deviceNameList);
        notFoundContainer.setVisibility(deviceNameList.isEmpty() ? View.VISIBLE : View.GONE);
    }

    void connectToDeviceWifi(String gunSsid) {
        ConnectWifiTask connectWifiTask = new ConnectWifiTask(this);
        connectWifiTask.setListener(new ConnectWifiTask.Listener() {
            @Override
            public void onFinish(boolean connected) {

                hideLoading();

                if (!connected) {
                    showErrorDialog(getString(R.string.connect_wifi_failed));
                }  else {
                    if (!NetworkHelper.isConnectingToScaleWifi()) {
                        showErrorDialog(getString(R.string.wrong_connected_wifi));
                    } else {
                        connectedToScaleWifi();
                    }
                }
            }
        });

        showLoading(getString(R.string.changing_wifi));
        connectWifiTask.execute(gunSsid);
    }

    void connectedToScaleWifi() {
        setResult(RESULT_OK);
        finish();
    }

    private class WifiInfoReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case WifiManager.SCAN_RESULTS_AVAILABLE_ACTION:
                    if (mResultList == null)
                        processWifiScanResult();
                    Log.d(TAG, "Received SCAN_RESULTS_AVAILABLE_ACTION");
                    break;

                case WifiManager.NETWORK_STATE_CHANGED_ACTION:
                    Log.d(TAG, "Received NETWORK_STATE_CHANGED_ACTION");
                    NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                    if (mWifiManager != null && mWifiManager.getDhcpInfo() != null) {
                        String ip = NetworkHelper.getCurrentIpAddress(mWifiManager);
                        Log.d(TAG, "Current wifi: " + mWifiManager.getConnectionInfo().getSSID()
                                + ", Ip address: " +  ip + ", state: " + networkInfo.getDetailedState().name());
                    }
                    break;

                case WifiManager.WIFI_STATE_CHANGED_ACTION:
                    Log.d(TAG, "Received WIFI_STATE_CHANGED_ACTION");
                    break;

                default:
                    break;
            }
        }
    }


    ProgressDialog mLoadingDialog;
    public void showLoading(String message) {
        if (mLoadingDialog == null) {
            mLoadingDialog = ProgressDialog.show(this, "", getString(R.string.common_loading), true);
        }

        mLoadingDialog.setMessage(message);
        mLoadingDialog.show();
    }

    public void showLoading() {
        if (mLoadingDialog == null) {
            mLoadingDialog = ProgressDialog.show(this, "", getString(R.string.common_loading), true);
            mLoadingDialog.show();
        }
    }

    public void hideLoading() {
        try {
            if (mLoadingDialog != null) {
                mLoadingDialog.dismiss();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        mLoadingDialog = null;
    }

    public void showMessage(String message) {
        showAlertDialog(null, message);
    }

    public void showErrorDialog(String message) {
        showAlertDialog(getString(R.string.common_error), message);
    }

    public void showAlertDialog(String title, String message) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}