package com.tepari.iscale;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tepari.iscale.Helper.Files_Saved;
import com.tepari.iscale.database.TepariDatabase;
import com.tepari.tpc.R;

import java.io.File;
import java.util.ArrayList;

public class Fragment_Records extends Fragment {

    public static final String FRAGMENT_ID = "fragment_records";

    ListView listViewSaved;

    private TepariDatabase tepariDatabase;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_records, container, false);

        tepariDatabase = new TepariDatabase(getActivity());

        ((Activity_Parent) getActivity()).actionBar.setTitle(getString(R.string.ui_view_title));
        buttons(rootView);

        ((Activity_Parent) getActivity()).getFiles();
        displayListView(((Activity_Parent) getActivity()).list_records);
        return rootView;
    }

    private void buttons(View rootView) {
        TextView btn_delete = (TextView) rootView.findViewById(R.id.btn_delete);
        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDelete();
            }
        });

        TextView btn_email = (TextView) rootView.findViewById(R.id.btn_email);
        btn_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailFiles();
            }
        });

        final TextView btn_select = (TextView) rootView.findViewById(R.id.btn_select);
        btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean chk_it;
                if (btn_select.getText().equals(getString(R.string.select))) {
                    chk_it = true;
                    btn_select.setText(getString(R.string.deselect));
                } else {
                    chk_it = false;
                    btn_select.setText(getString(R.string.select));
                }
                for (Files_Saved o : ((Activity_Parent) getActivity()).list_records) {
                    o.setSelected(chk_it);
                }
                displayListView(((Activity_Parent) getActivity()).list_records);
            }
        });

        // Listener for button click in button entity of custom checkbox view to open files for preview
        listViewSaved = (ListView) rootView.findViewById(R.id.listViewSaved);
        listViewSaved.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView adapterView, View view, int position, long id) {
                Files_Saved file = ((Activity_Parent) getActivity()).list_records.get(position);
                ((Activity_Parent) getActivity()).csv_filename = file.getName();
                ((Activity_Parent) getActivity()).showFragment(new Fragment_Summary(), "fragment_summary");
            }
        });
    }

    // Refresh file list view if necessary
    public void displayListView(ArrayList<Files_Saved> lst) {
        ((Activity_Parent) getActivity()).list_records = lst;
        ListAdapter dataAdapter = new ListAdapter(getActivity(), R.layout.checkbox_button, lst);
        listViewSaved.setAdapter(dataAdapter);
    }

    // Get selected files path
    private ArrayList<String> what_selected() {
        ArrayList<String> sel = new ArrayList<String>();
        ArrayList<Files_Saved> fileList = ((Activity_Parent) getActivity()).list_records;
        // Building string of files requested for deletion to display in the alert dialog
        for(Files_Saved o : fileList) {
            if(o.isSelected()) {
                sel.add(((Activity_Parent) getActivity()).file_location + o.getName());
            }
        }
        return sel;
    }

    // Checks what files are requested for deletion and lists them in the alert dialog for confirmation of delete
    private void confirmDelete() {
        // Splits the string of files to delete into a string array for serial deletion
        final ArrayList<String> to_delete = what_selected();
        if (to_delete.size() == 0) {
            Toast.makeText(getActivity(), getString(R.string.no_file_selected), Toast.LENGTH_LONG).show();
        } else {
            // Alert Dialog for file deletion
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setTitle(getString(R.string.delete_confirm));
            alertDialogBuilder
                    .setMessage(getString(R.string.s_delete_message, to_delete.size() + ""))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            boolean successfulDelete = true;
                            for (String o : to_delete) {
                                File fileToDelete = new File(o);
                                if (!fileToDelete.delete())
                                    successfulDelete = false;

                                //Delete Database
                                tepariDatabase.deleteTepariFile(fileToDelete.getName());
                            }
                            if (successfulDelete) {
                                Toast.makeText(getActivity(), getString(R.string.s_filename_delete, to_delete.size() + ""), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), getString(R.string.some_file_not_delete), Toast.LENGTH_LONG).show();
                            }
                            displayListView(((Activity_Parent) getActivity()).getFiles());
                        }
                    })
                    .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            // create alert dialog and show it
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    // Determines what files are selected in the list and attaches them all to an outgoing email message
    private void emailFiles() {
        final ArrayList<String> sel = what_selected();
        if (sel.size() == 0) {
            Toast.makeText(getActivity(), getString(R.string.no_file_selected), Toast.LENGTH_LONG).show();
        } else {
            Intent email = new Intent(Intent.ACTION_SEND_MULTIPLE);
            email.setType("text/plain");
            ArrayList<Uri> uris = new ArrayList<>();
            String[] filePaths = sel.toArray(new String[sel.size()]);
            for (String file : filePaths) {
                File fileIn = new File(file);
                Uri u;
                //    Make sure we're running on O or higher to use FileProvider APIs
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    u = FileProvider.getUriForFile((Activity_Parent) getActivity(), "com.tepari.tpc.provider", fileIn);
                }
                else{
                    u = Uri.fromFile(fileIn);
                }

                uris.add(u);
            }
            email.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            // Add as email attachment(s)
            email.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
            try {
                startActivity(Intent.createChooser(email, getString(R.string.notify_email_choose)));
            } catch (android.content.ActivityNotFoundException exception) {
                Toast.makeText(getActivity(), getString(R.string.notify_email_choose_err), Toast.LENGTH_LONG).show();
            }
        }
    }

    // List Adapter
    private class ListAdapter extends ArrayAdapter<Files_Saved> {

        ArrayList<Files_Saved> fileList;
        int resource;
        LayoutInflater inflater;

        public ListAdapter(Context context, int resource, ArrayList<Files_Saved> fileList) {
            super(context, resource, fileList);
            this.resource = resource;
            this.fileList = fileList;
            inflater = LayoutInflater.from(getActivity());
        }

        private class ViewHolder {
            CheckBox name;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            final ViewHolder holder;
            Files_Saved file = fileList.get(position);
            if (convertView == null) {
                holder = new ViewHolder();
                view = inflater.inflate(resource, parent, false);
                holder.name = (CheckBox) view.findViewById(R.id.checkBox1);
                view.setTag(holder);
                holder.name.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox) v;
                        Files_Saved file = (Files_Saved) cb.getTag();
                        file.setSelected(cb.isChecked());
                    }
                });
            } else {
                holder = (ViewHolder) view.getTag();
            }
            holder.name.setText(file.getName());
            holder.name.setChecked(file.isSelected());
            holder.name.setTag(file);
            return view;
        }
    }

}