package com.tepari.iscale.model;

/**
 * Created by KeHo on 10/26/15.
 */
public class StreamModel {
    private String eid;
    private String weight;
    private String date;

    private String listColumnValue;

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getListColumnValue() {
        return listColumnValue;
    }

    public void setListColumnValue(String listColumnValue) {
        this.listColumnValue = listColumnValue;
    }
}
