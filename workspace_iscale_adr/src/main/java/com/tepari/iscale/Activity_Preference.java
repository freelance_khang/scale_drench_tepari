package com.tepari.iscale;

import android.app.Activity;
import android.os.Bundle;

public class Activity_Preference extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            getFragmentManager().beginTransaction()
                    .replace(android.R.id.content, new Fragment_Preference())
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}