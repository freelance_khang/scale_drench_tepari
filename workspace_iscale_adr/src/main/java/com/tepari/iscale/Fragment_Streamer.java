package com.tepari.iscale;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.tepari.iscale.Helper.Constants;
import com.tepari.iscale.Helper.DecodeRecord;
import com.tepari.iscale.Helper.Files_Stream;
import com.tepari.iscale.database.TepariDatabase;
import com.tepari.iscale.model.UploadModel;
import com.tepari.tpc.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Fragment_Streamer extends Fragment {

    public static final String FRAGMENT_ID = "fragment_streamer";

    private TextView filesFoundText;

    private TextView title_eid;
    private TextView value_eid;
    private TextView title_weight;
    private TextView value_weight;



    ViewGroup contn;
    ListView list_csv;
    private ListAdapter_Csv streamTableAdapter;

    private TepariDatabase tepariDatabase;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_streamer, container, false);
        ((Activity_Parent) getActivity()).fragmentStreamer = this;

        tepariDatabase = new TepariDatabase(getActivity());
        filesFoundText = (TextView) rootView.findViewById(R.id.fileListHead);

        title_eid = (TextView) rootView.findViewById(R.id.title_eid);
        value_eid = (TextView) rootView.findViewById(R.id.value_eid);
        title_weight = (TextView) rootView.findViewById(R.id.title_weight);
        value_weight = (TextView) rootView.findViewById(R.id.value_weight);



        txtResult1 = (TextView) rootView.findViewById(R.id.txtResult1);
        txtResult2 = (TextView) rootView.findViewById(R.id.txtResult2);
        txtResult3 = (TextView) rootView.findViewById(R.id.txtResult3);
        txtResult4 = (TextView) rootView.findViewById(R.id.txtResult4);
        txtResult5 = (TextView) rootView.findViewById(R.id.txtResult5);
        txtResult6 = (TextView) rootView.findViewById(R.id.txtResult6);
        txtResult7 = (TextView) rootView.findViewById(R.id.txtResult7);
        txtResult8 = (TextView) rootView.findViewById(R.id.txtResult8);

        txtCmd1 = (EditText) rootView.findViewById(R.id.txtCmd1);
        txtCmd2 = (EditText) rootView.findViewById(R.id.txtCmd2);
        txtCmd3 = (EditText) rootView.findViewById(R.id.txtCmd3);
        txtCmd4 = (EditText) rootView.findViewById(R.id.txtCmd4);
        txtCmd5 = (EditText) rootView.findViewById(R.id.txtCmd5);
        txtCmd6 = (EditText) rootView.findViewById(R.id.txtCmd6);
        txtCmd7 = (EditText) rootView.findViewById(R.id.txtCmd7);
        txtCmd8 = (EditText) rootView.findViewById(R.id.txtCmd8);

        btnCmd1 = (Button) rootView.findViewById(R.id.btnCmd1);
        btnCmd1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendCmd1();
            }
        });
        btnCmd2 = (Button) rootView.findViewById(R.id.btnCmd2);
        btnCmd2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendCmd2();
            }
        });
        btnCmd3 = (Button) rootView.findViewById(R.id.btnCmd3);
        btnCmd3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendCmd3();
            }
        });
        btnCmd4 = (Button) rootView.findViewById(R.id.btnCmd4);
        btnCmd4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendCmd4();
            }
        });
        btnCmd5 = (Button) rootView.findViewById(R.id.btnCmd5);
        btnCmd5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendCmd5();
            }
        });
        btnCmd6 = (Button) rootView.findViewById(R.id.btnCmd6);
        btnCmd6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendCmd6();
            }
        });
        btnCmd7 = (Button) rootView.findViewById(R.id.btnCmd7);
        btnCmd7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendCmd7();
            }
        });
        btnCmd8 = (Button) rootView.findViewById(R.id.btnCmd8);
        btnCmd8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendCmd8();
            }
        });



        loadLocalData();

        contn = container;



        list_csv = (ListView) rootView.findViewById(R.id.list_csv);


        ArrayList<Files_Stream> listContentFile = ((Activity_Parent) getActivity()).listFileStream;
        streamTableAdapter = new ListAdapter_Csv(getActivity(), R.layout.list_live_weight, listContentFile);
        list_csv.setAdapter(streamTableAdapter);


        return rootView;
    }

    private void loadLocalData() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        txtCmd1.setText(sharedPreferences.getString("txtCmd1", ""));
        txtCmd2.setText(sharedPreferences.getString("txtCmd2", ""));
        txtCmd3.setText(sharedPreferences.getString("txtCmd3", ""));
        txtCmd4.setText(sharedPreferences.getString("txtCmd4", ""));
        txtCmd5.setText(sharedPreferences.getString("txtCmd5", ""));
        txtCmd6.setText(sharedPreferences.getString("txtCmd6", ""));
        txtCmd7.setText(sharedPreferences.getString("txtCmd7", ""));
        txtCmd8.setText(sharedPreferences.getString("txtCmd8", ""));

    }

    public void updateConnectStatus(final String statusMessage) {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    filesFoundText.setText(statusMessage);
                }
            });
        }
    }

    public boolean updateView(byte[] data_in) {
        if (data_in.length > 9) {
            try {
                filesFoundText.setText(getString(R.string.streaming));
                DecodeRecord decode = new DecodeRecord();
                Files_Stream obj = decode.constructLiveStream(data_in);
                DecimalFormat formatter = new DecimalFormat("0.##");
                if (obj.getType().equals("Wo")) {
                    title_eid.setVisibility(View.INVISIBLE);
                    value_eid.setVisibility(View.INVISIBLE);
                    title_weight.setVisibility(View.VISIBLE);
                    value_weight.setVisibility(View.VISIBLE);
                    value_weight.setText(formatter.format(Constants.convertDoubleValue(obj.getWeight())));
                } else if (obj.getType().equals("We")) {
                    title_eid.setVisibility(View.VISIBLE);
                    value_eid.setVisibility(View.VISIBLE);
                    title_weight.setVisibility(View.VISIBLE);
                    value_weight.setVisibility(View.VISIBLE);
                    value_eid.setText(obj.getEid());
                    value_weight.setText(formatter.format(Constants.convertDoubleValue(obj.getWeight())));
                }
                if (obj.getModifier().equals("R")) {
                    value_weight.setTextColor(getResources().getColor(R.color.tepari_green));
                    //((Activity_Parent) getActivity()).listFileStream.add(obj);
                /*if(((Activity_Parent) getActivity()).currentPortMode == Activity_Parent.PortMode.EID_WEIGHT) {
                    btnTableDisplayed.setVisibility(View.INVISIBLE);
                } else {
                    btnTableDisplayed.setVisibility(View.VISIBLE);
                }*/
                    ((Activity_Parent) getActivity()).listFileStream.add(obj);
                    displaySummaryRecord();

                    //Store Stream to DB
                    storeStreamToDb(obj);
                } else if (obj.getModifier().equals("L")) {
                    value_weight.setTextColor(getResources().getColor(R.color.tepari_blue));
                }
            } catch(Exception e) {
                e.printStackTrace();
            }

            return true;
        }
        return false;
    }

    private Button btnCmd1, btnCmd2, btnCmd3, btnCmd4, btnCmd5, btnCmd6, btnCmd7, btnCmd8;
    private EditText txtCmd1, txtCmd2, txtCmd3, txtCmd4, txtCmd5, txtCmd6, txtCmd7, txtCmd8;
    private TextView txtResult1, txtResult2, txtResult3, txtResult4, txtResult5, txtResult6, txtResult7, txtResult8;

    private int currentCmd = 0;
    private String currentResult = "";

    public void receiveData(String message) {
        currentResult = message;
        switch (currentCmd) {
            case 1:
                txtResult1.setText(currentResult);
                break;
            case 2:
                txtResult2.setText(currentResult);
                break;
            case 3:
                txtResult3.setText(currentResult);
                break;
            case 4:
                txtResult4.setText(currentResult);
                break;
            case 5:
                txtResult5.setText(currentResult);
                break;
            case 6:
                txtResult6.setText(currentResult);
                break;
            case 7:
                txtResult7.setText(currentResult);
                break;
            case 8:
                txtResult8.setText(currentResult);
                break;
            default:
                Log.i("Test", "No current command: " + message);
                break;
        }
    }

    private void sendCmd1() {
        currentCmd = 1;
        currentResult = "";
        String message = txtCmd1.getText().toString();
        if(message != null && message.length() > 0) {
            String response = ((Activity_Parent) getActivity()).sendCommandTest(message);
            txtResult1.setText(response);
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("txtCmd1", message);
        editor.commit();
    }

    private void sendCmd2() {
        currentCmd = 2;
        currentResult = "";
        String message = txtCmd2.getText().toString();
        if(message != null && message.length() > 0) {
            String response = ((Activity_Parent) getActivity()).sendCommandTest(message);
            txtResult2.setText(response);
        }

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("txtCmd2", message);
        editor.commit();
    }

    private void sendCmd3() {
        currentCmd = 3;
        currentResult = "";
        String message = txtCmd3.getText().toString();
        if(message != null && message.length() > 0) {
            String response = ((Activity_Parent) getActivity()).sendCommandTest(message);
            txtResult3.setText(response);
        }

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("txtCmd3", message);
        editor.commit();
    }

    private void sendCmd4() {
        currentCmd = 4;
        currentResult = "";
        String message = txtCmd4.getText().toString();
        if(message != null && message.length() > 0) {
            String response = ((Activity_Parent) getActivity()).sendCommandTest(message);
            txtResult4.setText(response);
        }

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("txtCmd4", message);
        editor.commit();
    }

    private void sendCmd5() {
        currentCmd = 5;
        currentResult = "";
        String message = txtCmd5.getText().toString();
        if(message != null && message.length() > 0) {
            String response = ((Activity_Parent) getActivity()).sendCommandTest(message);
            txtResult5.setText(response);
        }

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("txtCmd5", message);
        editor.commit();
    }

    private void sendCmd6() {
        currentCmd = 6;
        currentResult = "";
        String message = txtCmd6.getText().toString();
        if(message != null && message.length() > 0) {
            String response = ((Activity_Parent) getActivity()).sendCommandTest(message);
            txtResult6.setText(response);
        }

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("txtCmd6", message);
        editor.commit();
    }

    private void sendCmd7() {
        currentCmd = 7;
        currentResult = "";
        String message = txtCmd7.getText().toString();
        if(message != null && message.length() > 0) {
            String response = ((Activity_Parent) getActivity()).sendCommandTest(message);
            txtResult7.setText(response);
        }

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("txtCmd7", message);
        editor.commit();
    }

    private void sendCmd8() {
        currentCmd = 8;
        currentResult = "";
        String message = txtCmd8.getText().toString();
        if(message != null && message.length() > 0) {
            String response = ((Activity_Parent) getActivity()).sendCommandTest(message);
            txtResult8.setText(response);
        }

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("txtCmd8", message);
        editor.commit();
    }

    private void displaySummaryRecord() {
        ArrayList<Files_Stream> listContentFile = ((Activity_Parent) getActivity()).listFileStream;
        streamTableAdapter.refreshData(listContentFile);
    }

    private void storeStreamToDb(Files_Stream obj) {
        if(!getResources().getBoolean(R.bool.isRelease)) {
            String eid = obj.getEid();

            String weight = obj.getWeight();
            if(eid != null && !eid.trim().equals("")) {
                //Check have or not upload file and show popup
                UploadModel uploadModel = tepariDatabase.getTepariFile(eid);
                if(uploadModel != null) {

                    tepariDatabase.saveStreamRecord(uploadModel, eid, weight);
                } else {
                    //Show confirm to input filename
                    showInputFileName(eid, weight);
                }
            }
        }
    }

    private Dialog dialog;

    private void showInputFileName(final String eid, final String weight) {
        if(dialog != null && dialog.isShowing()) {
            return;
        }
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(getActivity());
        View promptsView = li.inflate(R.layout.prompts, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.editTextDialogUserInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                String fileName = userInput.getText().toString();
                                generateTepariUploadFile(fileName, eid, weight);
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        dialog = alertDialogBuilder.create();

        // show it
        dialog.show();
    }

    private void generateTepariUploadFile(String fileName, String eid, String weight) {
        TepariDatabase tepariDatabase = new TepariDatabase(getActivity());
        tepariDatabase.saveTepariFile(fileName, "EID", eid, eid);
    }

    private class ListAdapter_Csv extends ArrayAdapter<Files_Stream> {
        int resource;
        ArrayList<Files_Stream> fileList;

        LayoutInflater inflater;

        public ListAdapter_Csv(Context context, int resource, ArrayList<Files_Stream> fileList) {
            super(context, resource, fileList);
            this.resource = resource;
            this.fileList = fileList;
            inflater = LayoutInflater.from(getActivity());
        }

        public void refreshData(ArrayList<Files_Stream> data) {
            fileList = data;
            notifyDataSetChanged();
        }

        private class ViewHolder {
            LinearLayout csv_bkgnd;
            TextView eid;
            TextView weight;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            final ViewHolder holder;
            Files_Stream file = fileList.get(position);
            if (convertView == null) {
                holder = new ViewHolder();
                view = inflater.inflate(resource, parent, false);
                holder.csv_bkgnd = (LinearLayout) view.findViewById(R.id.csv_bkgnd);
                holder.eid = (TextView) view.findViewById(R.id.eid);
                holder.weight = (TextView) view.findViewById(R.id.weight);

                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            int col = (position % 2 == 1) ? getResources().getColor(R.color.tepari_csv_grey) : getResources().getColor(R.color.tepari_white);
            holder.csv_bkgnd.setBackgroundColor(col);

            if(file.getEid() != null && !file.getEid().equals("")) {
                holder.eid.setText(file.getEid());
            } else {
                holder.eid.setText(getString(R.string.history_weight_only));
            }

            holder.weight.setText(file.getWeight() + " KG");

            return view;
        }
    }
}