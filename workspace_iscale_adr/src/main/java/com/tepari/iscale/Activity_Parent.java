package com.tepari.iscale;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import androidx.core.content.FileProvider;
import android.text.TextUtils;
import android.text.format.Time;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tepari.iscale.Helper.CommandTimeout;
import com.tepari.iscale.Helper.Constants;
import com.tepari.iscale.Helper.DecodeRecord;
import com.tepari.iscale.Helper.Files_Advance;
import com.tepari.iscale.Helper.Files_Content;
import com.tepari.iscale.Helper.Files_Saved;
import com.tepari.iscale.Helper.Files_Stream;
import com.tepari.iscale.Helper.Files_iScale;
import com.tepari.iscale.database.TepariDatabase;
import com.tepari.iscale.dialog.FileDialog;
import com.tepari.iscale.model.StreamModel;
import com.tepari.iscale.model.UploadModel;
import com.tepari.iscale.wifi.ConnectWifiTask;
import com.tepari.iscale.wifi.NetworkHelper;
import com.tepari.tpc.R;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Activity_Parent extends Activity {

    private TepariDatabase tepariDatabase;

    public String csv_filename = "";
    public ArrayList<Files_Saved> list_records;
    public String file_location;

    TextView btn_connect;
    TextView filesFoundText;
    ProgressBar progressBar;
    ListView listView;

    public ActionBar actionBar;
    //Socket Handle Connect
    private NetworkTask networktask = new NetworkTask();

    //Status task of socket
    public enum SocketTask {STATE_DISCONNECTED, STATE_CONNECTING, STATE_BUSY, STATE_IDLE, STATE_FILELIST, STATE_RECORD, STATE_DELETE, STATE_LIVE_STREAMER}
    //Port Mode
    enum PortMode {EID_PC_CONNECT, EID_WEIGHT, WEIGHT_ONLY, AUTODRAFTING, NO_CHANGE}

    private SocketTask currentTask = SocketTask.STATE_DISCONNECTED;
    private SocketTask runningTask = SocketTask.STATE_DISCONNECTED;

    //Timer to check timeout
    private Handler handler = new Handler();
    private final static long COMMAND_TIMEOUT = 5000L;
    private CommandTimeout commandTimeout = null;


    private List<Files_iScale> listDeviceFile = new ArrayList<>();
    private boolean isLoadFileListFinish = false;

    private ListAdapter fileScaleAdapter;

    public PortMode currentPortMode = PortMode.EID_PC_CONNECT;

    public Fragment_Streamer fragmentStreamer;

    public ArrayList<Files_Stream> listFileStream = new ArrayList<>();

    public String exportFileLocation = null;
    private String fileNameExport = null;

    private byte bData[];
    private int counter=0;

    private String lastScaleSsid;

    // Determine whether the record data (s_rec) is a new structure (starts with 0xFEFC) or an old structure.
    // 0: old, 1: new
    public static int dataId = 0;

    public static int REQEUST_CODE_PICK_SCALE_WIFI = 100;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);

        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        tepariDatabase = new TepariDatabase(this);

        bData = new byte[200];

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        //Setup Action Bar
        actionBar = getActionBar();

        View btnExportLog = findViewById(R.id.btnExportLog);
        /*btnExportLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendLogFile();
            }
        });*/
        if(btnExportLog != null) {
            btnExportLog.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            handlerLog.postDelayed(mLongPressed, 5000);
                            //This is where my code for movement is initialized to get original location.
                            break;
                        case MotionEvent.ACTION_UP:
                            handlerLog.removeCallbacks(mLongPressed);

                            break;
                        case MotionEvent.ACTION_MOVE:
                            handlerLog.removeCallbacks(mLongPressed);
                            //Code for movement here. This may include using a window manager to update the view
                            break;
                    }
                    return false;
                }
            });
        }

        //Show Content page
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new Fragment_Contents())
                .commit();


        file_location = this.getExternalFilesDir(null) + "/TePariConnect/";
        exportFileLocation = this.getExternalFilesDir(null) + "/Export/";

        listDeviceFile = new ArrayList<>();
        list_records = getFiles();

        // Initialise connect page specific content
        connectPage();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_action, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_home:
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getFragmentManager().beginTransaction()
                        .replace(android.R.id.content, new Fragment_Contents())
                        .commit();
                return true;
            case R.id.menu_settings:
                startActivity(new Intent(Activity_Parent.this, Activity_Preference.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //In case the task is currently running
        disconnect();
        networktask.cancel(true);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            this.finish();
        } else {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean reconnect = sharedPreferences.getBoolean("reconnect", false);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("reconnect", false);
        editor.commit();

        //Check to reconnect socket
        if(reconnect) {
            if (currentTask != SocketTask.STATE_DISCONNECTED) {
                disconnect();
            }

            Log.i("FragmentCount", getFragmentManager().getBackStackEntryCount() + "");
            if(getFragmentManager().getBackStackEntryCount() > 0) {
                String fragmentName = getFragmentManager().getBackStackEntryAt(getFragmentManager().getBackStackEntryCount() - 1).getName();
                Log.i("FragmentName", fragmentName);
                if(!fragmentName.equals(Fragment_Contents.FRAGMENT_ID)) {
                    connect(true);
                }
            }
        }
    }

    // Show specific fragment over top of connect function
    public void showFragment(Fragment fragment, String tag) {
        try {
            getFragmentManager().beginTransaction()
                    .replace(android.R.id.content, fragment)
                    .addToBackStack(tag)
                    .commit();
        } catch (Exception e) {
            // error
            e.printStackTrace();
        }
    }

    // Search through the iScale directory on the device and list its contents in an array list
    public ArrayList<Files_Saved> getFiles() {
        File directory = new File(file_location);
        if (!directory.exists()) {
            if (directory.mkdir()) {
                Toast.makeText(this, getString(R.string.directory_created), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, getString(R.string.could_not_create_directory), Toast.LENGTH_LONG).show();
            }
        }
        list_records = new ArrayList<>();
        if (directory.exists()) {
            File[] fileArray = directory.listFiles();
            for (File o : fileArray) {
                Files_Saved file = new Files_Saved(o.getName(), false);
                list_records.add(file);
            }
        }
        return list_records;
    }

    // Check if file currently exists
    public boolean checkLocalExistence(String filename) {
        ArrayList<Files_Saved> local_files = getFiles();
        for (Files_Saved s : local_files) {
            if (s.getName().equals(filename))
                return true;
        }
        return false;
    }

    // Write csv to file
    private void writeToFile(String csv_name, String csv_content) {
        try {
            FileWriter writer = new FileWriter(file_location + csv_name);
            writer.write(csv_content);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        list_records = getFiles();
    }

    //Read both CSV or Minda
    public ArrayList<Files_Content> readContentFile() {
        ArrayList<Files_Content> csv_lst = new ArrayList<>();
        try {
            Log.i("Read File", file_location + csv_filename);
            BufferedReader br = new BufferedReader(new FileReader(file_location + csv_filename));
            String line;
            int count = 0;
            String header = "";

            // Hide values of other unit
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Activity_Parent.this);
            String unitType = prefs.getString("unit", Constants.UNIT_TYPE_KG);
            String eidType = prefs.getString("eidFormat", Constants.EID_TYPE_SPACE);

            while ((line = br.readLine()) != null) {
                if(line.trim().equals("")) {
                    continue;
                }
                if(count == 0) {
                    header = line;
                    count++;
                    continue;
                }
                String[] rec = line.split(",");
                if(rec.length <= 3) {
                    rec = line.split(";");
                }
                if(rec.length <= 3) {
                    rec = line.split(":");
                }
                Log.i("Length Line", rec.length + "-->" + line);

                if (!rec.equals("") && rec.length >= 17) {
                    String draft = "";
                    if (rec.length > 17) {
                        draft = rec[17];
                    }// Don't read PID, weightlb, prev_weightlb

                    Files_Content csv = null;
                    if (unitType.equals(Constants.UNIT_TYPE_LB)){
                        csv = new Files_Content(Constants.UNIT_TYPE_LB, "" + count, rec[0], rec[1], rec[2], "", rec[4], rec[5], rec[6], rec[7], rec[8], rec[9], rec[10], "", rec[12], rec[13], "", rec[15], rec[16], draft);
                        csv.calculateWeightGain();
                        csv.setWeight_gain("");             // Set it to empty if unit is lb
                        csv.setWeight_gain_total_kg("");    // Set it to empty
                    }
                    else {//if (unitType.equals(Constants.UNIT_TYPE_KG)) {
                        csv = new Files_Content(Constants.UNIT_TYPE_KG, "" + count, rec[0], rec[1], rec[2], "", rec[4], rec[5], rec[6], rec[7], rec[8], rec[9], rec[10], rec[11], "", rec[13], rec[14], "", rec[16], draft);
                        csv.calculateWeightGain();
                        csv.setWeight_gain_lb("");          // Set it to empty if unit is kg
                        csv.setWeight_gain_total_lb("");    // Set it to empty
                    }
                    csv_lst.add(csv);
                    count++;
                } else {
                    //Stream
                    if (header.contains(", WEIGHT, DATE")) {
                        Files_Content csv = new Files_Content(Constants.FILE_TYPE_STREAM, "" + count, "", rec[0], "", "", "", "", "", "", "", "", "", rec[rec.length - 2], "", rec[rec.length - 1], "", "", "", "");
                        csv.setListColumnName(header);
                        csv.setListColumnValue(line);
                        csv_lst.add(csv);
                    }
                    count++;
                }
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            csv_lst = new ArrayList<>();
        } catch (IOException e) {
            e.printStackTrace();
            csv_lst = new ArrayList<>();
        }
        return csv_lst;
    }

    // Minimise keyboard on button download press
    private static void closeKeyboard(Context c, IBinder windowToken) {
        InputMethodManager mgr = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(windowToken, 0);
    }

    public void generateCsvFromDatabase() {
        List<UploadModel> listUploadModel = tepariDatabase.getTepariFileList();
        for(UploadModel uploadModel: listUploadModel) {
            String eid = uploadModel.getEid();
            List<StreamModel> listStreamModel = tepariDatabase.getStreamModelList(eid);

            createFileCsvStream(uploadModel, listStreamModel);
        }
    }

    private void createFileCsvStream(UploadModel uploadModel, List<StreamModel> listStreamModel) {
        String fileNameWithExt = uploadModel.getFileName();
        String dataFile = uploadModel.getListColumnName() + ", WEIGHT, DATE\n";


        for(StreamModel entry : listStreamModel) {
            String row = entry.getListColumnValue();

            dataFile += row + "\n";
        }
        writeToFile(fileNameWithExt, dataFile);
    }

    public void generateAdvanceTemp(String fileName, ArrayList<Files_Advance> fileAdvanceList) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Activity_Parent.this);
        delimiterCharacter = prefs.getString("csvDelimiter", ",");


        String content = getString(R.string.csv_title_advance_bar);
        if(!delimiterCharacter.equals(",")) {
            content = content.replaceAll(",", delimiterCharacter);
        }
        DecimalFormat formatter = new DecimalFormat("0.##");
        for(Files_Advance entry : fileAdvanceList) {
            content += entry.getGateNumber() + delimiterCharacter + entry.getCount() + delimiterCharacter + formatter.format(Constants.convertDoubleValue(entry.getAverageWeight())) + delimiterCharacter + formatter.format(Constants.convertDoubleValue(entry.getTotalWeight())) + "\n";
        }

        fileNameExport = fileName;
        try {
            File temLocation = new File(exportFileLocation);
            if (!temLocation.exists()) {
                if (temLocation.mkdir()) {
                    Toast.makeText(this, getString(R.string.directory_created), Toast.LENGTH_LONG).show();
                }
            }
            FileWriter writer = new FileWriter(exportFileLocation + fileNameExport);
            writer.write(content);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String generateEmailExportFileContent(String fileName, List<Files_Content> listFileContent) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Activity_Parent.this);
        delimiterCharacter = prefs.getString("csvDelimiter", ",");
        String fileType = prefs.getString("fileFormat", Constants.FILE_TYPE_CSV);

        String content;

        if(listFileContent != null && !listFileContent.isEmpty() && listFileContent.get(0).getFileFormat().equals(Constants.FILE_TYPE_STREAM)) {
            content = listFileContent.get(0).getListColumnName() + "\n";

            for(Files_Content entry : listFileContent) {
                String rowValue = entry.getListColumnValue();
                if(!delimiterCharacter.equals(",")) {
                    rowValue = rowValue.replaceAll(",", delimiterCharacter);
                }
                content += rowValue + "\n";
            }
        } else {
            if(fileType.equals(Constants.FILE_TYPE_CSV)) {
                content = " ," + getString(R.string.csv_title_bar_view);
            } else {
                content = " ," + getString(R.string.csv_title_minda_bar);
            }
            if(!delimiterCharacter.equals(",")) {
                content = content.replaceAll(",", delimiterCharacter);
            }

            for(Files_Content entry : listFileContent) {
                content += entry.exportRow(fileType, delimiterCharacter);
            }
        }





        String tempExport = fileName;
        try {
            File temLocation = new File(exportFileLocation);
            if (!temLocation.exists()) {
                if (temLocation.mkdir()) {
                    Toast.makeText(this, getString(R.string.directory_created), Toast.LENGTH_LONG).show();
                }
            }
            FileWriter writer = new FileWriter(exportFileLocation + tempExport);
            writer.write(content);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tempExport;
    }


    public void generateSummaryTemp(String fileName, String fileType, String kgMaxWeight, String kgMinWeight, String kgTotalWeight, String kgAverageWeight, String kgWeightGainTotal, String kgWeighGainAverage, String kgWeightGainDay, String kgWeightGainDayAverage, String lbMaxWeight, String lbMinWeight, String lbTotalWeight, String lbAverageWeight, String lbWeightGainTotal, String lbWeighGainAverage, String lbWeightGainDay, String lbWeightGainDayAverage) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Activity_Parent.this);
        delimiterCharacter = prefs.getString("csvDelimiter", ",");


        String content = getString(R.string.csv_title_summary_bar);
        if(!delimiterCharacter.equals(",")) {
            content = content.replaceAll(",", delimiterCharacter);
        }
        content += kgMaxWeight + delimiterCharacter + kgMinWeight + delimiterCharacter + kgTotalWeight + delimiterCharacter + kgAverageWeight + delimiterCharacter + kgWeightGainTotal + delimiterCharacter + kgWeighGainAverage + delimiterCharacter + kgWeightGainDay + delimiterCharacter + kgWeightGainDayAverage + "\n";
        //if(fileType.equals(Constants.FILE_TYPE_CSV)) {
            content += lbMaxWeight + delimiterCharacter + lbMinWeight + delimiterCharacter + lbTotalWeight + delimiterCharacter + lbAverageWeight + delimiterCharacter + lbWeightGainTotal + delimiterCharacter + lbWeighGainAverage + delimiterCharacter + lbWeightGainDay + delimiterCharacter + lbWeightGainDayAverage + "\n";
        //}

        fileNameExport = fileName;
        try {
            File temLocation = new File(exportFileLocation);
            if (!temLocation.exists()) {
                if (temLocation.mkdir()) {
                    Toast.makeText(this, getString(R.string.directory_created), Toast.LENGTH_LONG).show();
                }
            }
            FileWriter writer = new FileWriter(exportFileLocation + fileNameExport);
            writer.write(content);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteExportFile() {
        if(fileNameExport != null && !fileNameExport.equals("")) {
            File fileToDelete = new File(exportFileLocation + fileNameExport);
            boolean deleted = fileToDelete.delete();
        }
        fileNameExport = null;
    }


    // Connect Page Specific Functions

    // Initialise connect page
    private void connectPage() {
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        filesFoundText = (TextView) findViewById(R.id.fileListHead);
        btn_connect = (TextView) findViewById(R.id.btn_connect);

        listView = (ListView) findViewById(R.id.listViewIScale);

        // Create initial instance so SendDataToNetwork doesn't throw an error.
        networktask = new NetworkTask();
        // Connection is disconnected/idle and progressbar should be null
        progressBar.setProgress(0);

        btn_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentTask == SocketTask.STATE_DISCONNECTED) {
                    connect(false);
                } else if(currentTask == SocketTask.STATE_BUSY) {
                    disconnect();
                    connect(false);
                } else {
                    disconnect();
                    runningTask = SocketTask.STATE_IDLE;
                }
            }
        });

        final TextView btnDownload = (TextView) findViewById(R.id.buttonDownload);
        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnDownloadClick();
            }
        });

        final TextView btnDelete = (TextView) findViewById(R.id.buttonDelete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_delete();
            }
        });
    }

    // Start network task on first connect page open
    public void autoStart() {
        currentPortMode = PortMode.EID_PC_CONNECT;
        runningTask = SocketTask.STATE_IDLE;
        fragmentStreamer = null;
        if (currentTask == SocketTask.STATE_DISCONNECTED) {
            connect(false);
        }else if(currentTask == SocketTask.STATE_LIVE_STREAMER) {
                networktask.progressChangePortModeAndGetListFile();
           }
    }

    // Check if a download should begin
    private void btnDownloadClick() {
        if (currentTask == SocketTask.STATE_IDLE) {

            ArrayList<Files_iScale> listDownload = new ArrayList<>();
            int to_download = 0;
            for (int i = 0; i < listDeviceFile.size(); i++) {
                Files_iScale iScale_file_list = listDeviceFile.get(i);
                if (iScale_file_list.isSelected()) {
                    listDownload.add(iScale_file_list);
                    to_download += iScale_file_list.getCode();
                }
            }
            if (to_download == 0) {
                Toast.makeText(this, getString(R.string.toast_none_selected), Toast.LENGTH_LONG).show();
            } else {
                //Check exist
                if (listDownload.size() == 1) {
                    String fileSaveName = listDownload.get(0).getName();
                    if (checkLocalExistence(fileSaveName + "." + Constants.FILE_TYPE_CSV)) {
                        alert_overwrite(false, fileSaveName, listDownload, to_download);
                    } else {
                        networktask.initiateDownload(false, fileSaveName, listDownload, to_download);
                    }
                } else {
                    String fileSaveName = listDownload.get(0).getName();
                    alert_defaultName(fileSaveName, listDownload, to_download);
                }
            }
        } else if (currentTask == SocketTask.STATE_DISCONNECTED) {
            Toast.makeText(Activity_Parent.this, getString(R.string.not_connected), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(Activity_Parent.this, getString(R.string.busy_try_again_soon), Toast.LENGTH_LONG).show();
        }
    }

    // Check if a download should begin
    private void btn_delete() {
        if (currentTask == SocketTask.STATE_IDLE) {
            ArrayList<Files_iScale> listDownload = new ArrayList<>();
            for (int i = 0; i < listDeviceFile.size(); i++) {
                Files_iScale iScale_file_list = listDeviceFile.get(i);
                if (iScale_file_list.isSelected()) {

                    listDownload.add(iScale_file_list);
                }
            }
            if (listDownload.size() == 0) {
                Toast.makeText(this, getString(R.string.toast_none_selected), Toast.LENGTH_LONG).show();
            } else {
                confirmDelete(listDownload);
            }
        } else if (currentTask == SocketTask.STATE_DISCONNECTED) {
            Toast.makeText(Activity_Parent.this, getString(R.string.not_connected), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(Activity_Parent.this, getString(R.string.busy_try_again_soon), Toast.LENGTH_LONG).show();
        }
    }

    // Checks what files are requested for deletion and lists them in the alert dialog for confirmation of delete
    private void confirmDelete(final ArrayList<Files_iScale> listSelectFile) {
        // Alert Dialog for file deletion
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.delete_confirm));
        alertDialogBuilder
                .setMessage(getString(R.string.s_delete_message, listSelectFile.size() + ""))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                networktask.initiateDeleteFile(listSelectFile, listSelectFile.size());
                            }
                        })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        // create alert dialog and show it
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    // Alert to use current time as default filename
    private void alert_defaultName(final String fileSaveName, final ArrayList<Files_iScale> listDownload, final int to_download) {
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();


        CharSequence[] items = new CharSequence[2];
        items[0] = getString(R.string.select_file_with_date);
        items[1] = getString(R.string.select_separate_file);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.default_filename));
        alertDialogBuilder
                .setCancelable(true)
                .setSingleChoiceItems(items, 0, null)
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        String dateFileName = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                        if (selectedPosition == 0) {
                            networktask.initiateDownload(true, dateFileName, listDownload, to_download);
                        } else {
                            //Check Exist
                            for (Files_iScale files_iScale : listDownload) {
                                String tempName = files_iScale.getName();
                                if (checkLocalExistence(tempName + "." + Constants.FILE_TYPE_CSV)) {
                                    alert_overwrite(false, fileSaveName, listDownload, to_download);
                                    return;
                                }
                            }
                            networktask.initiateDownload(false, fileSaveName, listDownload, to_download);
                        }

                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public String sendCommandTest(String message) {
        return networktask.requestSendCommandTest(message);
    }

    // Alert to use live weight mode
    public void confirmWeightOptionMode() {
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();

        CharSequence[] items = new CharSequence[3];
        items[0] = getString(R.string.mode_eid_weight);
        items[1] = getString(R.string.mode_weight_only);
        items[2] = getString(R.string.mode_autodrafting);


        if (currentTask != SocketTask.STATE_LIVE_STREAMER && currentTask != SocketTask.STATE_IDLE && currentTask != SocketTask.STATE_DISCONNECTED ) {
            Toast.makeText(Activity_Parent.this, getString(R.string.busy_try_again_soon), Toast.LENGTH_LONG).show();
            return;
        }

     /*   AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.select_port_mode));
        alertDialogBuilder
                .setCancelable(true)
                .setSingleChoiceItems(items, 0, null)
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                   */     fragmentStreamer = null;
                        listFileStream = new ArrayList<Files_Stream>();
                        showFragment(new Fragment_Streamer(), "fragment_streamer");
                 //       if (selectedPosition == 0) {
                            networktask.requestChangePortMode(PortMode.NO_CHANGE);
                   /*     } else if (selectedPosition == 1) {
                            networktask.requestChangePortMode(PortMode.WEIGHT_ONLY);
                        } else if (selectedPosition == 2) {
                            networktask.requestChangePortMode(PortMode.AUTODRAFTING);
                        }
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();*/   // &JS REMOVE ME
    }

    // Alert to overwrite a saved file with same name
    private void alert_overwrite(final boolean mergeFile, final String fileSaveName, final ArrayList<Files_iScale> listDownload, final int to_download) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.overwrite_existing_file));
        alertDialogBuilder
                .setMessage(getString(R.string.duplicate_save_file_message))
                .setCancelable(true)
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton(getString(R.string.overwrite), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        networktask.initiateDownload(mergeFile, fileSaveName, listDownload, to_download);
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    // Attempts 1connection to the iScale and prepares progressbar
    private void connect(boolean isRetry) {
        // Initialise progressbar to get file list soon
        progressBar.setProgress(0);
        progressBar.setMax(100);

        WifiManager wifiManager = (WifiManager) TePari_Application.getApplication()
                .getApplicationContext().getSystemService (Context.WIFI_SERVICE);
        String currentSsid = NetworkHelper.getCurrentWifiSsid(wifiManager);

        if (!NetworkHelper.isConnectingToScaleWifi()) {
            if (isRetry && !TextUtils.isEmpty(lastScaleSsid)) {
                progressBar.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        connectToDeviceWifi(lastScaleSsid);
                    }
                }, 150);

            } else {
                Intent intent = new Intent(this, Activity_PickWifi.class);
                startActivityForResult(intent, REQEUST_CODE_PICK_SCALE_WIFI);
            }

            return;
        }

        // Empty current file List
        //displayList(new ArrayList<Files_iScale>(), SocketTask.STATE_CONNECTING);
        updateConnectStatus(SocketTask.STATE_CONNECTING);
        Pair<String, Integer> connection = Constants.getConnectionDetails(Activity_Parent.this);
        filesFoundText.setText(getString(R.string.s_notify_attempt, connection.first, connection.second));
        if (fragmentStreamer != null) {
            fragmentStreamer.updateConnectStatus(getString(R.string.s_notify_attempt, connection.first, connection.second));
        }
        // Attempting 1connection
        networktask = new NetworkTask(); //New instance of NetworkTask
        networktask.execute();
        progressBar.setProgress(10);
    }

    void connectToDeviceWifi(String gunSsid) {
        ConnectWifiTask connectWifiTask = new ConnectWifiTask(this);
        connectWifiTask.setListener(new ConnectWifiTask.Listener() {
            @Override
            public void onFinish(boolean connected) {
                if (!connected) {
                    filesFoundText.setText(getString(R.string.notify_failed));
                    if (fragmentStreamer != null) {
                        fragmentStreamer.updateConnectStatus(getString(R.string.notify_failed));
                    }
                    updateConnectStatus(SocketTask.STATE_DISCONNECTED);
                }  else {
                    if (!NetworkHelper.isConnectingToScaleWifi()) {
                        filesFoundText.setText(getString(R.string.notify_failed));
                        if (fragmentStreamer != null) {
                            fragmentStreamer.updateConnectStatus(getString(R.string.notify_failed));
                        }
                        updateConnectStatus(SocketTask.STATE_DISCONNECTED);
                    } else {
                        connect(true);
                    }
                }
            }
        });

        updateConnectStatus(SocketTask.STATE_CONNECTING);
        filesFoundText.setText(getString(R.string.notify_connect_wifi_network));
        if (fragmentStreamer != null) {
            fragmentStreamer.updateConnectStatus(getString(R.string.notify_connect_wifi_network));
        }
        connectWifiTask.execute(gunSsid);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQEUST_CODE_PICK_SCALE_WIFI && resultCode == RESULT_OK) {
            WifiManager wifiManager = (WifiManager) TePari_Application.getApplication()
                    .getApplicationContext().getSystemService (Context.WIFI_SERVICE);
            lastScaleSsid = NetworkHelper.getCurrentWifiSsid(wifiManager);
            connect(false);
        }
    }

    //    private void showNoScaleWifiDialog() {
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
//        alertDialogBuilder.setTitle(getString(R.string.overwrite_existing_file));
//        alertDialogBuilder
//                .setMessage(getString(R.string.duplicate_save_file_message))
//                .setCancelable(true)
//                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//                    }
//                })
//                .setPositiveButton(getString(R.string.overwrite), new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        networktask.initiateDownload(mergeFile, fileSaveName, listDownload, to_download);
//                    }
//                });
//        AlertDialog alertDialog = alertDialogBuilder.create();
//        alertDialog.show();
//    }

    private void autoReconnect() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                networktask.close();
                connect(true);
            }
        });
    }

    // Discontinues socket communication between iScale and Android
    private void disconnect() {
        displayList(new ArrayList<Files_iScale>(), SocketTask.STATE_DISCONNECTED);
        networktask.close(); // Close connection
        updateConnectStatus(SocketTask.STATE_DISCONNECTED);
        progressBar.setProgress(0);
    }

    // Updates connect button text and current task
    private void updateConnectStatus(SocketTask cur_tsk) {
        currentTask = cur_tsk;
        switch (cur_tsk) {
            case STATE_CONNECTING: // Test Connection
                btn_connect.setText(getString(R.string.busy));
                break;
            case STATE_BUSY:
                btn_connect.setText(getString(R.string.reconnect));
                break;
            case STATE_FILELIST: // Get File List
                btn_connect.setText(getString(R.string.busy));
                break;
            case STATE_RECORD: // Get Record Data
                btn_connect.setText(getString(R.string.cancel));
                break;
            case STATE_DISCONNECTED: // Get Record Data
                btn_connect.setText(getString(R.string.connect));
                break;
            case STATE_IDLE: // Get Record Data
                btn_connect.setText(getString(R.string.disconnect));
                break;
            case STATE_LIVE_STREAMER:
                btn_connect.setText(getString(R.string.busy));
                break;
            default: // err
                break;
        }
    }

    // Display files on iscale
    private void displayList(List<Files_iScale> lst, SocketTask cur_task) {
        //listDeviceFile = lst;
        currentTask = cur_task;
        fileScaleAdapter = new ListAdapter(this, R.layout.checkbox_subtext, lst);
        listView.setAdapter(fileScaleAdapter);

        progressBar.setProgress(100);
    }

    private Runnable checkTimeOutRunnable = new Runnable() {
        @Override
        public void run() {
            if(commandTimeout == null) {
                return;
            }
            switch (commandTimeout.getSocketTask()) {
                case STATE_FILELIST:
                    filesFoundText.setText(getString(R.string.notify_not_established));
                    updateConnectStatus(SocketTask.STATE_BUSY);
                    break;
                default:
                    break;
            }
        }
    };

    private Runnable finalActionGetListFile = new Runnable() {
        @Override
        public void run() {
            filesFoundText.setText(getString(R.string.s_found_available_for_download, listDeviceFile.size() + ""));

            displayList(listDeviceFile, SocketTask.STATE_IDLE);
            updateConnectStatus(SocketTask.STATE_IDLE);

            commandTimeout = null;
            runningTask = SocketTask.STATE_IDLE;

            isLoadFileListFinish = false;
        }
    };

    private int num_downloaded = 0;

    private String listFileContent = "";

    private int toDownload = 0;
    private int num_selected = 0;
    private int download_index = 0;
    private List<String> listRecord;
    private String currentFileName = "";
    private boolean isRetry = false;
    private List<String> currentRecord = new ArrayList<>();

    private String delimiterCharacter;
    private boolean removeNonEid;
    private boolean mergeFile;
    private String saveFileName;

    private ArrayList<Files_iScale> listSelectedFile;
    private boolean multiFile = false;


    private long lastHeartBeatAt = 0;
    private Handler heartBeatHandler = new Handler();
    private Runnable taskHearBearThread = new Runnable() {
        @Override
        public void run() {
            long currentTime = System.currentTimeMillis();
            long delay = 20000;
            if(lastHeartBeatAt + 20000 <= currentTime) {
                lastHeartBeatAt = currentTime;
                networktask.requestSendHeartbeat();
            } else {
                delay = lastHeartBeatAt + 20000 - currentTime;
            }
            heartBeatHandler.postDelayed(taskHearBearThread, delay);
        }
    };

    private boolean isConnecting = false;
    private boolean isAutoReconnect = false;
    private int countRetryConnect = 0;

    // This class handles socket communication to iScale. (Send and receive)
    private class NetworkTask extends AsyncTask<Void, byte[], Boolean> {

        Socket nsocket;
        InputStream nis;
        OutputStream nos;

        private ByteArrayOutputStream socketBufferData = new ByteArrayOutputStream();

        @Override
        protected void onPreExecute() {
            Log.i("AsyncTask", "onPreExecute");
        }

        @Override
        public Boolean doInBackground(Void... params) {
            boolean result = false;
            try {
                lastHeartBeatAt = System.currentTimeMillis();
                Log.i("AsyncTask", "doInBackground: Creating socket at " + lastHeartBeatAt);
                isAutoReconnect = false;
                nsocket = new Socket();
                Pair<String, Integer> connection = Constants.getConnectionDetails(Activity_Parent.this);
                nsocket.connect(new InetSocketAddress(connection.first, connection.second), Constants.getConnectionTimeout(Activity_Parent.this));
                if (nsocket.isConnected()) {
                    WifiManager wifiManager = (WifiManager) TePari_Application.getApplication()
                            .getApplicationContext().getSystemService (Context.WIFI_SERVICE);
                    lastScaleSsid = NetworkHelper.getCurrentWifiSsid(wifiManager);

                    isConnecting = true;
                    isAutoReconnect = true;
                    countRetryConnect = 0;
                    // Establish streams
                    nis = nsocket.getInputStream();
                    nos = nsocket.getOutputStream();
                    byte[] buffer = new byte[4096];
                    int read = nis.read(buffer, 0, 4096); //This is blocking


                    heartBeatHandler.postDelayed(taskHearBearThread, 10000);

                    while (read != -1 && isConnecting) {
                        lastHeartBeatAt = System.currentTimeMillis();
                        byte[] temp_data = new byte[read];
                        System.arraycopy(buffer, 0, temp_data, 0, read);
                        publishProgress(temp_data); // onProgressUpdate

                        read = nis.read(buffer, 0, 4096); //This is blocking
                        Log.i("AsyncTask", read + " byte");
                    }
                }
            } catch (IOException e) {
                Log.i("AsyncTask", "doInBackground: IOException");
                result = true;
            } catch (Exception e) {
                Log.i("AsyncTask", "doInBackground: Exception");
                result = true;
            } finally {
                try {
                    nis.close();
                    nos.close();
                    nsocket.close();
                    isConnecting = false;
                    countRetryConnect++;
                    heartBeatHandler.removeCallbacks(taskHearBearThread);
                } catch (IOException e) {
                    Log.i("AsyncTask", "Fail: IOException");
                } catch (Exception e) {
                    Log.i("AsyncTask", "Fail: Exception");
                }
                Log.i("AsyncTask", "doInBackground: Finished");
                if(isAutoReconnect && countRetryConnect < 5) {
                    Log.i("AsyncTask", "doInBackground: AutoReconnect");
                    autoReconnect();
                }
            }
            return result;
        }

        private String commandTest = "";
        public String requestSendCommandTest(String message) {
            if (nsocket != null && nsocket.isConnected()) {
                commandTest = "";
                try {
                    nos.write(message.getBytes());
                    return getString(R.string.sent);
                } catch (Exception e) {
                    return getString(R.string.error);
                }
            } else {
                commandTest = message;
                autoReconnect();
                return getString(R.string.reconnect);
            }
        }

        public void requestChangePortMode(PortMode portMode) {
            try {
                runningTask = SocketTask.STATE_LIVE_STREAMER;
                currentPortMode = portMode;

                //Check connect
                if (nsocket == null || nsocket.isClosed()) {
                    connect(false);
                    return;
                }
                if (nsocket.isConnected()) {
                    Log.i("AsyncTask", "SendDataToNetwork: Writing request Live Weight message to socket " + portMode);
                    if (portMode != PortMode.EID_PC_CONNECT) {
                        updateConnectStatus(SocketTask.STATE_LIVE_STREAMER);
                        if (fragmentStreamer != null) {
                            fragmentStreamer.updateConnectStatus(getString(R.string.waiting_for_stream));
                        }
                    }
                    socketBufferData = new ByteArrayOutputStream();
                  /*  switch (portMode) {
                        case EID_WEIGHT:
                            nos.write(Constants.command_live_eid_weight);
                            break;
                        case WEIGHT_ONLY:
                         //   nos.write(Constants.command_live_weight_only);
                            nos.write(Constants.command_live_eid_weight);
                            break;
                        case AUTODRAFTING:
                            nos.write(Constants.command_live_autodrafting);
                            break;
                    }*/ // $JS REMOVE ME

                } else {
                    Log.i("AsyncTask", "SendDataToNetwork: Cannot send message. Socket is closed");
                    filesFoundText.setText(getString(R.string.notify_not_established));
                    if (fragmentStreamer != null) {
                        fragmentStreamer.updateConnectStatus(getString(R.string.notify_not_established));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.i("AsyncTask", "SendDataToNetwork: Message send failed. Caught an exception");
                filesFoundText.setText(getString(R.string.notify_please_connect));
                if (fragmentStreamer != null) {
                    fragmentStreamer.updateConnectStatus(getString(R.string.notify_please_connect));
                }
            }
        }

        public void requestSendHeartbeat() {
            try {
                lastHeartBeatAt = System.currentTimeMillis();

                //Check connect
                if (nsocket == null || nsocket.isClosed()) {
                    connect(false);
                    return;
                }
                if (nsocket.isConnected()) {
                    nos.write(Constants.command_heartbeat);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Request Record Data
        private void request_record(byte index, boolean first) {

            byte[] command = Constants.command_record;
            command[Constants.RECORD_INDEX] = index;
            if (first) num_downloaded = 0;
            try {
                if (nsocket.isConnected()) {
                    updateConnectStatus(SocketTask.STATE_RECORD);
                    Log.i("AsyncTask", "SendDataToNetwork: Writing received message to socket");
                    nos.write(command);
                } else {
                    Log.i("AsyncTask", "SendDataToNetwork: Cannot send message. Socket is closed");
                    filesFoundText.setText(getString(R.string.notify_not_established));
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.i("AsyncTask", "SendDataToNetwork: Message send failed. Caught an exception");
                filesFoundText.setText(getString(R.string.notify_please_connect));
            }
        }

        private void request_delete(byte index, boolean first) {
            byte[] command = Constants.command_delete;
            Log.i("DELETE INDEX", index + "");
            command[Constants.DELETE_INDEX] = index;
            if (first) num_downloaded = 0;
            try {
                if (nsocket.isConnected()) {
                    updateConnectStatus(SocketTask.STATE_DELETE);
                    Thread.sleep(1);
                    Log.i("AsyncTask", "SendDataToNetwork: Writing received message to socket");
                    //nos.write(Constants.command_file_list);
                    nos.write(command);


                } else {
                    Log.i("AsyncTask", "SendDataToNetwork: Cannot send message. Socket is closed");
                    filesFoundText.setText(getString(R.string.notify_not_established));
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.i("AsyncTask", "SendDataToNetwork: Message send failed. Caught an exception");
                filesFoundText.setText(getString(R.string.notify_please_connect));
            }
        }


        // Close socket to iScale and disconnect
        private void close() {
            try {
                Log.i("AsyncTask", "Call close socket");
                if(isConnecting) {
                    isConnecting = false;
                    isAutoReconnect = false;
                    nsocket.shutdownInput();
                    nsocket.shutdownOutput();
                    filesFoundText.setText(getString(R.string.notify_disconnected));
                    if (fragmentStreamer != null) {
                        fragmentStreamer.updateConnectStatus(getString(R.string.notify_disconnected));
                    }
                }
            } catch (Exception e) {
                Log.i("AsyncTask", "Fail close");
                e.printStackTrace();
                filesFoundText.setText(getString(R.string.notify_failed));
                if (fragmentStreamer != null) {
                    fragmentStreamer.updateConnectStatus(getString(R.string.notify_failed));
                }
            }
        }

        private boolean isLiveWeightCalculate = false;
        // Decode data returned from Socket
        @Override
        protected void onProgressUpdate(byte[]... values) {

            socketBufferData.write(values[0], 0, values[0].length);

            String temp = "";
            for(byte aa : values[0]) {
                temp += " " + aa;
            }

            Log.i("Trace", "(" + values[0].length + ")" + socketBufferData.size() + "|" + temp);

            if (values.length > 0) { // if data is returned
                switch (currentTask) {
                    case STATE_CONNECTING: // Test Connection
                        if(socketBufferData.size() >= Constants.Response.length()) {
                            String connectIdentifier = new String(socketBufferData.toByteArray()).substring(0, Constants.Response.length());

                            ByteArrayOutputStream newByteRemain = new ByteArrayOutputStream();
                            newByteRemain.write(socketBufferData.toByteArray(), Constants.Response.length(), socketBufferData.size() - Constants.Response.length());

                            socketBufferData = new ByteArrayOutputStream();
                            socketBufferData.write(newByteRemain.toByteArray(), 0, newByteRemain.size());
                            socketBufferData.write(newByteRemain.toByteArray(), 0, newByteRemain.size());

                            progressStateConnectingReceiveData(connectIdentifier);
                        }
                        break;
                    case STATE_FILELIST: // Get File List
                        String tempFileList = new String(socketBufferData.toByteArray());
                        tempFileList = tempFileList.replace(Constants.Response, "");
                        if(tempFileList.trim().endsWith(",")) {
                            boolean result = progressStateFileListReceiveData(tempFileList);
                            if(result) {
                                socketBufferData = new ByteArrayOutputStream();
                            }
                        }
                        break;
                    case STATE_RECORD: // Get Record Data
                        //socketBufferData = "";
                        for (int x=0; x<values[0].length; x++) {
                            bData[counter] = values[0][x];
                            counter++;
                        }
                        Log.i("Counter", counter + " - " + socketBufferData.size());
                        if (socketBufferData.size() >=128) {
                            Log.i("Counter Raw", socketBufferData.size() + "");
                            download(socketBufferData.toByteArray());
                            counter = 0;
                            socketBufferData = new ByteArrayOutputStream();
                        }
                        break;
                    case STATE_DELETE:
                        delete(values[0]);
                        break;
                    case STATE_LIVE_STREAMER:
                        if (fragmentStreamer != null) {
                            fragmentStreamer.updateConnectStatus(getString(R.string.streaming_waiting));
                            fragmentStreamer.receiveData(new String(socketBufferData.toByteArray()));

                            String bbbb = new String(socketBufferData.toByteArray());

                            //Remove <OK> and *HELLO*
                            bbbb = bbbb.replace(Constants.Response, "");
                            bbbb = bbbb.replace("<OK>", "");

                            int firstSign = 0;

                            if(!isLiveWeightCalculate) {
                                isLiveWeightCalculate = true;
                                while(bbbb.contains("<") && bbbb.contains(">")) {
                                    firstSign = bbbb.indexOf("<");
                                    int endMessage = bbbb.indexOf(">");
                                    String packageMessage = bbbb.substring(firstSign, endMessage + 1);

                                    if(bbbb.length() > endMessage + 1) {
                                        bbbb = bbbb.substring(endMessage + 1);

                                        socketBufferData = new ByteArrayOutputStream();
                                        socketBufferData.write(bbbb.getBytes(Charset.forName("US-ASCII")), 0, bbbb.length());
                                    } else {
                                        socketBufferData = new ByteArrayOutputStream();
                                        bbbb = "";
                                    }

                                    fragmentStreamer.updateView(packageMessage.getBytes(Charset.forName("US-ASCII")));
                                }

                                isLiveWeightCalculate = false;
                            }
                        }
                        break;
                    case STATE_BUSY:
                        //Release Device available
                        progressStateBusyReceiveData(new String(values[0], Charset.forName("US-ASCII")));
                        break;
                    default: // err or sync
                        Log.i("STATE NOT PROGRESS", currentTask + " - " + new String(values[0], Charset.forName("US-ASCII")));
                        break;
                }
            }
        }

        @Override
        protected void onCancelled() {
            Log.i("AsyncTask", "Cancelled.");
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                Log.i("AsyncTask", "onPostExecute: Completed with an Error.");
                filesFoundText.setText(getString(R.string.notify_failed));
                if (fragmentStreamer != null) {
                    fragmentStreamer.updateConnectStatus(getString(R.string.notify_failed));
                }
                disconnect();
                displayList(new ArrayList<Files_iScale>(), SocketTask.STATE_DISCONNECTED);
            } else {
                Log.i("AsyncTask", "onPostExecute: Completed.");
            }
        }

        // Check if device has been doing other actions before
        private void progressStateBusyReceiveData(String incoming) {
            if (incoming.charAt(0) == '#') {
                updateConnectStatus(SocketTask.STATE_IDLE);

                progressContinueRunningTask();
            }
        }

        // Checks if connection has been established before asking for file list
        private void progressStateConnectingReceiveData(String listValues) {
            Log.i("Identifier", listValues);
            // Check for initial connection response -> "*HELLO*"
            if (listValues.contains(Constants.Response)) {
                progressBar.setProgress(15);
                updateConnectStatus(SocketTask.STATE_IDLE);
                progressContinueRunningTask();
            } else {
                // Did not connect to iScale
                progressBar.setProgress(0); // reset progressbar
                displayList(new ArrayList<Files_iScale>(), SocketTask.STATE_DISCONNECTED);
                updateConnectStatus(SocketTask.STATE_DISCONNECTED);
                autoReconnect();
            }
        }

        //Continue running task
        private void progressContinueRunningTask() {
            switch (runningTask) {
                case STATE_RECORD:
                    if(listSelectedFile != null && !listSelectedFile.isEmpty()) {
                        requestContinueDownload();
                    } else {
                        progressChangePortModeAndGetListFile();
                    }
                    break;
                case STATE_LIVE_STREAMER:
                    requestChangePortMode(currentPortMode);
                    if(commandTest != null && !commandTest.equals("")) {
                        requestSendCommandTest(commandTest);
                    }
                    break;
                case STATE_DELETE:
                    if(listSelectedFile != null && !listSelectedFile.isEmpty()) {
                        requestContinueDelete();
                    } else {
                        progressChangePortModeAndGetListFile();
                    }
                    break;
                default:
                    //Check connection available
                    progressChangePortModeAndGetListFile();
                    break;
            }
        }

        private void progressChangePortModeAndGetListFile() {
            try {
                if (nsocket.isConnected()) {

                    socketBufferData = new ByteArrayOutputStream();
                    // Now that we are connected to the iScale, request file list
                    updateConnectStatus(SocketTask.STATE_FILELIST);
                    runningTask = SocketTask.STATE_IDLE;

                    nos.write(Constants.command_change_com_port);

                    progressBar.setProgress(30);

                    Thread.sleep(2000);

                    commandTimeout = new CommandTimeout(SocketTask.STATE_FILELIST);
                    handler.postDelayed(checkTimeOutRunnable, Constants.getConnectionTimeout(Activity_Parent.this));

                    isLoadFileListFinish = false;
                    listFileContent = "";
                    nos.write(Constants.command_file_list);

                    progressBar.setProgress(60);
                } else {
                    Log.i("AsyncTask", "SendDataToNetwork: Cannot send message. Socket is closed");
                    filesFoundText.setText(getString(R.string.notify_not_established));
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.i("AsyncTask", "SendDataToNetwork: Message send failed. Caught an exception");
                filesFoundText.setText(getString(R.string.notify_please_connect));

                displayList(new ArrayList<Files_iScale>(), SocketTask.STATE_DISCONNECTED);
            }
        }



        // Fills the visible file list in the app with the file names and number of records
        private boolean progressStateFileListReceiveData(String responseData) {
            // Record data may come in the form "Farm1,Farm2,Farm3,Farm4,#  3,#  6,# 10,#143,Farm5,Farm6,#  9,#  4"
            // There should always be the same number of file names and records. This single large string gets split into array lists
            listFileContent = responseData;

            String[] splitElement = listFileContent.split(",");
            List<String> listName = new ArrayList<>();
            List<String> listNumber = new ArrayList<>();

            for(String entry : splitElement) {
                if(entry == null || entry.equals("")) {
                    continue;
                }
                if(entry.startsWith("#")) {
                    listNumber.add(entry.replaceAll("[# ]", ""));
                } else {
                    listName.add(entry.trim());
                }
            }

            if(listNumber.size() == listName.size()) {
                if(isLoadFileListFinish) {
                    return true;
                }
                List<Files_iScale> listTemp = new ArrayList<>();
                for(int i = 0; i < listName.size(); i++) {
                    String filename = listName.get(i).trim();
                    String replaced = listNumber.get(i);

                    String fileDownloaded = filename + "." + Constants.FILE_TYPE_CSV;
                    boolean downloaded = checkLocalExistence(fileDownloaded);

                    Files_iScale files = new Files_iScale(i, filename, Constants.convertIntValue(replaced, 0), false, downloaded, fileDownloaded);

                    //Check select
                    boolean isSelect = false;
                    for(Files_iScale entry : listDeviceFile) {
                        if(entry.getName().equals(filename)) {
                            isSelect = entry.isSelected();
                            break;
                        }
                    }

                    files.setSelected(isSelect);

                    listTemp.add(files);
                }
                listDeviceFile = new ArrayList<>(listTemp);
                commandTimeout = null;
                if(!isLoadFileListFinish) {
                    isLoadFileListFinish = true;

                    //handler.postDelayed(finalActionGetListFile, 6000L);
                    handler.postDelayed(finalActionGetListFile, 0L);
                }
                return true;
            }
            return false;
        }

        private void requestContinueDownload() {
            fileScaleAdapter = new ListAdapter(Activity_Parent.this, R.layout.checkbox_subtext, listDeviceFile);
            listView.setAdapter(fileScaleAdapter);

            filesFoundText.setText(getString(R.string.s_download_record, num_downloaded + "", toDownload + ""));
            progressBar.setProgress(num_downloaded);
            progressBar.setMax(toDownload);

            download_index = listSelectedFile.get(0).getIndex();
            currentFileName = listSelectedFile.get(0).getName();
            counter = 0;
            socketBufferData = new ByteArrayOutputStream();
            request_record((byte) download_index, false);
        }

        private void requestContinueDelete() {
            fileScaleAdapter = new ListAdapter(Activity_Parent.this, R.layout.checkbox_subtext, listDeviceFile);
            listView.setAdapter(fileScaleAdapter);

            filesFoundText.setText(getString(R.string.s_delete_file, num_downloaded + "", toDownload + ""));
            progressBar.setProgress(num_downloaded);
            progressBar.setMax(toDownload);
        }

        // Initiates the record download sequence
        private void initiateDownload(boolean isMergeFile, String saveAsName, ArrayList<Files_iScale> lstSelectFile, int to_download) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Activity_Parent.this);

            removeNonEid = prefs.getBoolean("removeNonEid", false);

            delimiterCharacter = prefs.getString("csvDelimiter", ",");

            listSelectedFile = lstSelectFile;

            mergeFile = isMergeFile;
            saveFileName = saveAsName;

            num_selected = listSelectedFile.size();
            toDownload = to_download;
            num_downloaded = 0;


            listRecord = new ArrayList<>();
            currentRecord = new ArrayList<>();

            String headerTitle = getString(R.string.csv_title_bar);
            if(!delimiterCharacter.equals(",")) {
                headerTitle = headerTitle.replaceAll(",", delimiterCharacter);
            }
            listRecord.add(headerTitle);
            currentRecord.add(headerTitle);

            updateConnectStatus(SocketTask.STATE_RECORD);
            runningTask = SocketTask.STATE_RECORD;

            progressBar.setProgress(0);
            progressBar.setMax(toDownload);

            if(listSelectedFile.size() > 1) {
                multiFile = true;
            } else {
                multiFile = false;
            }

            download_index = listSelectedFile.get(0).getIndex();
            currentFileName = listSelectedFile.get(0).getName();

            counter = 0;
            socketBufferData = new ByteArrayOutputStream();
            request_record((byte) download_index, true);
        }

        private void initiateDeleteFile(ArrayList<Files_iScale> lstSelectFile, int to_download) {
            listSelectedFile = lstSelectFile;

            num_selected = listSelectedFile.size();
            toDownload = to_download;
            num_downloaded = 0;

            updateConnectStatus(SocketTask.STATE_DELETE);
            runningTask = SocketTask.STATE_DELETE;

            progressBar.setProgress(0);
            progressBar.setMax(toDownload);

            download_index = listSelectedFile.get(listSelectedFile.size() - 1).getIndex();
            request_delete((byte) download_index, true);
        }

        private void delete(byte[] rawData) {
            // If 5 consecutive e's are present, the end of the file is reached
            String result = Character.toString((char) rawData[0]);

            if (result.equals("Y")) {
                num_downloaded++;
                int position = listSelectedFile.remove(listSelectedFile.size() - 1).getIndex();
                listDeviceFile.remove(position);

                if (listSelectedFile.size() > 0) {
                    // Move on to the next record in the queue by deleting this entry from the array and proceed till array empty
                    download_index = listSelectedFile.get(listSelectedFile.size() - 1).getIndex();

                    filesFoundText.setText(getString(R.string.s_delete_file, num_downloaded + "", toDownload + ""));

                    request_delete((byte) download_index, false);
                } else {
                    alertDeleteComplete();
                }
            }
        }

        // Download file queue method. Continues to ask for more records till the end of file is reached then closes the writer
        private void download(byte[] rawData) {
            Log.i("Download", rawData.length + " - " + new String(rawData, Charset.forName("US-ASCII")));

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Activity_Parent.this);
            String eidType = prefs.getString("eidFormat", Constants.EID_TYPE_SPACE);

            // If 5 consecutive e's are present, the end of the file is reached
            if (testEOF(rawData)) {
                int position = listSelectedFile.get(0).getIndex();
                //if(currentRecord.size() >= listSelectedFile.get(0).getCode()) {
                //Apply for remomove EID
                if(true) {
                    isRetry = false;
                    //Valid
                    //Save File
                    if (mergeFile) {
                        //Continue store together
                    } else {
                        String fileName = fileScaleAdapter.getFileNameAt(position);
                        Log.i("Save File name", fileName);
                        String dataFile = "";
                        for(String row : currentRecord) {
                            row = row.replaceAll("\n", "");
                            dataFile += row + "\n";
                        }
                        Log.i("DataFile", dataFile);
                        writeToFile(fileName + "." + Constants.FILE_TYPE_CSV, dataFile);

                        fileScaleAdapter.updateDownloaded(position, fileName + "." + Constants.FILE_TYPE_CSV);

                    }

                    currentRecord = new ArrayList<>();
                    String headerTitle = getString(R.string.csv_title_bar);
                    if(!delimiterCharacter.equals(",")) {
                        headerTitle = headerTitle.replaceAll(",", delimiterCharacter);
                    }
                    currentRecord.add(headerTitle);

                    listSelectedFile.remove(0).getIndex();

                    if (listSelectedFile.size() > 0) {
                        // Move on to the next record in the queue by deleting this entry from the array and proceed till array empty
                        download_index = listSelectedFile.get(0).getIndex();
                        currentFileName = listSelectedFile.get(0).getName();


                        counter = 0;
                        socketBufferData = new ByteArrayOutputStream();
                        request_record((byte) download_index, false);
                    } else {
                        alertDownloadComplete(); // No more files queued for download, chain complete
                    }
                } else {
                    //Invalid, miss record, redownload
                    if(!isRetry) {
                        isRetry = true;
                        download_index = listSelectedFile.get(0).getIndex();
                        currentFileName = listSelectedFile.get(0).getName();

                        counter = 0;
                        socketBufferData = new ByteArrayOutputStream();
                        request_record((byte) download_index, false);
                    } else {
                        //Show error
                        Toast.makeText(Activity_Parent.this, getString(R.string.download_file_error_reconnect_again), Toast.LENGTH_SHORT).show();
                    }
                }
            } else if (rawData == null) {
                Log.i("", "onPreExecute");
            } else {
                // Decode the raw byte data received and construct usable csv data then write to file.
                DecodeRecord decode = new DecodeRecord();

                String rowRecord = decode.constructRecord(rawData, removeNonEid, delimiterCharacter, eidType);

                // Change the file name starting position
                int startPos = 0;       // old structure
                if (dataId == 1){
                    startPos = 2;       // new structure
                }
                String receiveFileName = decode.get_info(rawData, startPos, 17).trim();

                if(receiveFileName.equals(currentFileName) && !currentRecord.contains(rowRecord)) {
                    if (!rowRecord.equals("")) {
                        //csv_content += rowRecord + "\n";
                        listRecord.add(rowRecord);
                        currentRecord.add(rowRecord);

                        num_downloaded++; // Increment records downloaded so progressbar can update
                    }
                    filesFoundText.setText(getString(R.string.s_download_record, num_downloaded + "", toDownload + ""));
                    progressBar.setProgress(num_downloaded);
                } else {
                    Log.i("DUPLICATE RECORD", receiveFileName + "-" + currentFileName + "-" + currentRecord + "-" + rowRecord);
                }

                Log.i("Row Data", "xxx: " + rowRecord);
                Log.i("Current File Name", currentFileName);
                Log.i("Receive File Name", receiveFileName);


                counter = 0;
                socketBufferData = new ByteArrayOutputStream();
                request_record((byte) download_index, false);
            }
        }

        // pop an alert if download complete
        private void alertDownloadComplete() {
            String fileNameWithExt = saveFileName + "." + Constants.FILE_TYPE_CSV;
            if (mergeFile) {
                String dataFile = "";
                for(String row : listRecord) {
                    row = row.replaceAll("\n", "");
                    dataFile += row + "\n";
                }
                writeToFile(fileNameWithExt, dataFile);
            } else if (multiFile) {
                fileNameWithExt += ",...";
            }
            updateConnectStatus(SocketTask.STATE_IDLE);
            runningTask = SocketTask.STATE_IDLE;
            filesFoundText.setText(getString(R.string.s_number_download_record, toDownload + ""));
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Activity_Parent.this);
            alertDialogBuilder.setTitle(getString(R.string.download_complete));
            alertDialogBuilder
                    .setMessage(getString(R.string.s_record_save_with, fileNameWithExt))
                    .setCancelable(false)
                    .setNeutralButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }

        private void alertDeleteComplete() {
            updateConnectStatus(SocketTask.STATE_IDLE);
            filesFoundText.setText(getString(R.string.s_number_delete_file, toDownload + ""));
            Toast.makeText(Activity_Parent.this, getString(R.string.s_number_delete_file, toDownload), Toast.LENGTH_SHORT).show();
            fileScaleAdapter.notifyDataSetChanged();
        }

        // check for end of file -> string of e's
        private boolean testEOF(byte[] rawData) {
            String testStart = "";
            for (int x = 0; x <= 4; x++) {
                if ((rawData[x] != 0) || (rawData[x] != 255)) {
                    char c = (char) rawData[x];
                    testStart += Character.toString(c);
                }
            }
            return testStart.equals("eeeee");
        }

    }

    // List Adapter
    private class ListAdapter extends ArrayAdapter<Files_iScale> {
        List<Files_iScale> filesList;
        int resource;
        LayoutInflater inflater;

        public ListAdapter(Context context, int resource, List<Files_iScale> filesList) {
            super(context, resource, filesList);
            this.resource = resource;
            this.filesList = filesList;
            inflater = LayoutInflater.from(Activity_Parent.this);
        }

        private class ViewHolder {
            // [Checkbox] NAME (code)
            CheckBox iScale_chkbox;
            TextView iScale_file;
            TextView code;
            ImageView iScaleDetail;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            final ViewHolder holder;
            Files_iScale file = filesList.get(position);
            if (convertView == null) {
                holder = new ViewHolder();
                view = inflater.inflate(resource, parent, false);
                holder.iScale_chkbox = (CheckBox) view.findViewById(R.id.iScale_chkbox);
                holder.iScale_file = (TextView) view.findViewById(R.id.iScale_file);
                holder.code = (TextView) view.findViewById(R.id.code);
                holder.iScaleDetail = (ImageView) view.findViewById(R.id.iScaleDetail);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            holder.iScale_file.setText(file.getName());
            holder.code.setText(getString(R.string.s_number_record, file.getCode() + ""));
            holder.iScale_chkbox.setChecked(file.isSelected());
            holder.iScale_chkbox.setTag(file);

            if (file.isDownloaded()) {
                holder.iScaleDetail.setVisibility(View.VISIBLE);
                holder.iScaleDetail.setTag(file.getFileDownloaded());
            } else {
                holder.iScaleDetail.setVisibility(View.INVISIBLE);
            }

            holder.iScale_chkbox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // Checks to see if an checkbox is selected
                    CheckBox cb = (CheckBox) v;
                    Files_iScale file = (Files_iScale) cb.getTag();
                    file.setSelected(cb.isChecked());
                }
            });

            holder.iScaleDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    csv_filename = view.getTag().toString();
                    showFragment(new Fragment_Summary(), "fragment_summary");
                }
            });
            return view;
        }

        public void updateDownloaded(int position, String fileName) {
            Files_iScale fileIScale = filesList.get(position);

            if ((fileIScale.getName() + "." + Constants.FILE_TYPE_CSV).equals(fileName)) {
                fileIScale.setDownloaded(true);
                fileIScale.setFileDownloaded(fileName);
            } else {
                fileIScale.setDownloaded(false);
            }
            notifyDataSetChanged();
        }

        public String getFileNameAt(int position) {
            Files_iScale fileIScale = filesList.get(position);
            return fileIScale.getName();
        }
    }

    /**
     * Upload Function
     */
    public UploadModel uploadModel;
    public void onUploadClick(View view) {
        String upload_location = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/";
        File directory = new File(upload_location);
        if(!directory.exists()) {
            directory.mkdirs();
            Log.i("Upload", "Clear Directory TePariUpload");
        }

        /*try {

            FileWriter writer = new FileWriter(upload_location + "upload.csv");
            writer.write("eid,weight,field1,field2,field3,field4,field5,field6,field7,field8,field9,field10\n111,222,333");
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        File mPath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/");
        FileDialog fileDialog = new FileDialog(this, mPath);
        fileDialog.setFileEndsWith(".csv");
        fileDialog.addFileListener(new FileDialog.FileSelectedListener() {
            public void fileSelected(File file) {
                readUploadFile(file);
            }
        });
        fileDialog.showDialog();
    }

    private void readUploadFile(File file) {
        try {
            uploadModel = null;
            String fileName = file.getName();
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            int count = 0;

            String columnName = "";
            String columnValue = "";

            while ((line = br.readLine()) != null) {
                if(count == 0) {
                    Log.i("CSV", "Skip Header: " + line);
                    columnName = line;
                    columnName = columnName.replaceAll(";", ",");
                    columnName = columnName.replaceAll(":", ",");
                    count++;
                    continue;
                }
                if(line.trim().equals("")) {
                    continue;
                }

                columnValue = line;
                columnValue = columnValue.replaceAll(";", ",");
                columnValue = columnValue.replaceAll(":", ",");

                String[] field = line.split(",");
                if(field.length <= 2) {
                    field = line.split(";");
                }
                if(field.length <= 2) {
                    field = line.split(":");
                }

                String eid = field[0];
                String weight = "";
                String field1 = "";
                String field2 = "";
                String field3 = "";
                String field4 = "";
                String field5 = "";
                String field6 = "";
                String field7 = "";
                String field8 = "";
                String field9 = "";
                String field10 = "";
                if(field.length > 1) {
                    weight = field[1];
                }
                if(field.length > 2) {
                    field1 = field[2];
                }
                if(field.length > 3) {
                    field2 = field[3];
                }
                if(field.length > 4) {
                    field3 = field[4];
                }
                if(field.length > 5) {
                    field4 = field[5];
                }
                if(field.length > 6) {
                    field5 = field[6];
                }
                if(field.length > 7) {
                    field6 = field[7];
                }
                if(field.length > 8) {
                    field7 = field[8];
                }
                if(field.length > 9) {
                    field8 = field[9];
                }
                if(field.length > 10) {
                    field9 = field[10];
                }
                if(field.length > 11) {
                    field10 = field[11];
                }

                uploadModel = new UploadModel(fileName, eid);
                uploadModel.setWeight(weight);
                uploadModel.setField1(field1);
                uploadModel.setField2(field2);
                uploadModel.setField3(field3);
                uploadModel.setField4(field4);
                uploadModel.setField5(field5);
                uploadModel.setField6(field6);
                uploadModel.setField7(field7);
                uploadModel.setField8(field8);
                uploadModel.setField9(field9);
                uploadModel.setField10(field10);

                uploadModel.setListColumnName(columnName);
                uploadModel.setListColumnValue(columnValue);

                showUploadContent(uploadModel);
                br.close();
                return;
            }
            br.close();

            if(uploadModel == null) {
                Toast.makeText(this, getString(R.string.toast_invalid_format), Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showUploadContent(UploadModel uploadModel) {
        showFragment(new FragmentUpload(), "fragment_upload");
    }

    boolean goneFlag = false;

    //Put this into the class
    final Handler handlerLog = new Handler();
    Runnable mLongPressed = new Runnable() {
        public void run() {
            goneFlag = true;
            //Code for long click
            sendLogFile();
        }
    };

    public void sendLogFile() {
        File file = extractLogToFileAndWeb();
        ArrayList<Uri> uris = new ArrayList<>();

        Intent email = new Intent(Intent.ACTION_SEND_MULTIPLE);
        //    Make sure we're running on O or higher to use FileProvider APIs
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            uris.add(FileProvider.getUriForFile(Activity_Parent.this, "com.tepari.tpc.provider", file));
            email.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        else{
            uris.add(Uri.fromFile(file));
        }
        email.putExtra(Intent.EXTRA_EMAIL, new String[] { "hovanke@gmail.com" });
        email.setData(Uri.parse("hovanke@gmail.com"));
        email.putExtra(Intent.EXTRA_SUBJECT, "LogFile iScale");
        email.setType("text/plain");
        email.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        try {
            startActivity(Intent.createChooser(email, getString(R.string.notify_email_choose)));
        } catch (ActivityNotFoundException exception) {
            Toast.makeText(this, getString(R.string.toast_email_err), Toast.LENGTH_LONG).show();
        }
    }

    public File extractLogToFileAndWeb(){
        //set a file
        Date datum = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String fullName = df.format(datum)+"appLog.log";
        File file = new File (Environment.getExternalStorageDirectory(), fullName);

        //clears a file
        if(file.exists()){
            file.delete();
        }


        //write log to file
        int pid = android.os.Process.myPid();
        try {
            String command = String.format("logcat -d -v threadtime *:*");
            Process process = Runtime.getRuntime().exec(command);

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            StringBuilder result = new StringBuilder();
            String currentLine = null;

            while ((currentLine = reader.readLine()) != null) {
                if (currentLine != null && currentLine.contains(String.valueOf(pid))) {
                    result.append(currentLine);
                    result.append("\n");
                }
            }

            FileWriter out = new FileWriter(file);
            out.write(result.toString());
            out.close();

            //Runtime.getRuntime().exec("logcat -d -v time -f "+file.getAbsolutePath());
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }


        //clear the log
        try {
            Runtime.getRuntime().exec("logcat -c");
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }

        return file;
    }
}