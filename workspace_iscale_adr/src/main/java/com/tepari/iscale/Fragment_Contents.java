package com.tepari.iscale;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tepari.tpc.R;


public class Fragment_Contents extends Fragment {

    public static final String FRAGMENT_ID = "fragment_contents";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contents, container, false);
        ((Activity_Parent) getActivity()).actionBar.setTitle(getString(R.string.app_name));

        TextView title = (TextView) rootView.findViewById(R.id.title);
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Aachen Bold Plain.ttf");
        title.setTypeface(font);

        ImageButton btn_connect = (ImageButton) rootView.findViewById(R.id.btn_connect);
        btn_connect.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                ((Activity_Parent) getActivity()).showFragment(new Fragment_Download(), Fragment_Download.FRAGMENT_ID);
            }
        });

        ImageButton btn_view = (ImageButton) rootView.findViewById(R.id.btn_view);
        btn_view.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                ((Activity_Parent) getActivity()).generateCsvFromDatabase();
                ((Activity_Parent) getActivity()).showFragment(new Fragment_Records(), Fragment_Records.FRAGMENT_ID);
            }
        });

        ImageButton btn_liveStream = (ImageButton) rootView.findViewById(R.id.btn_liveStream);
        btn_liveStream.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                ((Activity_Parent) getActivity()).confirmWeightOptionMode();
            }
        });

        ImageButton btn_trouble = (ImageButton) rootView.findViewById(R.id.btn_trouble);
        btn_trouble.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                ((Activity_Parent) getActivity()).showFragment(new Fragment_Help(), Fragment_Help.FRAGMENT_ID);
            }
        });

//        Tracker t = ((TePari_Application) getActivity().getApplication()).getTracker(TePari_Application.TrackerName.APP_TRACKER);
//        t.setScreenName("Contents");
//        t.send(new HitBuilders.AppViewBuilder().build());

//        TePari_Application.getFirebaseAnalytics().setCurrentScreen(getActivity(), "Contents", null);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "Contents", null);

        TextView txtVersion = (TextView) rootView.findViewById(R.id.txtVersion);
        String versionName = "1.0";
        try {
            versionName = getActivity().getPackageManager()
                    .getPackageInfo(getActivity().getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        txtVersion.setText(getString(R.string.version_copyright, versionName));

        return rootView;
    }



}