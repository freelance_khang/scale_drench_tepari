package com.tepari.iscale;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tepari.iscale.Helper.Constants;
import com.tepari.iscale.Helper.Files_Content;
import com.tepari.iscale.database.TepariDatabase;
import com.tepari.iscale.model.UploadModel;
import com.tepari.tpc.R;

import java.util.ArrayList;
import java.util.List;


public class FragmentUpload extends Fragment {

    private TextView btnSave;

    private EditText txtFileName;

    private ListView list_csv_title;
    private ListView list_csv;

    private TextView txtEid;


    public FragmentUpload() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upload, container, false);
        btnSave = (TextView) view.findViewById(R.id.btnSave);
        txtFileName = (EditText) view.findViewById(R.id.txtFileName);

        list_csv_title = (ListView) view.findViewById(R.id.list_csv_title);
        list_csv = (ListView) view.findViewById(R.id.list_csv);

        txtEid = (TextView) view.findViewById(R.id.txtEid);


        displayUploadContent();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSaveFileClick();
            }
        });

        return view;
    }

    private void displayUploadContent() {
        UploadModel uploadModel = ((Activity_Parent) getActivity()).uploadModel;
        txtFileName.setText(uploadModel.getFileName());
        txtEid.setText(uploadModel.getEid());



        ArrayList<String[]> fst = new ArrayList<>();
        String[] header = uploadModel.getListColumnName().split(",");
        fst.add(header);

        ListAdapter_Csv dataAdapter;
        dataAdapter = new ListAdapter_Csv(getActivity(), R.layout.list_upload, fst, true);
        list_csv_title.setAdapter(dataAdapter);

        //For Value
        ArrayList<String[]> entry = new ArrayList<>();
        String[] value = uploadModel.getListColumnValue().split(",");
        entry.add(value);

        dataAdapter = new ListAdapter_Csv(getActivity(), R.layout.list_upload, entry, false);
        list_csv.setAdapter(dataAdapter);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    private void onSaveFileClick() {
        String fileName = txtFileName.getText().toString();
        String eid = txtEid.getText().toString();
        if(fileName == null || fileName.trim().length() == 0 || eid == null || eid.trim().length() == 0) {
            Toast.makeText(getActivity(), getString(R.string.toast_invalid_format), Toast.LENGTH_LONG).show();
            return;
        }

        UploadModel uploadModel = ((Activity_Parent) getActivity()).uploadModel;

        TepariDatabase tepariDatabase = new TepariDatabase(getActivity());
        tepariDatabase.saveTepariFile(fileName, uploadModel.getListColumnName(), uploadModel.getListColumnValue(), eid);

        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new Fragment_Contents())
                .commit();
    }


    private class ListAdapter_Csv extends ArrayAdapter<String[]> {
        int resource;
        List<String[]> fileList;
        boolean title_bar;
        LayoutInflater inflater;

        public ListAdapter_Csv(Context context, int resource, List<String[]> fileList, boolean title_bar) {
            super(context, resource, fileList);
            this.resource = resource;
            this.fileList = fileList;
            this.title_bar = title_bar;
            inflater = LayoutInflater.from(getActivity());
        }

        private class ViewHolder {
            LinearLayout csv_bkgnd;

            TextView field0;
            TextView field1;
            TextView field2;
            TextView field3;
            TextView field4;
            TextView field5;
            TextView field6;
            TextView field7;
            TextView field8;
            TextView field9;
            TextView field10;
            TextView field11;


        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            final ViewHolder holder;
            String[] file = fileList.get(position);
            if (convertView == null) {
                holder = new ViewHolder();
                view = inflater.inflate(resource, parent, false);
                holder.csv_bkgnd = (LinearLayout) view.findViewById(R.id.csv_bkgnd);

                holder.field0 = (TextView) view.findViewById(R.id.csv_00);
                holder.field1 = (TextView) view.findViewById(R.id.csv_01);
                holder.field2 = (TextView) view.findViewById(R.id.csv_02);
                holder.field3 = (TextView) view.findViewById(R.id.csv_03);
                holder.field4 = (TextView) view.findViewById(R.id.csv_04);
                holder.field5 = (TextView) view.findViewById(R.id.csv_05);
                holder.field6 = (TextView) view.findViewById(R.id.csv_06);
                holder.field7 = (TextView) view.findViewById(R.id.csv_07);
                holder.field8 = (TextView) view.findViewById(R.id.csv_08);
                holder.field9 = (TextView) view.findViewById(R.id.csv_09);
                holder.field10 = (TextView) view.findViewById(R.id.csv_10);
                holder.field11 = (TextView) view.findViewById(R.id.csv_11);

                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            if (title_bar) {
                holder.csv_bkgnd.setBackgroundColor(getResources().getColor(R.color.tepari_blue));
                int col = getResources().getColor(R.color.tepari_white);
                holder.field0.setTextColor(col);
                holder.field1.setTextColor(col);
                holder.field2.setTextColor(col);
                holder.field3.setTextColor(col);
                holder.field4.setTextColor(col);
                holder.field5.setTextColor(col);
                holder.field6.setTextColor(col);
                holder.field7.setTextColor(col);
                holder.field8.setTextColor(col);
                holder.field9.setTextColor(col);
                holder.field10.setTextColor(col);
                holder.field11.setTextColor(col);

            } else {
                int col = (position % 2 == 1) ? getResources().getColor(R.color.tepari_csv_grey) : getResources().getColor(R.color.tepari_white);
                holder.csv_bkgnd.setBackgroundColor(col);
            }

            if(file != null) {
                if(file.length > 0) {
                    holder.field0.setText(file[0]);
                    holder.field0.setVisibility(View.VISIBLE);
                }
                if(file.length > 1) {
                    holder.field1.setText(file[1]);
                    holder.field1.setVisibility(View.VISIBLE);
                }
                if(file.length > 2) {
                    holder.field2.setText(file[2]);
                    holder.field2.setVisibility(View.VISIBLE);
                }
                if(file.length > 3) {
                    holder.field3.setText(file[3]);
                    holder.field3.setVisibility(View.VISIBLE);
                }
                if(file.length > 4) {
                    holder.field4.setText(file[4]);
                    holder.field4.setVisibility(View.VISIBLE);
                }
                if(file.length > 5) {
                    holder.field5.setText(file[5]);
                    holder.field5.setVisibility(View.VISIBLE);
                }
                if(file.length > 6) {
                    holder.field6.setText(file[6]);
                    holder.field6.setVisibility(View.VISIBLE);
                }
                if(file.length > 7) {
                    holder.field7.setText(file[7]);
                    holder.field7.setVisibility(View.VISIBLE);
                }
                if(file.length > 8) {
                    holder.field8.setText(file[8]);
                    holder.field8.setVisibility(View.VISIBLE);
                }
                if(file.length > 9) {
                    holder.field9.setText(file[9]);
                    holder.field9.setVisibility(View.VISIBLE);
                }
                if(file.length > 10) {
                    holder.field10.setText(file[10]);
                    holder.field10.setVisibility(View.VISIBLE);
                }

                if(file.length > 11) {
                    holder.field11.setText(file[11]);
                    holder.field11.setVisibility(View.VISIBLE);
                }
            }


            return view;
        }
    }
}
