package com.tepari.iscale;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.FileProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tepari.iscale.Helper.Constants;
import com.tepari.iscale.Helper.Files_Advance;
import com.tepari.iscale.Helper.Files_Content;
import com.tepari.tpc.R;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class Fragment_Advance extends Fragment {

    public static final String FRAGMENT_ID = "fragment_advance";

    ViewGroup contn;
    ListView list_csv_title;
    ListView list_csv;

    private ArrayList<Files_Advance> lst_content = new ArrayList<>();

    private LinearLayout chartContainer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_advance, container, false);
        setHasOptionsMenu(true);
        contn = container;

        String filename = ((Activity_Parent) getActivity()).csv_filename;
        ((Activity_Parent) getActivity()).actionBar.setTitle(filename);

        list_csv_title = (ListView) rootView.findViewById(R.id.list_csv_title);
        list_csv = (ListView) rootView.findViewById(R.id.list_csv);

        chartContainer = (LinearLayout)rootView.findViewById(R.id.chartContainer);

        displaySummaryRecord();

        TextView btn_email = (TextView) rootView.findViewById(R.id.btn_email);
        btn_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailFiles(((Activity_Parent) getActivity()).csv_filename);
            }
        });

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.menu_csv, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_rename_file:
                alertRenameFile(contn);
                return true;
            case R.id.menu_delete_single:
                confirmDelete(((Activity_Parent) getActivity()).csv_filename);
                return true;
            case R.id.menu_open_external:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                File externalFile = new File(((Activity_Parent) getActivity()).file_location + ((Activity_Parent) getActivity()).csv_filename);
                Uri path;
                //    Make sure we're running on O or higher to use FileProvider APIs
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    path = FileProvider.getUriForFile((Activity_Parent) getActivity(), "com.tepari.tpc.provider", externalFile);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                else{
                    path = Uri.fromFile(externalFile);
                }
                intent.setDataAndType(path, "application/vnd.ms-excel");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getActivity(), getString(R.string.notify_spreadsheets), Toast.LENGTH_SHORT).show();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        displaySummaryRecord();

    }

    private void displaySummaryRecord() {
        ArrayList<Files_Content> listContentFile = ((Activity_Parent) getActivity()).readContentFile();

        HashMap<String, Files_Advance> mapValues = new HashMap<>();

        for(Files_Content fileContent : listContentFile) {
            // Display in either kg or lb
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            String unitType = prefs.getString("unit", Constants.UNIT_TYPE_KG);
            double weight = 0;
            if (unitType.equals(Constants.UNIT_TYPE_KG)) {
                weight = Constants.convertDoubleValue(fileContent.getWeightkg());
            }
            else if (unitType.equals(Constants.UNIT_TYPE_LB)) {
                weight = Constants.convertDoubleValue(fileContent.getWeightlb());
            }
            String draft = fileContent.getDraft();
            if(draft == null || draft.equals("") || draft.equals("0")) {
                draft = getString(R.string.none_or_zero);
            }

            Files_Advance record = mapValues.get(draft);

            if(record == null) {
                record = new Files_Advance(draft, "1", String.valueOf(weight), String.valueOf(weight));
                mapValues.put(draft, record);
            } else {
                int count = Integer.valueOf(record.getCount()) + 1;
                double finalWeightTotal = Double.valueOf(record.getTotalWeight()) + weight;
                double finalWeightAverage = finalWeightTotal / count;
                record.setCount(String.valueOf(count));
                record.setTotalWeight(String.valueOf(finalWeightTotal));
                record.setAverageWeight(String.valueOf(finalWeightAverage));
            }
        }

        lst_content = new ArrayList<>(mapValues.values());

        //Sort list_content
        Collections.sort(lst_content, new Comparator<Files_Advance>() {
            public int compare(Files_Advance o1, Files_Advance o2) {
                int value1 = Constants.convertIntValue(o1.getGateNumber());
                int value2 = Constants.convertIntValue(o2.getGateNumber());
                return value1 - value2;
            }
        });

        ArrayList<Files_Advance> fst = new ArrayList<>();
        String[] rec = (getString(R.string.csv_title_advance_bar).replaceAll("\n", "")).split(",");
        Files_Advance filesAdvance = new Files_Advance(rec[0], rec[1], rec[2], rec[3]);
        fst.add(filesAdvance);

        ListAdapter_Csv dataAdapter;
        dataAdapter = new ListAdapter_Csv(getActivity(), R.layout.list_advance, fst, true);
        list_csv_title.setAdapter(dataAdapter);
        dataAdapter = new ListAdapter_Csv(getActivity(), R.layout.list_advance, lst_content, false);
        list_csv.setAdapter(dataAdapter);

        openChart(lst_content);
    }

    // Email selected files
    private void emailFiles(String filename) {
        ArrayList<Uri> uris = new ArrayList<>();
        ((Activity_Parent) getActivity()).generateAdvanceTemp("advance_" + filename, lst_content);

        Intent email = new Intent(Intent.ACTION_SEND_MULTIPLE);
        //    Make sure we're running on O or higher to use FileProvider APIs
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            uris.add(FileProvider.getUriForFile((Activity_Parent) getActivity(), "com.tepari.tpc.provider", new File(((Activity_Parent) getActivity()).exportFileLocation + "advance_" + filename)));
            email.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        else{
            uris.add(Uri.fromFile(new File(((Activity_Parent) getActivity()).exportFileLocation + "advance_" + filename)));
        }
        email.setType("text/plain");
        email.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        try {
            startActivity(Intent.createChooser(email, getString(R.string.notify_email_choose)));
        } catch (ActivityNotFoundException exception) {
            Toast.makeText(getActivity(), getString(R.string.toast_email_err), Toast.LENGTH_LONG).show();
        }
    }

    // Deletes the currently opened file and returns to ui_records intent
    private void confirmDelete(String filename) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder
                .setTitle(getString(R.string.delete_confirm))
                .setMessage(getString(R.string.s_delete_file_message, filename))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String filename = ((Activity_Parent) getActivity()).csv_filename;
                                File fileToDelete = new File(((Activity_Parent) getActivity()).file_location + filename);
                                boolean deleted = fileToDelete.delete();
                                if (deleted) {
                                    Toast.makeText(getActivity(), getString(R.string.s_filename_delete, filename), Toast.LENGTH_SHORT).show();
                                    ((Activity_Parent) getActivity()).getFiles();
                                    getActivity().getFragmentManager().popBackStack("fragment_summary", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                } else {
                                    Toast.makeText(getActivity(), getString(R.string.s_unable_to_delete_file, filename), Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    // Alert to ask for file rename
    private void alertRenameFile(final ViewGroup contn) {
        LayoutInflater li = LayoutInflater.from(getActivity());
        View promptsView = li.inflate(R.layout.alert_rename, contn, false); // set alert_rename.xml to alert dialog builder
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptsView);
        final EditText userInput = (EditText) promptsView.findViewById(R.id.editTextDialogUserInput);
        alertDialogBuilder
                .setTitle(getString(R.string.rename_file))
                        .setMessage(getString(R.string.alert_rename_as))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.rename), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (((Activity_Parent) getActivity()).csv_filename.contains(".csv")) {
                                    String newFilename = userInput.getText().toString() + ".csv";
                                    ((Activity_Parent) getActivity()).checkLocalExistence(newFilename);
                                    if (((Activity_Parent) getActivity()).checkLocalExistence(newFilename)) {
                                        Toast.makeText(getActivity(), getString(R.string.toast_file_exist), Toast.LENGTH_LONG).show();
                                    } else {
                                        renameFile(newFilename);
                                    }
                                } else if (((Activity_Parent) getActivity()).csv_filename.contains(".minda")) {
                                    String newFilename = userInput.getText().toString() + ".minda";
                                    ((Activity_Parent) getActivity()).checkLocalExistence(newFilename);
                                    if (((Activity_Parent) getActivity()).checkLocalExistence(newFilename)) {
                                        Toast.makeText(getActivity(), getString(R.string.toast_file_exist), Toast.LENGTH_LONG).show();
                                    } else {
                                        renameFile(newFilename);
                                    }
                                } else {
                                    Toast.makeText(getActivity(), getString(R.string.toast_invalid_name), Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        // create alert dialog and show it
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    // Rename file
    private void renameFile(String newFilename) {
        File from = new File(((Activity_Parent) getActivity()).file_location, ((Activity_Parent) getActivity()).csv_filename);
        File to = new File(((Activity_Parent) getActivity()).file_location, newFilename);
        if (from.renameTo(to)) {
            ((Activity_Parent) getActivity()).csv_filename = newFilename;
            ((Activity_Parent) getActivity()).actionBar.setTitle(((Activity_Parent) getActivity()).csv_filename);
        } else {
            Toast.makeText(getActivity(), getString(R.string.toast_error_rename_file), Toast.LENGTH_LONG).show();
        }
    }

    private void openChart(ArrayList<Files_Advance> listChart ){
        if(listChart == null || listChart.isEmpty()) {
            return;
        }

        // Pie Chart Section Names
        String[] code = new String[listChart.size()];
        // Pie Chart Section Value
        double[] distribution = new double[listChart.size()];

        for(int i = 0; i < listChart.size(); i++) {
            code[i] = "G" + listChart.get(i).getGateNumber();
            distribution[i] = Double.valueOf(listChart.get(i).getCount());
        }
        // Color of each Pie Chart Sections
        int[] colors = { Color.BLUE, Color.MAGENTA, Color.GREEN, Color.CYAN, Color.RED,
                Color.YELLOW };

        // Instantiating CategorySeries to plot Pie Chart
        CategorySeries distributionSeries = new CategorySeries("IScale 2015");
        for(int i=0 ;i < distribution.length;i++){
            // Adding a slice with its values and name to the Pie Chart
            distributionSeries.add(code[i], distribution[i]);
        }

        NumberFormat integerFormatter = new DecimalFormat("###,###,###.##");
        // Instantiating a renderer for the Pie Chart
        DefaultRenderer defaultRenderer  = new DefaultRenderer();
        for(int i = 0 ;i<distribution.length;i++){
            SimpleSeriesRenderer seriesRenderer = new SimpleSeriesRenderer();
            int indexColor = i % 6;
            seriesRenderer.setColor(colors[indexColor]);
            seriesRenderer.setChartValuesFormat(integerFormatter);



            // Adding a renderer for a slice
            defaultRenderer.addSeriesRenderer(seriesRenderer);
        }

        defaultRenderer.setChartTitle("Count");
        defaultRenderer.setChartTitleTextSize(20);
        defaultRenderer.setLabelsColor(Color.BLACK);
        defaultRenderer.setLabelsTextSize(16);
        defaultRenderer.setLegendTextSize(16);
        //defaultRenderer.setZoomButtonsVisible(true);

        // remove any views before u paint the chart

        //chartContainer.removeAllViews();

        // drawing pie chart

        GraphicalView mChart = ChartFactory.getPieChartView(getActivity(),

                distributionSeries, defaultRenderer);

        // adding the view to the linearlayout

        chartContainer.addView(mChart);


    }

    private class ListAdapter_Csv extends ArrayAdapter<Files_Advance> {
        int resource;
        ArrayList<Files_Advance> fileList;
        boolean title_bar;
        LayoutInflater inflater;

        public ListAdapter_Csv(Context context, int resource, ArrayList<Files_Advance> fileList, boolean title_bar) {
            super(context, resource, fileList);
            this.resource = resource;
            this.fileList = fileList;
            this.title_bar = title_bar;
            inflater = LayoutInflater.from(getActivity());
        }

        private class ViewHolder {
            LinearLayout csv_bkgnd;
            TextView gateNumber;
            TextView count;
            TextView averageWeight;
            TextView totalWeight;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            final ViewHolder holder;
            Files_Advance file = fileList.get(position);
            if (convertView == null) {
                holder = new ViewHolder();
                view = inflater.inflate(resource, parent, false);
                holder.csv_bkgnd = (LinearLayout) view.findViewById(R.id.csv_bkgnd);
                holder.gateNumber = (TextView) view.findViewById(R.id.advanceGateNumber);
                holder.count = (TextView) view.findViewById(R.id.advanceCount);
                holder.averageWeight = (TextView) view.findViewById(R.id.advanceAverageWeight);
                holder.totalWeight = (TextView) view.findViewById(R.id.advanceTotalWeight);

                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            if (title_bar) {
                holder.csv_bkgnd.setBackgroundColor(getResources().getColor(R.color.tepari_blue));
                int col = getResources().getColor(R.color.tepari_white);
                holder.gateNumber.setTextColor(col);
                holder.count.setTextColor(col);
                holder.averageWeight.setTextColor(col);
                holder.totalWeight.setTextColor(col);
            } else {
                int col = (position % 2 == 1) ? getResources().getColor(R.color.tepari_csv_grey) : getResources().getColor(R.color.tepari_white);
                holder.csv_bkgnd.setBackgroundColor(col);
            }

            DecimalFormat formatter = new DecimalFormat("0.##");

            holder.gateNumber.setText(file.getGateNumber());
            holder.count.setText(file.getCount() + "");

            if (title_bar) {
                holder.averageWeight.setText(file.getAverageWeight());
                holder.totalWeight.setText(file.getTotalWeight());
            } else {
                holder.averageWeight.setText(formatter.format(Constants.convertDoubleValue(file.getAverageWeight())));
                holder.totalWeight.setText(formatter.format(Constants.convertDoubleValue(file.getTotalWeight())));
            }

            return view;
        }
    }
}