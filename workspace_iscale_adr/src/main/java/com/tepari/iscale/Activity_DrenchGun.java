package com.tepari.iscale;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.tepari.tpc.R;

import android.view.View;
/**
 * Created by 2fDesign1 on 14/11/2016.
 */

public class Activity_DrenchGun extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drenchgun);
    }

/*    @Override
    public void onBackPressed() {
           if (getFragmentManager().getBackStackEntryCount() == 0) {
        this.finish();
        } else {
            getFragmentManager().popBackStack();
        }
    }*/

    public void firmware(View view) {
        startActivity(new Intent(Activity_DrenchGun.this, Activity_Firmware.class));

    }
}
