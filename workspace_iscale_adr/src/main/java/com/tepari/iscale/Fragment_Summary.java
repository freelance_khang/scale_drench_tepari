package com.tepari.iscale;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.core.content.FileProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tepari.iscale.Helper.Constants;
import com.tepari.iscale.Helper.Files_Content;
import com.tepari.iscale.database.TepariDatabase;
import com.tepari.tpc.R;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class Fragment_Summary extends Fragment {

    public static final String FRAGMENT_ID = "fragment_summary";

    ViewGroup contn;

    private TextView txtFileName;
    private TextView txtRecordNumber;

    private TextView btnViewRecord;
    private TextView btnAdvance;

    private TextView kgWeightMax, kgWeightMin, kgWeightTotal, kgWeightAverage;
    private TextView lbWeightMax, lbWeightMin, lbWeightTotal, lbWeightAverage;

    private TextView kgWeightGainTotal, kgWeightGainAverage, kgWeightGainDay, kgWeightGainDayAverage;
    private TextView lbWeightGainTotal, lbWeightGainAverage, lbWeightGainDay, lbWeightGainDayAverage;

    private LinearLayout layoutWeightLb;
    private LinearLayout layoutWeightKg;

    private String fileFormat;

    private TepariDatabase tepariDatabase;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_summary, container, false);

        tepariDatabase = new TepariDatabase(getActivity());

        setHasOptionsMenu(true);
        contn = container;

        String filename = ((Activity_Parent) getActivity()).csv_filename;
        ((Activity_Parent) getActivity()).actionBar.setTitle(filename);

        txtFileName = (TextView) rootView.findViewById(R.id.txtFileName);
        txtFileName.setText(filename);

        TextView btn_email = (TextView) rootView.findViewById(R.id.btn_email);
        btn_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailFiles(((Activity_Parent) getActivity()).csv_filename);
            }
        });

        txtRecordNumber = (TextView) rootView.findViewById(R.id.txtRecordNumber);

        btnViewRecord = (TextView) rootView.findViewById(R.id.btnViewRecord);
        btnViewRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity_Parent) getActivity()).showFragment(new Fragment_CSV(), "fragment_csv");
            }
        });

        btnAdvance = (TextView) rootView.findViewById(R.id.btnAdvance);
        btnAdvance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity_Parent) getActivity()).showFragment(new Fragment_Advance(), "fragment_advance");
            }
        });

        kgWeightMax = (TextView) rootView.findViewById(R.id.kgWeightMax);
        kgWeightMin = (TextView) rootView.findViewById(R.id.kgWeightMin);
        kgWeightTotal = (TextView) rootView.findViewById(R.id.kgWeightTotal);
        kgWeightAverage = (TextView) rootView.findViewById(R.id.kgWeightAverage);

        kgWeightGainTotal = (TextView) rootView.findViewById(R.id.kgWeightGainTotal);
        kgWeightGainAverage = (TextView) rootView.findViewById(R.id.kgWeightGainAverage);
        kgWeightGainDay = (TextView) rootView.findViewById(R.id.kgWeightGainDay);
        kgWeightGainDayAverage = (TextView) rootView.findViewById(R.id.kgWeightGainDayAverage);


        layoutWeightLb = (LinearLayout) rootView.findViewById(R.id.layoutWeightLb);
        layoutWeightKg = (LinearLayout) rootView.findViewById(R.id.layoutWeightKg);

        lbWeightMax = (TextView) rootView.findViewById(R.id.lbWeightMax);
        lbWeightMin = (TextView) rootView.findViewById(R.id.lbWeightMin);
        lbWeightTotal = (TextView) rootView.findViewById(R.id.lbWeightTotal);
        lbWeightAverage = (TextView) rootView.findViewById(R.id.lbWeightAverage);

        lbWeightGainTotal = (TextView) rootView.findViewById(R.id.lbWeightGainTotal);
        lbWeightGainAverage = (TextView) rootView.findViewById(R.id.lbWeightGainAverage);
        lbWeightGainDay = (TextView) rootView.findViewById(R.id.lbWeightGainDay);
        lbWeightGainDayAverage = (TextView) rootView.findViewById(R.id.lbWeightGainDayAverage);

        displaySummaryRecord();

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.menu_csv, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_rename_file:
                alertRenameFile(contn);
                return true;
            case R.id.menu_delete_single:
                confirmDelete(((Activity_Parent) getActivity()).csv_filename);
                return true;
            case R.id.menu_open_external:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                File externalFile = new File(((Activity_Parent) getActivity()).file_location + ((Activity_Parent) getActivity()).csv_filename);
                Uri path;
                //    Make sure we're running on O or higher to use FileProvider APIs
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    path = FileProvider.getUriForFile((Activity_Parent) getActivity(), "com.tepari.tpc.provider", externalFile);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                else{
                    path = Uri.fromFile(externalFile);
                }
                intent.setDataAndType(path, "application/vnd.ms-excel");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getActivity(), getString(R.string.notify_spreadsheets), Toast.LENGTH_SHORT).show();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        //((Activity_Parent) getActivity()).deleteExportFile();
        displaySummaryRecord();
    }

    // Email selected files
    private void emailFiles(String filename) {
        ArrayList<Uri> uris = new ArrayList<>();
        ((Activity_Parent) getActivity()).generateSummaryTemp("summary_" + filename, fileFormat, kgWeightMax.getText().toString(), kgWeightMin.getText().toString(), kgWeightTotal.getText().toString(), kgWeightAverage.getText().toString(), kgWeightGainTotal.getText().toString(), kgWeightGainAverage.getText().toString(), kgWeightGainDay.getText().toString(), kgWeightGainDayAverage.getText().toString(), lbWeightMax.getText().toString(), lbWeightMin.getText().toString(), lbWeightTotal.getText().toString(), lbWeightAverage.getText().toString(), lbWeightGainTotal.getText().toString(), lbWeightGainAverage.getText().toString(), lbWeightGainDay.getText().toString(), lbWeightGainDayAverage.getText().toString());

        Intent email = new Intent(Intent.ACTION_SEND_MULTIPLE);
        //    Make sure we're running on O or higher to use FileProvider APIs
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            uris.add(FileProvider.getUriForFile((Activity_Parent) getActivity(), "com.tepari.tpc.provider", new File(((Activity_Parent) getActivity()).exportFileLocation + "summary_" + filename)));
            email.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        else{
            uris.add(Uri.fromFile(new File(((Activity_Parent) getActivity()).exportFileLocation + "summary_" + filename)));
        }
        email.setType("text/plain");
        email.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        try {
            startActivity(Intent.createChooser(email, getString(R.string.notify_email_choose)));
        } catch (ActivityNotFoundException exception) {
            Toast.makeText(getActivity(), getString(R.string.toast_email_err), Toast.LENGTH_LONG).show();
        }
    }

    // Deletes the currently opened file and returns to ui_records intent
    private void confirmDelete(String filename) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder
                .setTitle(getString(R.string.delete_confirm))
                .setMessage(getString(R.string.s_delete_file_message, filename))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String filename = ((Activity_Parent) getActivity()).csv_filename;
                        File fileToDelete = new File(((Activity_Parent) getActivity()).file_location + filename);
                        //Delete Database
                        tepariDatabase.deleteTepariFile(fileToDelete.getName());

                        boolean deleted = fileToDelete.delete();
                        if (deleted) {
                            Toast.makeText(getActivity(), getString(R.string.s_filename_delete, filename), Toast.LENGTH_SHORT).show();
                            ((Activity_Parent) getActivity()).getFiles();
                            getActivity().getFragmentManager().popBackStack();
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.s_unable_to_delete_file, filename), Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    // Alert to ask for file rename
    private void alertRenameFile(final ViewGroup contn) {
        LayoutInflater li = LayoutInflater.from(getActivity());
        View promptsView = li.inflate(R.layout.alert_rename, contn, false); // set alert_rename.xml to alert dialog builder
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptsView);
        final EditText userInput = (EditText) promptsView.findViewById(R.id.editTextDialogUserInput);
        alertDialogBuilder
                .setTitle(getString(R.string.rename_file))
                .setMessage(getString(R.string.alert_rename_as))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.rename), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (((Activity_Parent) getActivity()).csv_filename.contains(".csv")) {
                            String newFilename = userInput.getText().toString() + ".csv";
                            ((Activity_Parent) getActivity()).checkLocalExistence(newFilename);
                            if(((Activity_Parent) getActivity()).checkLocalExistence(newFilename)) {
                                Toast.makeText(getActivity(), getString(R.string.toast_file_exist), Toast.LENGTH_LONG).show();
                            } else {
                                renameFile(newFilename);
                            }
                        } else if (((Activity_Parent) getActivity()).csv_filename.contains(".minda")) {
                            String newFilename = userInput.getText().toString() + ".minda";
                            ((Activity_Parent) getActivity()).checkLocalExistence(newFilename);
                            if(((Activity_Parent) getActivity()).checkLocalExistence(newFilename)) {
                                Toast.makeText(getActivity(), getString(R.string.toast_file_exist), Toast.LENGTH_LONG).show();
                            } else {
                                renameFile(newFilename);
                            }
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.toast_invalid_name), Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        // create alert dialog and show it
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    // Rename file
    private void renameFile(String newFilename) {
        File from = new File(((Activity_Parent) getActivity()).file_location, ((Activity_Parent) getActivity()).csv_filename);
        File to = new File(((Activity_Parent) getActivity()).file_location, newFilename);
        if (from.renameTo(to)) {
            ((Activity_Parent) getActivity()).csv_filename = newFilename;
            ((Activity_Parent) getActivity()).actionBar.setTitle(((Activity_Parent) getActivity()).csv_filename);
        } else {
            Toast.makeText(getActivity(), getString(R.string.toast_error_rename_file), Toast.LENGTH_LONG).show();
        }
    }

    private void displaySummaryRecord() {
        // Display either KG or LB values at a time
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String unitType = prefs.getString("unit", Constants.UNIT_TYPE_KG);
        ViewGroup parent;

        ArrayList<Files_Content> listContentFile = ((Activity_Parent) getActivity()).readContentFile();
        double kgWeightMaxValue = 0, kgWeightMinValue = -1, kgWeightTotalValue = 0, kgWeightAverageValue = 0;
        double lbWeightMaxValue = 0, lbWeightMinValue = -1, lbWeightTotalValue = 0, lbWeightAverageValue = 0;

        double kgWeightGainTotalValue = 0, kgWeightGainAverageValue = 0, kgWeightGainDayValue = 0, kgWeightGainDayAverageValue = 0;
        double lbWeightGainTotalValue = 0, lbWeightGainAverageValue = 0, lbWeightGainDayValue = 0, lbWeightGainDayAverageValue = 0;

        int countIgnoreKgGainTotal = 0, countIgnoreKgGainDay = 0;
        int countIgnoreLbGainTotal = 0, countIgnoreLbGainDay = 0;
        fileFormat = Constants.FILE_TYPE_CSV;

        txtRecordNumber.setText(getString(R.string.s_number_record, listContentFile.size() + ""));

        for(Files_Content fileContent : listContentFile) {
            fileFormat = fileContent.getFileFormat();
            double tempWeightKg = Constants.convertDoubleValue(fileContent.getWeightkg());
            if(kgWeightMinValue < 0) {
                kgWeightMinValue = tempWeightKg;
            }

            if(tempWeightKg > kgWeightMaxValue) {
                kgWeightMaxValue = tempWeightKg;
            }

            if(tempWeightKg < kgWeightMinValue) {
                kgWeightMinValue = tempWeightKg;
            }

            kgWeightTotalValue += tempWeightKg;

            double tempWeightLb = Constants.convertDoubleValue(fileContent.getWeightlb());
            if(lbWeightMinValue < 0) {
                lbWeightMinValue = tempWeightLb;
            }

            if(tempWeightLb > lbWeightMaxValue) {
                lbWeightMaxValue = tempWeightLb;
            }

            if(tempWeightLb < lbWeightMinValue) {
                lbWeightMinValue = tempWeightLb;
            }

            lbWeightTotalValue += tempWeightLb;

            double temp1 = Constants.convertDoubleValue(fileContent.getWeight_gain_total_kg());
            if(temp1 != 0) {
                kgWeightGainTotalValue += temp1;
                countIgnoreKgGainTotal++;
            }
            double temp2 = Constants.convertDoubleValue(fileContent.getWeight_gain());
            if(temp2 != 0) {
                kgWeightGainDayValue += temp2;
                countIgnoreKgGainDay++;
            }

            double temp3 = Constants.convertDoubleValue(fileContent.getWeight_gain_total_lb());
            if(temp3 != 0) {
                lbWeightGainTotalValue += temp3;
                countIgnoreLbGainTotal++;
            }
            double temp4 = Constants.convertDoubleValue(fileContent.getWeight_gain_lb());
            if(temp4 != 0) {
                lbWeightGainDayValue += temp4;
                countIgnoreLbGainDay++;
            }
        }

        if(kgWeightMinValue < 0) {
            kgWeightMinValue = 0;
        }
        if(lbWeightMinValue < 0) {
            lbWeightMinValue = 0;
        }

        if(listContentFile.size() > 0) {
            kgWeightAverageValue = kgWeightTotalValue / listContentFile.size();
            lbWeightAverageValue = lbWeightTotalValue / listContentFile.size();


        }

        if(countIgnoreKgGainTotal > 0) {
            kgWeightGainAverageValue = kgWeightGainTotalValue / countIgnoreKgGainTotal;
        }
        if(countIgnoreKgGainDay > 0) {
            kgWeightGainDayAverageValue = kgWeightGainDayValue / countIgnoreKgGainDay;
        }


        if(countIgnoreLbGainTotal > 0) {
            lbWeightGainAverageValue = lbWeightGainTotalValue / countIgnoreLbGainTotal;
        }

        if(countIgnoreLbGainDay > 0) {
            lbWeightGainDayAverageValue = lbWeightGainDayValue / countIgnoreLbGainDay;
        }

        DecimalFormat formatter = new DecimalFormat("0.##");

        kgWeightMax.setText(formatter.format(kgWeightMaxValue));
        kgWeightMin.setText(formatter.format(kgWeightMinValue));
        kgWeightTotal.setText(formatter.format(kgWeightTotalValue));
        kgWeightAverage.setText(formatter.format(kgWeightAverageValue));

        kgWeightGainTotal.setText(formatter.format(kgWeightGainTotalValue));
        kgWeightGainAverage.setText(formatter.format(kgWeightGainAverageValue));
        kgWeightGainDay.setText(formatter.format(kgWeightGainDayValue));
        kgWeightGainDayAverage.setText(formatter.format(kgWeightGainDayAverageValue));

        lbWeightMax.setText(formatter.format(lbWeightMaxValue));
        lbWeightMin.setText(formatter.format(lbWeightMinValue));
        lbWeightTotal.setText(formatter.format(lbWeightTotalValue));
        lbWeightAverage.setText(formatter.format(lbWeightAverageValue));

        lbWeightGainTotal.setText(formatter.format(lbWeightGainTotalValue));
        lbWeightGainAverage.setText(formatter.format(lbWeightGainAverageValue));
        lbWeightGainDay.setText(formatter.format(lbWeightGainDayValue));
        lbWeightGainDayAverage.setText(formatter.format(lbWeightGainDayAverageValue));

        if (unitType.equals(Constants.UNIT_TYPE_KG)) {
            layoutWeightKg.setVisibility(View.VISIBLE);
            layoutWeightLb.setVisibility(View.INVISIBLE);
            // Remove LB view from the layout so KG data shows up at the top of the screen
            parent = (ViewGroup) layoutWeightKg.getParent();
            parent.bringChildToFront(layoutWeightLb);
        }
        else if (unitType.equals(Constants.UNIT_TYPE_LB))
        {
            layoutWeightKg.setVisibility(View.INVISIBLE);
            layoutWeightLb.setVisibility(View.VISIBLE);

            // Remove KG view from the layout so LB data shows up at the top of the screen
            parent = (ViewGroup) layoutWeightKg.getParent();
            parent.bringChildToFront(layoutWeightKg);//.removeView(layoutWeightKg);
        }
    }
}