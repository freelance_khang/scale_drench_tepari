package com.tepari.iscale.Helper;

// Handles the File List present on the Android

public class Files_Saved {

    String name = "";
    boolean selected = false;

    public Files_Saved(String name, boolean selected) {
        super();
        this.name = name;
        this.selected = selected;
    }

    public String getName() {
        return name;
    }
    public boolean isSelected() {
        return selected;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}