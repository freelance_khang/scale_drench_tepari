package com.tepari.iscale.Helper;

import android.util.Log;

import com.tepari.iscale.Activity_Parent;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class DecodeRecord {

    public int getDataId(){
        return Activity_Parent.dataId;
    }

    public void setDataId(int flag){
        Activity_Parent.dataId = flag;
    }

    public Files_Stream constructLiveStream(byte[] data_in) {
        Log.i("Data Length", data_in.length + "");
        String type     = get_info(data_in, 1, 1);
        String eid      = "";
        String modifier = "";
        String weight   = "";

        if (type.equals("We")) {
            type     = get_info(data_in, 1, 1);
            eid      = get_info(data_in, 3, 23).trim();
            modifier = get_info(data_in, 27, 0);
            weight   = get_info(data_in, 28, 5);
        } else if (type.equals("Wo")) {

            type     = get_info(data_in, 1, 1);
            eid      = "";
            modifier = get_info(data_in, 3, 0);
            weight   = get_info(data_in, 4, 5);
        }
        return new Files_Stream(type, eid, modifier, weight);
    }

    public Files_Stream constructOneStream(byte[] data_in) {
        String type     = get_info(data_in, 1, 1);
        String eid      = "";
        String modifier = "";
        String weight   = "";

        if (type.equals("We")) {
            type     = get_info(data_in, 1, 1);
            eid      = get_info(data_in, 3, 23).trim();
            modifier = get_info(data_in, 27, 0);
            weight   = get_info(data_in, 28, 5);
        } else if (type.equals("Wo")) {

            type     = get_info(data_in, 1, 1);
            eid      = "";
            modifier = get_info(data_in, 3, 0);
            weight   = get_info(data_in, 4, 5);
        }
        return new Files_Stream(type, eid, modifier, weight);
    }

    public static void main(String[] args) {
        //84 69 83 84 32 49 50 95 51 48 48 55 49 53 0 0 0 0 78 79 32 69 73 68 48 48 48 52 48 52 49 0 0 0 0 0 0 0 0 0 0 0 78 79 32 86 73 68 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 63 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 63 0 0 51 48 48 55 49 53 48 54 50 57 0 0 0 0 0 8 57 63 63 63 63 44
        //String test = "84,69,83,84,32,49,50,95,51,48,48,55,49,53,0,0,0,0,78,79,32,69,73,68,48,48,48,52,48,52,49,0,0,0,0,0,0,0,0,0,0,0,78,79,32,86,73,68,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,63,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,63,0,0,51,48,48,55,49,53,48,54,50,57,0,0,0,0,0,8,57,63,63,63,63,44";
        String test = "84,69,83,84,32,49,50,95,51,48,48,55,49,53,0,0,0,0,78,79,32,69,73,68,48,48,48,52,48,52,52,0,0,0,0,0,0,0,0,0,0,0,78,79,32,86,73,68,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,63,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,75,0,0,51,48,48,55,49,53,48,54,50,57,0,0,0,0,0,8,57,63,63,63,63,44";
        String[] split = test.split(",");
        System.out.println(split.length);
        String char102 = split[102];
        String char103 = split[103];

        System.out.println(char102);
        System.out.println(char103);

        long value = 0;
        /*for (byte i : bb) {
            if (i != (byte) 255) {
                value = (value << 8) + (i & 0xff);
            }
        }
        double valueTenth = value / 10.0;
        return "" + valueTenth;*/

        //Wrong: TEST 12_300715,NO EID0004041,NO VID,,,.0,,.0,,,,6.3,0.0,30/07/15 06:29,0.0,0.0,8.0,9,
        //Right: TEST 12_300715,NO EID0004041,NO VID,,,.0,,.0,,,,18.9,0.0,30/07/15 06:29,0.0,0.0,8.0,9,
    }

    public String constructRecord(byte[] data_in, boolean removeNonEid, String delimiterCharacter, String eidType) {
        String byteValue = "";
        if(removeNonEid) {
            String nonEidValue = get_info(data_in, 18, 23);
            if(nonEidValue.startsWith("NO EID")) {
                //return "";
            }
        }

        // Check if the data structure is old or new
        setDataId(get_data_structure(data_in));

        String record_csv;
        if(getDataId() == 1) {
            // New structure
            record_csv = get_info(data_in, 2, 17) + delimiterCharacter; //Filename - Add "'" to front to stop Excel treating it as a scientific notation number
            if (eidType.equals(Constants.EID_TYPE_SPACE))
                record_csv += get_info(data_in, 20, 23) + delimiterCharacter;  //EID
            else
                record_csv += get_info_nospace(data_in, 20, 23) + delimiterCharacter;  //EID
            record_csv += get_info(data_in, 44, 14) + delimiterCharacter;  //VID
            record_csv += "" + delimiterCharacter;  //TAG
            record_csv += get_info(data_in, 59, 8) + delimiterCharacter;  //HEALTH 1 TYPE
            record_csv += get_info_dose(data_in, 68) + delimiterCharacter;  //HEALTH 1 DOSE
            record_csv += get_info(data_in, 70, 8) + delimiterCharacter;  //HEALTH 2 TYPE
            record_csv += get_info_dose(data_in, 80) + delimiterCharacter;  //HEALTH 2 DOSE
            record_csv += get_info(data_in, 82, 7) + delimiterCharacter;  //CODE1
            record_csv += get_info(data_in, 90, 7) + delimiterCharacter;  //CODE2
            record_csv += get_info(data_in, 98, 7) + delimiterCharacter;  //CODE3

            record_csv += get_info_int(data_in, 106) + delimiterCharacter;  //WEIGHT KG
            record_csv += get_info_int(data_in, 108) + delimiterCharacter;  //WEIGHT LB
            record_csv += get_info_date(data_in, 110) + delimiterCharacter;  //TIME/DATE
            record_csv += get_info_int(data_in, 120) + delimiterCharacter;  //PREV WEIGHT KG
            record_csv += get_info_int(data_in, 122) + delimiterCharacter;  //PREV WEIGHT LB
            record_csv += get_info_gain(data_in, 124) + delimiterCharacter;  //GAIN DAYS
            record_csv += get_info_draft(data_in, 126) + delimiterCharacter;  //DRAFT
        }
        else {
            // Old structure
            record_csv = get_info(data_in, 0, 17) + delimiterCharacter; //Filename - Add "'" to front to stop Excel treating it as a scientific notation number
            if (eidType.equals(Constants.EID_TYPE_SPACE))
                record_csv += get_info(data_in, 18, 23) + delimiterCharacter;  //EID
            else
                record_csv += get_info_nospace(data_in, 18, 23) + delimiterCharacter;  //EID
            record_csv += get_info(data_in, 42, 5) + delimiterCharacter;  //VID
            record_csv += get_info(data_in, 48, 6) + delimiterCharacter;  //TAG
            record_csv += get_info(data_in, 55, 8) + delimiterCharacter;  //HEALTH 1 TYPE
            record_csv += get_info_dose(data_in, 64) + delimiterCharacter;  //HEALTH 1 DOSE
            record_csv += get_info(data_in, 66, 8) + delimiterCharacter;  //HEALTH 2 TYPE
            record_csv += get_info_dose(data_in, 76) + delimiterCharacter;  //HEALTH 2 DOSE
            record_csv += get_info(data_in, 78, 7) + delimiterCharacter;  //CODE1
            record_csv += get_info(data_in, 86, 7) + delimiterCharacter;  //CODE2
            record_csv += get_info(data_in, 94, 7) + delimiterCharacter;  //CODE3

            record_csv += get_info_int(data_in, 102) + delimiterCharacter;  //WEIGHT KG
            record_csv += get_info_int(data_in, 104) + delimiterCharacter;  //WEIGHT LB
            record_csv += get_info_date(data_in, 106) + delimiterCharacter;  //TIME/DATE
            record_csv += get_info_int(data_in, 116) + delimiterCharacter;  //PREV WEIGHT KG
            record_csv += get_info_int(data_in, 118) + delimiterCharacter;  //PREV WEIGHT LB
            record_csv += get_info_gain(data_in, 120) + delimiterCharacter;  //GAIN DAYS
            record_csv += get_info_draft(data_in, 122) + delimiterCharacter;  //DRAFT
        }

        for(byte value : data_in) {
            byteValue += "," + value;
        }
        Log.i("Byte Length", data_in.length + "");
        Log.i("Byte Value", byteValue);
        Log.i("Save Record", decode_string(record_csv));
        return decode_string(record_csv);
    }

    public String decode_string(String str) {
        String result = "";
        for (int i=0; i< str.length(); i++) {
            char c = str.charAt(i);
            if ((c > 31) && (c < 127)) {
                result += "" + c;
            }
        }
        return result;
    }

    // Data structure
    public int get_data_structure(byte[] rawData) {
        if ((rawData[0] == (byte)0xFE) && (rawData[1] == (byte)0xFC))
            return 1;
        else
            return 0;
    }

    // Info
    public String get_info(byte[] rawData, int start, int size) {
        String result = "";
        for (int x = start; x <= (start + size); x++) {
            if ((rawData[x] != 0) && (rawData[x] != 255)) {
                char c = (char) rawData[x];
                result += Character.toString(c);
            }
            else
                break;
        }
        return result;
    }

    // Info
    public String get_info_nospace(byte[] rawData, int start, int size) {
        String result = "";
        for (int x = start; x <= (start + size); x++) {
            if ((rawData[x] != 0) && (rawData[x] != 255)
                    && (rawData[x] != ' ')) {
                char c = (char) rawData[x];
                result += Character.toString(c);
            }
            else if(rawData[x] == ' ')
            {
                ;
            }
            else
                break;
        }
        return result;
    }

    // Dose
    public String get_info_dose(byte[] rawData, int start) {
        long value_hi = rawData[start] & 0xFF;
        long value_lo = rawData[start + 1];
        byte[] bytes_hi;
        byte[] bytes_lo;
        if ((value_hi & 0x80) == 0x80) {
            bytes_hi = ByteBuffer.allocate(8).putLong(value_hi & 0x7F).array();
            bytes_lo = ByteBuffer.allocate(8).putLong(value_lo).array();
            return new String(bytes_hi) + "." + new String(bytes_lo) + "0";
        }
        else {
            bytes_hi = ByteBuffer.allocate(8).putLong(value_hi).array();
            bytes_lo = ByteBuffer.allocate(8).putLong(value_lo).array();
            return new String(bytes_hi) + new String(bytes_lo)+ ".0";
        }
    }

    // Info Integer
    public String get_info_int(byte[] rawData, int start) {
        byte[] bb = new byte[] {rawData[start], rawData[start+1]};
        long value = 0;
        for (byte i : bb) {
            if (i != (byte) 255) {
                value = (value << 8) + (i & 0xff);
            }
        }
        double valueTenth = value / 10.0;
        return "" + valueTenth;
    }

    public String get_info_gain(byte[] rawData, int start) {
        byte[] bb = new byte[] {rawData[start], rawData[start+1]};
        long value = 0;
        for (byte i : bb) {
            if (i != (byte) 255) {
                value = (value << 8) + (i & 0xff);
            }
        }
        double valueTenth = value;
        return "" + valueTenth;
    }

    // Date
    public String get_info_date(byte[] rawData, int start) {
        String result;
        // DD/MM/YY HH:MM
        result = Character.toString((char) rawData[start])     + Character.toString((char) rawData[start + 1]) + "/" +
                 Character.toString((char) rawData[start + 2]) + Character.toString((char) rawData[start + 3]) + "/" +
                 Character.toString((char) rawData[start + 4]) + Character.toString((char) rawData[start + 5]) + " " +
                 Character.toString((char) rawData[start + 6]) + Character.toString((char) rawData[start + 7]) + ":" +
                 Character.toString((char) rawData[start + 8]) + Character.toString((char) rawData[start + 9]);
        return result;
    }

    // Date
    public String get_info_date_min(byte[] rawData, int start) {
        String result;
        // DD/MM/YY
        result = Character.toString((char) rawData[start])     + Character.toString((char) rawData[start + 1]) + "/" +
                 Character.toString((char) rawData[start + 2]) + Character.toString((char) rawData[start + 3]) + "/" +
                 Character.toString((char) rawData[start + 4]) + Character.toString((char) rawData[start + 5]);
        return result;
    }

    // Draft
    public String get_info_draft(byte[] rawData, int start) {
        char x = (char) rawData[start];
        switch (x) {
            case '>':
                return "<"; //Arrow Left
            case '=':
                return "|"; //Arrow Up
            case '?':
                return ">"; //Arrow Right
            case ':':
                return "L";
            case ';':
                return "M";
            case '<':
                return "H";
            default:
                return Character.toString(x);
        }
    }

}
