package com.tepari.iscale.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Pair;

public class Constants {

    public static String FILE_TYPE_CSV = "csv";
    public static String FILE_TYPE_MINDA = "minda";
    public static String FILE_TYPE_STREAM = "stream";

    public static String UNIT_TYPE_KG = "KG";
    public static String UNIT_TYPE_LB = "LB";

    public static String EID_TYPE_SPACE = "Space";
    public static String EID_TYPE_NO_SPACE = "No Space";

    public static int FILELIST_WAIT = 3000;
    public static String Response = "*HELLO*";
    public static byte[] command_file_list = new byte[] {(byte) 0xA6, 0, 0, 0, (byte) 0xFF};
    public static byte[] command_record = new byte[] {(byte) 0xA7, 0, 0, 0, 0};

    //Delete File
    public static byte[] command_delete = new byte[] {(byte) 0xAD, 0, 0, 0, 0};

    //M5
    //{ '<', 'M', '5', '>', 0 }
    public static byte[] command_change_com_port = new byte[] {(byte)'<', (byte)'M', (byte)'5', (byte)'>', 0};
    public static byte[] command_live_eid_weight = new byte[] {(byte)'<', (byte)'M', (byte)'4', (byte)'>', 0};
    public static byte[] command_live_weight_only = new byte[] {(byte)'<', (byte)'M', (byte)'1', (byte)'>', 0};
    public static byte[] command_live_autodrafting = new byte[] {(byte)'<', (byte)'M', (byte)'7', (byte)'>', 0};
    //public static byte[] command_live_autodrafting = new byte[] {(byte)'<', (byte)'M', (byte)'2', (byte)'>', 0};

    public static byte[] command_heartbeat = new byte[] {(byte)'<', (byte)'K', (byte)'N', (byte)'>', 0};

    public static int RECORD_INDEX = 4;
    public static int DELETE_INDEX = 1;


    public static Pair<String, Integer> getConnectionDetails(Context ctx) {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(ctx);
        String ip_address;
        int port_number;
        try {
            ip_address = SP.getString("ipAddress", "10.10.10.1");
        } catch (Exception e) {
            ip_address = "10.10.10.1";
        }
        try {
            port_number = Integer.parseInt(SP.getString("portNumber", "2000"));
        } catch (Exception e) {
            port_number = 2000;
        }
        return new Pair<>(ip_address, port_number);
    }

    public static int getConnectionTimeout(Context ctx) {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(ctx);
        int timeout;
        try {
            timeout = Integer.parseInt(SP.getString("timeout", "5000"));
        } catch (Exception e) {
            timeout = 5000;
        }
        return timeout;
    }


    public static double convertDoubleValue(String value) {
        try {
            return Double.valueOf(value);
        } catch(Exception e) {
            return 0;
        }
    }

    public static int convertIntValue(String value) {
        try {
            return Integer.valueOf(value);
        } catch(Exception e) {
            return Integer.MAX_VALUE;
        }
    }

    public static int convertIntValue(String value, int defaultValue) {
        try {
            return Integer.valueOf(value);
        } catch(Exception e) {
            return defaultValue;
        }
    }
}
