package com.tepari.iscale.Helper;

// Handles the File List present on the iScale

public class Files_Stream {

    private String type = ""; // Wo or We - Weight Only or Weight and EID
    private String eid = "";
    private String modifier = ""; // L or R - Lock on / zero, or Record
    private String weight = ""; // 1dp
    

    public Files_Stream(
            String type, 
            String eid, 
            String modifier, 
            String weight) {
        super();
        this.type = type;
        this.eid = eid;
        this.modifier = modifier;
        this.weight = weight;
    }

    public String getType() {
        return type;
    }
    public String getEid() {
        return eid;
    }
    public String getModifier() {
        return modifier;
    }
    public String getWeight() {
        return weight;
    }

}