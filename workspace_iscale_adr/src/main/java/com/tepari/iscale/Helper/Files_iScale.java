package com.tepari.iscale.Helper;

// Handles the File List present on the iScale

public class Files_iScale {
    private int index;
    private String name = "";
    private int code = 0;
    private boolean selected = false;

    private boolean downloaded = false;
    private String fileDownloaded = null;

    public Files_iScale(int index, String name, int code, boolean selected, boolean downloaded, String fileDownloaded) {
        super();
        this.index = index;
        this.name = name;
        this.code = code;
        this.selected = selected;
        this.downloaded = downloaded;
        this.fileDownloaded = fileDownloaded;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isDownloaded() {
        return downloaded;
    }

    public void setDownloaded(boolean downloaded) {
        this.downloaded = downloaded;
    }

    public String getFileDownloaded() {
        return fileDownloaded;
    }

    public void setFileDownloaded(String fileDownloaded) {
        this.fileDownloaded = fileDownloaded;
    }
}