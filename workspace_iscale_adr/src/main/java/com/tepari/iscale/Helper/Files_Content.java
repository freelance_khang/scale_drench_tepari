package com.tepari.iscale.Helper;

import com.tepari.iscale.Activity_Parent;

import java.text.DecimalFormat;

public class Files_Content {
    String fileFormat = "";
    String num = "";
    String filename = "";
    String eid = "";
    String vid = "";
    String pid = "";
    String health1_type = "";
    String health1_dose = "";
    String health2_type = "";
    String health2_dose = "";
    String code1 = "";
    String code2 = "";
    String code3 = "";
    String weightkg = "";
    String weightlb = "";
    String date = "";
    String prev_weightkg = "";
    String prev_weightlb = "";
    String gain = "";
    String draft = "";
    String weight_gain = "WEIGHT GAIN KG";
    String weight_gain_lb = "WEIGHT GAIN LB";

    String weight_gain_total_kg = "WEIGHT GAIN TOTAL KG";
    String weight_gain_total_lb = "WEIGHT GAIN TOTAL LB";

    private String listColumnName;
    private String listColumnValue;

    public Files_Content(String fileFormat,
                         String num,
                         String filename,
                         String eid,
                         String vid,
                         String pid,
                         String health1_type,
                         String health1_dose,
                         String health2_type,
                         String health2_dose,
                         String code1,
                         String code2,
                         String code3,
                         String weightkg,
                         String weightlb,
                         String date,
                         String prev_weightkg,
                         String prev_weightlb,
                         String gain,
                         String draft) {
        super();
        this.fileFormat = fileFormat;
        this.num = num;
        this.filename = filename;
        this.eid = eid;
        this.vid = vid;
        this.pid = pid;
        this.health1_type = health1_type;
        this.health1_dose = health1_dose;
        this.health2_type = health2_type;
        this.health2_dose = health2_dose;
        this.code1 = code1;
        this.code2 = code2;
        this.code3 = code3;
        this.weightkg = weightkg;
        this.weightlb = weightlb;
        this.date = date;
        this.prev_weightkg = prev_weightkg;
        this.prev_weightlb = prev_weightlb;
        this.gain = gain;
        this.draft = draft;

        if(!gain.equals("GAIN DAYS")) {
            calculateWeightGain();
        }

    }

    public String getNum() {
        return num;
    }

    public String getFilename() {
        return filename;
    }

    public String getEid() {
        return eid;
    }

    public String getVid() {
        return vid;
    }

    public String getPid() {
        return pid;
    }

    public String getHealth1_type() {
        return health1_type;
    }

    public String getHealth1_dose() {
        return health1_dose;
    }

    public String getHealth2_type() {
        return health2_type;
    }

    public String getHealth2_dose() {
        return health2_dose;
    }

    public String getCode1() {
        return code1;
    }

    public String getCode2() {
        return code2;
    }

    public String getCode3() {
        return code3;
    }

    public String getWeightkg() {
        return weightkg;
    }

    public String getWeightlb() {
        return weightlb;
    }

    public String getDate() {
        return date;
    }

    public String getPrev_weightkg() {
        return prev_weightkg;
    }

    public String getPrev_weightlb() {
        return prev_weightlb;
    }

    public String getGain() {
        return gain;
    }

    public String getDraft() {
        return draft;
    }

    public String getFileFormat() {
        return fileFormat;
    }

    public String getWeight_gain() {
        return weight_gain;
    }

    public void calculateWeightGain() {
        weight_gain = "0";
        weight_gain_lb = "0";
        weight_gain_total_kg = "0";
        weight_gain_total_lb = "0";
        try {
            double gainValue = 0;
            if(gain != null && !gain.equals("")) {
                gainValue = Double.valueOf(gain);
            }

            double weight = 0;
            if(weightkg != null && !weightkg.equals("")) {
                weight = Double.valueOf(weightkg);
            }
            double previous_weight = 0;
            if(prev_weightkg != null && !prev_weightkg.equals("")) {
                previous_weight = Double.valueOf(prev_weightkg);
            }

            double weightLb = 0;
            if(weightlb != null && !weightlb.equals("")) {
                weightLb = Double.valueOf(weightlb);
            }
            double previous_weight_lb = 0;
            if(prev_weightlb != null && !prev_weightlb.equals("")) {
                previous_weight_lb = Double.valueOf(prev_weightlb);
            }

            double value_weight_gain_total_kg = 0;
            if(previous_weight <= 0) {
                value_weight_gain_total_kg = 0;
            } else {
                value_weight_gain_total_kg = (weight - previous_weight);
            }
            weight_gain_total_kg = "" + value_weight_gain_total_kg;

            double value_weight_gain_total_lb = 0;
            if(previous_weight_lb <= 0) {
                value_weight_gain_total_lb = 0;
            } else {
                value_weight_gain_total_lb = weightLb - previous_weight_lb;
            }
            weight_gain_total_lb = "" + value_weight_gain_total_lb;

            if(gainValue != 0) {
                DecimalFormat formatter = new DecimalFormat("0.#");
                weight_gain = "" + formatter.format(value_weight_gain_total_kg / gainValue);
                weight_gain_lb = "" + formatter.format(value_weight_gain_total_lb / gainValue);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }



    public String exportRow(String fileType, String delimiterCharacter) {
        String result = "";
        /**
         * <string name="csv_title_bar">FILENAME,EID,VID,PID,HEALTH 1 TYPE,HEALTH 1 DOSE,HEALTH 2 TYPE,HEALTH 2 DOSE,CODE 1,CODE 2,CODE 3,WEIGHT KG,WEIGHT LB,DATE/TIME,PREV WEIGHT KG,PREV WEIGHT LB,GAIN DAYS,DRAFT\n</string>

         <string name="csv_title_minda_bar">EID,DATE,VID,WEIGHT KG\n</string>
         */
        if(fileType.equals(Constants.FILE_TYPE_CSV)) {
            if(Activity_Parent.dataId == 1) {
                result = num + delimiterCharacter + filename + delimiterCharacter + eid + delimiterCharacter + vid + delimiterCharacter + pid + delimiterCharacter + health1_type + delimiterCharacter + health1_dose + delimiterCharacter
                        + health2_type + delimiterCharacter + health2_dose + delimiterCharacter + code1 + delimiterCharacter + code2 + delimiterCharacter + code3 + delimiterCharacter + weightkg + delimiterCharacter + weightlb + delimiterCharacter + prev_weightkg + delimiterCharacter + prev_weightlb + delimiterCharacter + gain + delimiterCharacter
                        + weight_gain + delimiterCharacter + weight_gain_lb + delimiterCharacter
                        + weight_gain_total_kg + delimiterCharacter + weight_gain_total_lb + delimiterCharacter
                        + draft + delimiterCharacter + date + "\n";
            /*    result = num + delimiterCharacter + filename + delimiterCharacter + eid + delimiterCharacter + vid + delimiterCharacter + health1_type + delimiterCharacter + health1_dose + delimiterCharacter
                        + health2_type + delimiterCharacter + health2_dose + delimiterCharacter + code1 + delimiterCharacter + code2 + delimiterCharacter + code3 + delimiterCharacter + weightkg + delimiterCharacter + prev_weightkg + delimiterCharacter + gain + delimiterCharacter
                        + weight_gain + delimiterCharacter
                        + weight_gain_total_kg + delimiterCharacter
                        + draft + delimiterCharacter + date + "\n";*/
            }
            else {
                result = num + delimiterCharacter + filename + delimiterCharacter + eid + delimiterCharacter + vid + delimiterCharacter + pid + delimiterCharacter + health1_type + delimiterCharacter + health1_dose + delimiterCharacter
                    + health2_type + delimiterCharacter + health2_dose + delimiterCharacter + code1 + delimiterCharacter + code2 + delimiterCharacter + code3 + delimiterCharacter + weightkg + delimiterCharacter + weightlb + delimiterCharacter  + prev_weightkg + delimiterCharacter + prev_weightlb + delimiterCharacter + gain  + delimiterCharacter
                    + weight_gain + delimiterCharacter + weight_gain_lb + delimiterCharacter
                    + weight_gain_total_kg + delimiterCharacter + weight_gain_total_lb + delimiterCharacter
                    + draft + delimiterCharacter + date+ "\n";
            }
        } else {
            result = num + delimiterCharacter + eid + delimiterCharacter + date + delimiterCharacter + vid + delimiterCharacter + weightkg + "\n";
        }
        return result;
    }

    public String getListColumnName() {
        return listColumnName;
    }

    public void setListColumnName(String listColumnName) {
        this.listColumnName = listColumnName;
    }

    public String getListColumnValue() {
        return listColumnValue;
    }

    public void setListColumnValue(String listColumnValue) {
        this.listColumnValue = listColumnValue;
    }

    public void setWeight_gain(String weight_gain) {
        this.weight_gain = weight_gain;
    }

    public String getWeight_gain_lb() {
        return weight_gain_lb;
    }

    public void setWeight_gain_lb(String weight_gain_lb) {
        this.weight_gain_lb = weight_gain_lb;
    }

    public String getWeight_gain_total_kg() {
        return weight_gain_total_kg;
    }

    public void setWeight_gain_total_kg(String weight_gain_total_kg) {
        this.weight_gain_total_kg = weight_gain_total_kg;
    }

    public String getWeight_gain_total_lb() {
        return weight_gain_total_lb;
    }

    public void setWeight_gain_total_lb(String weight_gain_total_lb) {
        this.weight_gain_total_lb = weight_gain_total_lb;
    }
}