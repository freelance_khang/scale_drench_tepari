package com.tepari.iscale.Helper;

public class Files_Csv {

    String num = "";
    String filename = "";
    String eid = "";
    String vid = "";
    String pid = "";
    String health1_type = "";
    String health1_dose = "";
    String health2_type = "";
    String health2_dose = "";
    String code1 = "";
    String code2 = "";
    String code3 = "";
    String weightkg = "";
    String weightlb = "";
    String date = "";
    String prev_weightkg = "";
    String prev_weightlb = "";
    String gain = "";
    String draft = "";

    public Files_Csv(String num,
                     String filename,
                     String eid,
                     String vid,
                     String pid,
                     String health1_type,
                     String health1_dose,
                     String health2_type,
                     String health2_dose,
                     String code1,
                     String code2,
                     String code3,
                     String weightkg,
                     String weightlb,
                     String date,
                     String prev_weightkg,
                     String prev_weightlb,
                     String gain,
                     String draft) {
        super();
        this.num = num;
        this.filename = filename;
        this.eid = eid;
        this.vid = vid;
        this.pid = pid;
        this.health1_type = health1_type;
        this.health1_dose = health1_dose;
        this.health2_type = health2_type;
        this.health2_dose = health2_dose;
        this.code1 = code1;
        this.code2 = code2;
        this.code3 = code3;
        this.weightkg = weightkg;
        this.weightlb = weightlb;
        this.date = date;
        this.prev_weightkg = prev_weightkg;
        this.prev_weightlb = prev_weightlb;
        this.gain = gain;
        this.draft = draft;
    }

    public String getNum() {
        return num;
    }
    public String getFilename() {
        return filename;
    }
    public String getEid() {
        return eid;
    }
    public String getVid() {
        return vid;
    }
    public String getPid() {
        return pid;
    }
    public String getHealth1_type() {
        return health1_type;
    }
    public String getHealth1_dose() {
        return health1_dose;
    }
    public String getHealth2_type() {
        return health2_type;
    }
    public String getHealth2_dose() {
        return health2_dose;
    }
    public String getCode1() {
        return code1;
    }
    public String getCode2() {
        return code2;
    }
    public String getCode3() {
        return code3;
    }
    public String getWeightkg() {
        return weightkg;
    }
    public String getWeightlb() {
        return weightlb;
    }
    public String getDate() {
        return date;
    }
    public String getPrev_weightkg() {
        return prev_weightkg;
    }
    public String getPrev_weightlb() {
        return prev_weightlb;
    }
    public String getGain() {
        return gain;
    }
    public String getDraft() {
        return draft;
    }

}