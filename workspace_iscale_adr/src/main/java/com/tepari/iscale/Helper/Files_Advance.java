package com.tepari.iscale.Helper;

public class Files_Advance {
    private String gateNumber = "";
    private String count = "";
    private String averageWeight = "";
    private String totalWeight = "";

    public Files_Advance(String gateNumber, String count, String averageWeight, String totalWeight) {
        this.gateNumber = gateNumber;
        this.count = count;
        this.averageWeight = averageWeight;
        this.totalWeight = totalWeight;
    }

    public String getGateNumber() {
        return gateNumber;
    }

    public void setGateNumber(String gateNumber) {
        this.gateNumber = gateNumber;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getAverageWeight() {
        return averageWeight;
    }

    public void setAverageWeight(String averageWeight) {
        this.averageWeight = averageWeight;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }
}