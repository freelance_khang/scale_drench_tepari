package com.tepari.iscale.Helper;

public class Files_Minda {

    String num = "";
    String eid = "";
    String vid = "";
    String weight = "";
    String date = "";

    public Files_Minda(String num,
                       String eid,
                       String date,
                       String vid,
                       String weight) {
        super();
        this.num = num;
        this.eid = eid;
        this.date = date;
        this.vid = vid;
        this.weight = weight;
    }

    public String getNum() {
        return num;
    }
    public String getEid() {
        return eid;
    }
    public String getDate() {
        return date;
    }
    public String getVid() {
        return vid;
    }
    public String getWeight() {
        return weight;
    }

}