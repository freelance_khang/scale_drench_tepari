package com.tepari.iscale.Helper;

import com.tepari.iscale.Activity_Parent.SocketTask;

/**
 * Created by KeHo on 5/12/15.
 */
public class CommandTimeout {
    private SocketTask socketTask;
    private int sendCounter;

    public CommandTimeout(SocketTask socketTask) {
        this.socketTask = socketTask;
        this.sendCounter = 1;
    }

    public int getSendCounter() {
        return sendCounter;
    }

    public void setSendCounter(int sendCounter) {
        this.sendCounter = sendCounter;
    }

    public SocketTask getSocketTask() {
        return socketTask;
    }

    public void setSocketTask(SocketTask socketTask) {
        this.socketTask = socketTask;
    }
}
