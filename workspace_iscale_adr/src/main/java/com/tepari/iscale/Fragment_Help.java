package com.tepari.iscale;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tepari.tpc.R;

public class Fragment_Help extends Fragment {

    public static final String FRAGMENT_ID = "fragment_help";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_help, container, false);
        ((Activity_Parent) getActivity()).actionBar.setTitle(getString(R.string.ui_trouble_title));
        return rootView;
    }

}