package com.tepari.iscale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tepari.iscale.Helper.Constants;
import com.tepari.tpc.R;

import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import io.fabric.sdk.android.ActivityLifecycleManager;

import static com.tepari.iscale.Fragment_DgUpgradeProtocol.SocketTaskStatus.STATE_FWBANK;
import static com.tepari.iscale.Fragment_DgUpgradeProtocol.SocketTaskStatus.STATE_IDLE;
import static com.tepari.iscale.Fragment_DgUpgradeProtocol.SocketTaskStatus.STATE_INITPACKET;
import static com.tepari.iscale.Fragment_DgUpgradeProtocol.SocketTaskStatus.STATE_SENDPACKET;
import static com.tepari.iscale.Fragment_DgUpgradeProtocol.SocketTaskStatus.STATE_STARTTX;
import static com.tepari.iscale.Fragment_DgUpgradeProtocol.SocketTaskStatus.STATE_SUCCESS;
import static com.tepari.iscale.Fragment_DgUpgradeProtocol.SocketTaskStatus.STATE_END;
import static com.tepari.iscale.Fragment_DgUpgradeProtocol.SocketTaskStatus.STATE_VERSIONNO;
import static com.tepari.iscale.Fragment_DgUpgradeProtocol.SocketTaskStatus.STATE_WRITEEOT;
import static com.tepari.tpc.R.id.progressbar;
import static com.tepari.tpc.R.id.statusLabel;
import static com.tepari.tpc.R.id.upgradeButton;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

/**
 * Created by 2fDesign1 on 8/12/2016.
 */

public class Fragment_DgUpgradeProtocol extends Fragment implements View.OnClickListener {

    GunNetworkTask mNetworkTask;
    DG_Ymodem mYmodem;

    private ProgressBar mProgressBar;
    private Button goButton = null;
    private LinearLayout linearLayout;
    private ImageView tickImage;

    private static final int MAX = 100;
    static boolean ALERTPRESSED = FALSE;

    private Boolean BANKNOCHOSEN = FALSE;
    private Boolean OLDFIRMWARE = FALSE;
    private boolean isConnecting = false;   // checking for connection

    private boolean errors = false;
    final byte[] fname = new byte[]{'E','G','_','F','i','r','m','w','a','r','e','.','b','i','n','\0'};

   // private final int DIV = (mYmodem.DATA_SIZE/mYmodem.PACKET_1K_SIZE)*80;
    private final int DIV = (51200/mYmodem.PACKET_1K_SIZE)*80;
    private int currentBank;

    //Status task of socket
    public enum SocketTaskStatus {STATE_DISCONNECTED, STATE_CONNECTING, STATE_BUSY, STATE_IDLE,
        STATE_SENDPACKET, STATE_VERSIONNO, STATE_STARTTX, STATE_INITPACKET, STATE_WRITEEOT, STATE_LASTDATA, STATE_SUCCESS, STATE_END, STATE_FWBANK}
    private SocketTaskStatus currentTaskStatus = SocketTaskStatus.STATE_DISCONNECTED;

    // File setting
    FileSettings settings = new FileSettings();

    private int firmwareBankRequired = -1;

    ViewGroup contn;
    private ImageButton btn_connect_wifi;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        Log.i("onCreate", "Entered");
        // Retain this instance so it isn't destroyed when Activity_Firmware and
        // this Fragment change configuration.
        setRetainInstance(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        Log.i("onCreateView", "Entered");

        contn = container;
        LayoutInflater lf = getActivity().getLayoutInflater();
        View view = lf.inflate(R.layout.fragment_dgupgradeprotocol, container, false);

        // callback for the Go button
        goButton = (Button) view.findViewById(R.id.upgradeButton);
        goButton.setOnClickListener(this);

        mProgressBar = (ProgressBar)view.findViewById(R.id.progressBarHorizontal);
        mProgressBar.setMax(MAX);

        // LinearLayout container
        linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout1);


        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case (R.id.upgradeButton):
                if(goButton.getText().equals("Go")) {
                    Log.i("Go Button: ", "pressed");
                    // Check if the user has connected to tepari_EG_upgrade wifi connection
                    dgFirmwareUpgradeViewController upgrade = new dgFirmwareUpgradeViewController(getActivity());
                    Boolean wifiPresent = upgrade.currentWifiEquals_upgrade();

                    if (wifiPresent == true) {
                        connectGun();

                        goButton.setText("Cancel");
                    } else {
                        ((Activity_Firmware)getActivity()).changeStatusTextView("You must be connected to the"
                         + "\"tepari_EG_upgrade\" network in order to update the firmware");
                        ((Activity_Firmware)getActivity()).changeInstrTextView("");
                    }
                }
                else {
                    Log.i("Cancel Button: ", "pressed");
                    disconnect();
                    goButton.setText("Go");
                    mProgressBar.setProgress(0);
                    if (tickImage != null) {
                        linearLayout.removeView(tickImage);
                    }
                    if(mNetworkTask != null) {
                        Log.d("onClick", "mNetwork = null");
                        mNetworkTask = null;
                        ALERTPRESSED = FALSE;
                        mYmodem.blkNumber = 0x01;
                        errors = false;
                    }
                }
                break;
            default:
                break;

        }
    }

    // This is to work around what is apparently a bug. If you don't have it
    // here the dialog will be dismissed on rotation, so tell it not to dismiss.
    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("onDestroy","disconnect()");
        //In case the task is currently running
        disconnect();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        // See if the task has finished while we weren't
        // in this activity, and then we can close ourselves.
       /* if (mNetworkTask == null)
            mNetworkTask.close();*/
    }

    //Ask the user whether to upgrade the firmware or not
    public byte[] alert_fwUpload(String version){
        // download '1 enter' to start the transmission
        final byte[] tx = {'1','0','0'};
        // Launch  a dialog
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(R.string.ui_fw_uploadtitle);
        alertDialog.setMessage(version + "Do you want to update to " + settings.getFirmwareVersion(this.getActivity())
                + "?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which){
                        Log.d("alert_fwUpload",  "Set STATE to STARTTX");

                        currentTaskStatus = SocketTaskStatus.STATE_STARTTX;
                        ALERTPRESSED = TRUE;

                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which){
                        currentTaskStatus = STATE_IDLE;
                        ALERTPRESSED = TRUE;
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        return tx;
    }

    /*
     * Ask the user to enter either Bank 1 or Bank 2
     */
    public byte[] alert_bankNo(final ViewGroup contn){
        // Send Enter to receive the version no
        byte[] tx = {'\r', '\n', 0};
        // Launch  a dialog
        LayoutInflater li = LayoutInflater.from(getActivity());
        View promptsView = li.inflate(R.layout.wifi_prompts, contn, false); // set wifi_prompts.xml to alert dialog builder
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setView(promptsView);
        alertDialog.setTitle(R.string.ui_fw_banknotitle);
        alertDialog.setMessage(getActivity().getString(R.string.ui_fw_bankno));
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "BANK 1",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which){
                        Log.d("alert_bankNo",  "BANK 1");
                        firmwareBankRequired = 2;
                        ALERTPRESSED = TRUE;

                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "BANK2",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which){
                        Log.d("alert_bankNo",  "BANK 2");
                        firmwareBankRequired = 1;
                        ALERTPRESSED = TRUE;
                    }
                });

        // Open the Wifi setting
        btn_connect_wifi = (ImageButton) promptsView.findViewById(R.id.btn_connect_wifi);

        btn_connect_wifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("btn_connect_wifi", "pressed");
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        });

        alertDialog.show();
        return tx;
    }

    /*
     * Start transmitting initial data
     */
    public byte[] startTransmittingInitial(){
        byte[] initialPacket = null;
        setYmodem(new DG_Ymodem(getActivity()));
        if(firmwareBankRequired == 0)
            initialPacket = mYmodem.Ymodem_transmit_initialPacket(fname, 105472);
        else if(firmwareBankRequired > 0)
            initialPacket = mYmodem.Ymodem_transmit_initialPacket(fname, 51200);

        return initialPacket;
    }


    /*
     * Start transmitting data
     */
    public byte[] startTransmittingData(){

        byte[] data = null;
        // Need to determine whether to send B1 or B2.

        if (firmwareBankRequired == 0) {
            byte[] firmwareB0Raw = settings.getFirmwareFileB0(this.getActivity());
            data = mYmodem.Ymodem_transmit_dataPacket(firmwareB0Raw, mYmodem.firmwareB0Length);
            Log.d("startTransmittingData", firmwareB0Raw.length + "firmwareB0Raw.length");
        }
        else if (firmwareBankRequired == 1) {
            byte[] firmwareB1Raw = settings.getFirmwareFileB1(this.getActivity());
            data = mYmodem.Ymodem_transmit_dataPacket(firmwareB1Raw, mYmodem.firmwareB1Length);
            Log.d("startTransmittingData", firmwareB1Raw.length + "firmwareB1Raw.length");
        }
        else if (firmwareBankRequired == 2) {
            byte[] firmwareB2Raw = settings.getFirmwareFileB2(this.getActivity());
            data = mYmodem.Ymodem_transmit_dataPacket(firmwareB2Raw, mYmodem.firmwareB2Length);
            Log.d("startTransmittingData", firmwareB2Raw.length + "firmwareB2Raw.length");
        }

        return data;
    }

    /*
     * Start transmitting the last packet of data
     */
    public byte[] startTransmittingLastData() {

        byte[] data = mYmodem.Ymodem_transmit_LastPacket();
        return data;
    }

    // Attempts connection to the Drench Gun and prepares progressbar
    public void connectGun() {
        try {
            mNetworkTask = new GunNetworkTask();
            mNetworkTask.setProgressBar(mProgressBar);
            // Send Enter to receive the version no
            byte[] tx = {'\r', '\n', 0};
            currentTaskStatus = STATE_VERSIONNO;
            Log.d("thread 1 ", "started");
            mNetworkTask.execute(tx);
            Log.d("thread 1 ", "finished");

        } catch (Exception e){
            e.printStackTrace();
        }

    }


    private void autoReconnect() {
        /*runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mNetworkTask.close();
                connect();
            }
        });*/
    }

    // Discontinues socket communication between iScale and Android
    public void disconnect() {

        if(mNetworkTask != null) {
            mNetworkTask.cancel(true);
            mNetworkTask.close(); // Close connection for enter
            Log.i("disconnect ()", "mNetworkTask");

        }
    }

    /*
     * Network connection with the Drench gun
     */
    public class GunNetworkTask extends AsyncTask<byte[], byte[], byte[]> {

        private Socket nsocket;
        private InputStream nis;
        private OutputStream nos;
        private boolean result = false;

        ProgressBar bar;
    //    private static final int MAX = 100;
        byte[] temp_data = {0};
        byte[] data = new byte[4096];

        int errorCode = 0;

        String fwVersion = null;
        byte[] command;
        long exitLoop = 0;

        public void setProgressBar(ProgressBar bar){
            this.bar = bar;
        }

        @Override
        protected void onPreExecute() {
            Log.i("AsyncTask", "onPreExecute");
        }

        @Override
        protected byte[] doInBackground(byte[]... params) {

            try {
                nsocket = new Socket();
                Pair<String, Integer> connection = Constants.getConnectionDetails(getActivity());
                nsocket.connect(new InetSocketAddress(connection.first, connection.second), Constants.getConnectionTimeout(getActivity()));

                // Send an instruction to the Gun (Enter)
                command = params[0];
                do {

                    if (nsocket.isConnected()) {
                        isConnecting = true;
                        byte[] buffer = new byte[4096];
                        int read;

                        // Establish streams
                        nis = nsocket.getInputStream();
                        nos = nsocket.getOutputStream();

                        Log.i("sendCommand", " Entered");

/*
                        int remainder = command.length%4;
                        int rows = command.length/4;
                        int index = 0;

                        for(int i = 0; i < rows; i++) {
                            System.out.print(String.format("%02x%02x%02x%02x", command[index++], command[index++], command[index++], command[index++]));
                            System.out.print(" ");
                        }
                        for(int k = 0; k < remainder; k++) {
                            System.out.print(String.format("%02x", command[index++]));
                            System.out.println();
                        }*/

                        nos.write(command);
                        nos.flush();

                        // Read the response
                        read = nis.read(buffer);
                        if(read > 0  && isConnecting){
                            temp_data = new byte[read];
                            System.arraycopy(buffer, 0, temp_data, 0, read);
                            /*for(byte t: temp_data)
                                Log.d("sendCommand", "temp bytes read: " + String.format("%08x", t));*/
                            errors = false;
                        }
                        else {
                            temp_data = null;
                            Log.d("sendCommand ", "-ve read");
                            errors = true;
                        }

                        decodeData(temp_data);
                        publishProgress(temp_data);// onProgressUpdate

                        // Wait until the user presses Ok
                        while (errors == false && ALERTPRESSED == FALSE) {
                            Thread.sleep(1000);
                        }

                        // User cancelled upgrade
                        if (currentTaskStatus == STATE_IDLE) {
                            errorCode = 1;
                            break;
                        }
                        else if(currentTaskStatus == STATE_FWBANK){
                        // To upgrade an old version, disconnect and reconnect the gun
                            currentTaskStatus = STATE_VERSIONNO;
                            BANKNOCHOSEN = TRUE;
                            disconnect();
                            connectGun();
                        }

                        Log.d("AsyncTask", "EndOfWhile " + errors);
                    }

                    // Escape early if cancel() is called
                    /*if (isCancelled()) break;*/
                    if (isCancelled()) return null;

                    // Loop while there is error and the no of error is less than 10
                    // Even when there is no error loop until it reaches the end of the code
                }while ((errors == true || exitLoop == 0) && errorCode < 10);

            } catch (IOException e) {
                Log.e("AsyncTask", "doInBackground: IOException");
                result = true;
                BANKNOCHOSEN = FALSE;

            } catch (Exception e) {
                Log.e("AsyncTask", "doInBackground: Exception");
                e.printStackTrace();
                result = true;
                BANKNOCHOSEN = FALSE;
            } finally {
                try {
                    nis.close();
                    nos.close();
                    nsocket.close();
                    isConnecting = false;
                    Log.d("sendCommand", "closed streams");
                } catch (IOException e) {
                    Log.e("AsyncTask", "Fail: IOException");
                } catch (Exception e) {
                    Log.e("AsyncTask", "Fail: Exception");
                    e.printStackTrace();
                }
                return temp_data;
            }
        }

        /*
         * brief  Decode the data received from the iGun
         * param  values
         * retval errorCode number of errors occurred
         */
        private int decodeData(byte[]... values){

            errors = false;
            if (values[0] == null || values.length < 1) {
                errors = true;
                return errorCode++;
            }

            Log.i("decodeData", values[0].length + " bytes received.");
            try {
                switch (currentTaskStatus) {
                    case STATE_VERSIONNO:
                        Log.i("decodeData ", "STATE_VERSIONNO.");
                        // Check firmware version.
                        if (parseForVersionString(values[0]) == TRUE) {
                            exitLoop = 0;
                            errorCode = 0;
                        }
                        else {
                            if (firmwareBankRequired == 0) {

                            }
                            else if (firmwareBankRequired > 0){

                            }
                            else{
                                // Something is wrong...
                            }
                            Log.e("decodeData", "error");
                        }
                        break;
                    case STATE_STARTTX:
                        Log.i("decodeData ", "STATE_STARTTX.");
                        // Check if it received 'C' back
                        if (parseForVersionString(values[0]) == TRUE) {
                            currentTaskStatus = STATE_INITPACKET;
                            command = startTransmittingInitial();
                            exitLoop = 0;
                            errorCode = 0;
                        }
                        else {
                            Log.e("decodeData", "error");
                            errorCode++;
                            errors = true;
                            exitLoop = 1;
                        }
                        break;
                    case STATE_INITPACKET:
                        Log.i("decodeData ", "STATE_INITPACKET.");
                        // Check if it received 'ACK' back
                        if (parseForVersionString(values[0]) == TRUE) {
                            exitLoop = 0;
                            currentTaskStatus = STATE_SENDPACKET;
                            command = startTransmittingData();
                            Log.d("decodeDataINITPACKET", "mYmodem.blkNumber: " + mYmodem.blkNumber);

                            errorCode = 0;
                        }
                        else {
                            exitLoop = 1;
                            errorCode++;
                            errors = true;
                        }

                        break;
                    case STATE_SENDPACKET:
                        Log.i("decodeData ", "STATE_SENDPACKET");
                        // Check if it received 'ACK' back
                        if (parseForVersionString(values[0]) == TRUE) {
                            exitLoop = 0;
                            long maxBlkNumber = 0;
                            // Loop until maximum block number is reached
                            if (firmwareBankRequired > 0){
                                maxBlkNumber = mYmodem.maxBlkNumberB1;
                            }
                            else if (firmwareBankRequired == 0){
                                maxBlkNumber = mYmodem.maxBlkNumberB0;
                            }
                            else{
                                // Something is wrong...
                            }

                            if(mYmodem.blkNumber <= maxBlkNumber) {
                            //    if(mYmodem.blkNumber <= 50) {
                                command = startTransmittingData();
                                Log.d("decodeData", "mYmodem.blkNumber: " + mYmodem.blkNumber);
                            }
                            else{
                                currentTaskStatus = SocketTaskStatus.STATE_WRITEEOT;
                                byte[] EOT = {(byte) 0x04};
                                command = EOT;
                            }

                            errorCode = 0;
                        }
                        else {
                            exitLoop = 1;
                            Log.w("decodeData", "Failed to send");
                            Log.w("decodeData", "mYmodem.blkNumber-> " + mYmodem.blkNumber);
                            errorCode++;
                            errors = true;
                        }
                        break;
                    case STATE_WRITEEOT:
                        Log.i("decodeData ", "STATE_WRITEEOT");
                        // Check if it received 'ACK' back
                        if (parseForVersionString(values[0]) == TRUE) {
                            command = startTransmittingLastData();
                            currentTaskStatus = SocketTaskStatus.STATE_LASTDATA;
                            exitLoop = 0;
                            errorCode = 0;
                        }
                        else {
                            exitLoop = 1;
                            errorCode++;
                            errors = true;
                        }
                        break;
                    case STATE_LASTDATA:
                        Log.i("decodeData ", "STATE_LASTDATA");
                        // Check if it received 'ACK' back
                        if (parseForVersionString(values[0]) == TRUE) {
                            // Old firmware exits without seeing SUCCESS
                            if(OLDFIRMWARE == TRUE) {
                                exitLoop = 1;
                                errorCode = 0;
                                OLDFIRMWARE = FALSE;
                            }
                            else {
                                // If it is Bank 1 or 2, go to state success
                                // If it is Bank 0, go to state end and finish
                                if(firmwareBankRequired > 0) {
                                    currentTaskStatus = STATE_SUCCESS;
                                    exitLoop = 0;
                                    command[0] = 0x00;
                                } else if (firmwareBankRequired == 0) {
                                    currentTaskStatus = STATE_END;
                                    exitLoop = 1;
                                }
                                errorCode = 0;
                            }
                        }
                        else {
                            exitLoop = 1;
                            errorCode++;
                            errors = true;
                        }
                        break;
                    case STATE_SUCCESS:
                        Log.i("decodeData ", "STATE_SUCCESS");
                        exitLoop = 1;
                        // Check if it received '<SUCCESS' back
                        if (parseForVersionString(values[0]) == TRUE) {
                            errorCode = 0;
                        }
                        else {
                            errorCode++;
                            errors = true;
                        }
                        break;

                    default:
                        errors = true;
                        break;
                }
            } catch(Exception e){
                e.printStackTrace();
            }
            return errorCode;
        }

        /*
         * brief  Parse the data received from the iGun
         * param  data
         * retval true if the data received matches with the string pattern
         *        false if there is no match
         */
        private boolean parseForVersionString(byte[] data){
            try {
                switch (currentTaskStatus) {
                    case STATE_VERSIONNO:   // <xx.xx.xx.Bx>
                        if(data.length >= 13){
                            String bigStr = new String(data);
                            String pattern = "(<\\w{2}.\\w{2}.\\w{2}.\\D{1}\\d{1}>)";
                            Pattern p = Pattern.compile(pattern);
                            Matcher matcher = p.matcher(bigStr);
                            if(matcher.find())
                            {
                                data = matcher.group(1).getBytes("UTF-8");
                                if ((data[0] == '<') && (data[10] == 'B') && (data[12] == '>')) {
                                    String versionNo = new String(data);
                                    int version1 = Integer.parseInt((versionNo.subSequence(1, 3)).toString());
                                    int version2 = 0, version3 = 0;
                                    // Not an invalid application
                                    if(version1 != 15) {
                                        version2 = Integer.parseInt((versionNo.subSequence(4, 6)).toString());
                                        version3 = Integer.parseInt((versionNo.subSequence(7, 9)).toString());
                                    }
                                    currentBank = Integer.parseInt((versionNo.subSequence(11, 12)).toString());

                                    if (currentBank == 0)
                                        firmwareBankRequired = 0;
                                    else if (currentBank == 1)
                                        firmwareBankRequired = 2;
                                    else if (currentBank == 2)
                                        firmwareBankRequired = 1;
                                    else
                                        firmwareBankRequired = -1;

                                    if (currentBank == 0) {
                                        // <00.00.00.B0> when application firmware is not present
                                        // <I5.I5.I5.B0> when there is no valid application
                                        if( ((version1 == 15) || (version1 == 0)) && (version2 == 0) && (version3 == 0)) {
                                            fwVersion = "Gun firmware is required. \r\n";
                                        }
                                        else{
                                            fwVersion = "Current firmware is " + String.format("%02d", version1) + "." + String.format("%02d", version2)
                                                    + "." + String.format("%02d", version3) + "\r\n";
                                        }
                                    }
                                    else if (currentBank > 0){
                                        fwVersion = "Current firmware is " + String.format("%02d", version1) + "." + String.format("%02d", version2)
                                                + "." + String.format("%02d", version3) + "\r\nWe need to update your gun to a new file format before upgrading the firmware. \r\n";
                                    }
                                    else{
                                        // Something has gone really wrong... Do something.
                                    }
                                    Log.d("parseForVersionString", "Match found");
                                    return true;
                                }
                            }
                            else {
                                pattern = "(\\w{7}\\s{1}\\w{6}\\s{1}\\W)";
                                p = Pattern.compile(pattern);
                                matcher = p.matcher(bigStr);
                                if(matcher.find()) {    // Invalid Number ! or Upgrade Firmware or Main Menu
                                    data = matcher.group(1).getBytes("UTF-8");
                                    if ((data[0] == 'I') && (data[1] == 'n') && (data[2] == 'v')) {
                                        //Old version found
                                        OLDFIRMWARE = TRUE;
                                        Log.i("parseForVersionString", "Invalid Number !");
                                        currentTaskStatus = SocketTaskStatus.STATE_FWBANK;
                                        // Old version does not output version number
                                        fwVersion = "";
                                        return true;
                                    }
                                    else
                                        Log.d("parseForVersionString", "Invalid Number not found");
                                }
                                else
                                    Log.d("parseForVersionString", "Match not found");
                            }
                        }
                        break;
                    case STATE_STARTTX:     // <READY>\r\rC
                        if (data.length >= 10 && ((data[0] == '<') && (data[6] == '>') && (data[9] == 'C'))) {
                            Log.d("parseForVersionString ", "<READY>\\r\\rC");
                            return true;
                        }
                        else if (data[0] == 'C') {
                            Log.d("parseForVersionString ", "C");
                            return true;
                        }
                        break;
                    case STATE_INITPACKET:  // ACK
                        if (data[0] == 6) {
                            Log.d("parseForVersionString ", "STATE_INITPACKET: ACK");
                            return true;
                        }
                        break;
                    case STATE_SENDPACKET:  // ACK
                        if (data[0] == 6) {
                            Log.d("STATE_SENDPACKET: ", "ACK");
                            return true;
                        }
                        break;
                    case STATE_WRITEEOT:    // ACK
                        if (data[0] == 6) {
                            Log.d("STATE_WRITEEOT: ", "ACK");
                            return true;
                        }
                        break;
                    case STATE_LASTDATA:    // ACK
                        if (data[0] == 6) {
                            Log.d("STATE_LASTDATA: ", "ACK");
                            if (firmwareBankRequired == 0) {
                                if ((data[1] == '<') && (data[2] == 'S') && (data[3] == 'U') && (data[4] == 'C')) {
                                    Log.d("STATE_SUCCESS: ", "<SUCCESS");
                                    return true;
                                }
                            }
                            else if (firmwareBankRequired > 0){
                                return true;
                            }
                            else{
                                // Something is wrong...
                            }
                        }
                        break;
                    case STATE_SUCCESS: // SUCCESS
                        if ((data[0] == '<') && (data[1] == 'S') && (data[2] == 'U') && (data[3] == 'C')) {
                            Log.d("STATE_SUCCESS: ", "<SUCCESS");
                            return true;
                        }
                        break;
                    default:
                        return false;
                }
            } catch(Exception e){
                e.printStackTrace();
            }
            return false;   // Data received didn't match the string pattern
        }

        /*
         * brief  Open the alert dialog after receiving the version number.
         *        Update the progress bar
         * param  values
         * retval void
         */
        @Override
        protected void onProgressUpdate(byte[]... values) {

            try {

                // No response from the Drench Gun
                if(values[0] == null){
                    ((Activity_Firmware) getActivity()).changeStatusTextView("Transmit failed.");
                    ((Activity_Firmware) getActivity()).changeInstrTextView("Please restart the Drench Gun");
                }

                switch (currentTaskStatus) {
                    case STATE_VERSIONNO:
                        Log.i("onProgressUpdate ", "STATE_VERSIONNO.");
                        // Check firmware version.
                        // If the firmware version is valid, ask the user
                        // whether to proceed with firmware upgrade or not
                        if (errorCode == 0) {
                            command = alert_fwUpload(fwVersion);
                        }
                        else {
                            Log.e("onProgressUpdate", "error");
                        }
                        break;
                    case STATE_FWBANK:
                        Log.i("onProgressUpdate ", "STATE_FWBANK.");
                        // Ask the user to insert the bank number
                        if (errorCode == 0) {
                            if(BANKNOCHOSEN == TRUE){
                                command = alert_fwUpload(fwVersion);
                                BANKNOCHOSEN = FALSE;
                            }else {
                                command = alert_bankNo(contn);
                            }
                        }
                        else {
                            Log.e("onProgressUpdate", "error");
                        }
                        break;
                    case STATE_STARTTX:
                        Log.i("onProgressUpdate ", "STATE_STARTTX.");
                        if (this.bar != null) {
                            bar.setProgress(10);
                        }
                        break;
                    case STATE_INITPACKET:
                        Log.i("onProgressUpdate ", "STATE_INITPACKET.");
                        if (this.bar != null) {
                            bar.setProgress(20);
                        }
                        break;
                    case STATE_SENDPACKET:
                        Log.i("onProgressUpdate ", "STATE_SENDPACKET");
                        if (this.bar != null) {
                            bar.setProgress((int)mYmodem.blkNumber*5/10+20);
                        //    bar.setProgress((int)(mYmodem.blkNumber/DIV)+20);
                        }
                        break;
                    case STATE_WRITEEOT:
                        Log.i("onProgressUpdate ", "STATE_WRITEEOT");
                        if (this.bar != null) {
                            bar.setProgress(80);
                        }
                        break;
                    case STATE_LASTDATA:
                        Log.i("onProgressUpdate ", "STATE_LASTDATA");
                        if (this.bar != null) {
                            bar.setProgress(90);
                        }
                        break;
                    default:
                        break;
                }
            } catch(Exception e){
                e.printStackTrace();
            }
        }

        @Override
        protected void onPostExecute(byte[] temp_data) {

            try {
                if (temp_data == null)
                    return;

                // Connection with Drench Gun failed
                if (result) {
                    Log.e("AsyncTask", "onPostExecute: Completed with an Error.");

                    ((Activity_Firmware) getActivity()).changeStatusTextView("Transmit Incomplete or Interrupted.");
                    ((Activity_Firmware) getActivity()).changeInstrTextView("The firmware upload has been cancelled due to network error");
                    result = false;
                } else {
                    if(errorCode > 0) {
                        ((Activity_Firmware) getActivity()).changeStatusTextView("Transmit Incomplete or Interrupted.");
                        ((Activity_Firmware) getActivity()).changeInstrTextView("The firmware upload has exited with code "
                                + errorCode + "."
                                + " If you cancelled the upload then exit the app, "
                                + "otherwise press GO to try again.");
                    }
                    else{
                        if (currentBank == 0) {
                            ((Activity_Firmware) getActivity()).changeStatusTextView("Firmware update Completed");
                            ((Activity_Firmware) getActivity()).changeInstrTextView("Power cycle Drench Gun to "
                                    + "complete the Upgrade process, then exit this app");
                        }
                        else if (currentBank > 0){
                            ((Activity_Firmware) getActivity()).changeStatusTextView("File Format Update Completed");
                            ((Activity_Firmware) getActivity()).changeInstrTextView("Power cycle Drench Gun to "
                                    + "complete the File Format Upgrade process. Follow the gun screen instructions, until the gun is back in"
                                    + "\"upgrade mode\".\r\n You will then need to go back into the phone Wifi settings and re-connect to "
                                    + "the \"tepari_EG_upgrade\" network again. \r\nPress the Go button once this is complete.");
                        }

                        // Add a tick image at the top
                        tickImage = new ImageView(getActivity());
                        tickImage.setBackgroundResource(R.drawable.bluetick);
                        linearLayout.addView(tickImage, 0);
                        tickImage.getLayoutParams().height = 50;
                        tickImage.getLayoutParams().width = 50;

                        // Center the tick image
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) tickImage.getLayoutParams();
                        params.gravity = Gravity.CENTER;
                        // Add padding at the top, left and right
                        params.setMargins(40, 40, 40, 0);
                        tickImage.setLayoutParams(params);

                        tickImage.requestLayout();
                        if (this.bar != null) {
                            bar.setProgress(MAX);
                        }

                        // Change the button to OK
                        goButton.setText("Ok");
                    }
                    Log.i("AsyncTask", "onPostExecute: Completed.");
                }
                disconnect();
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        protected void onCancelled() {
            Log.i("AsyncTask", "Cancelled.");
        }

        void setData(byte[] buf){
            data = buf;
        }

        // Close socket to iScale and disconnect
        public void close() {
            try {
                Log.i("AsyncTask", "Call close socket");
                errorCode = 0;
                ALERTPRESSED = FALSE;
                errors = false;
                mYmodem.blkNumber = 0x01;
                /*if (this.bar != null) {
                    bar.setProgress(MAX);
                }*/
                if(isConnecting) {
                    isConnecting = false;
                    //   isAutoReconnect = false;
                    nsocket.shutdownInput();
                    nsocket.shutdownOutput();
                    Log.i("AsyncTask", "nsocket.shutdownInput()Output()");
                }
            } catch (Exception e) {
                Log.i("AsyncTask", "Fail close");
                e.printStackTrace();
            }
        }

        public void cancelUpgrade(){
            byte[] buf = new byte[10];
            buf[0] = 'a';
            buf[1] = '\r';
            buf[2] = '\n';
            // write to outputstream?
        }
    }

    /*
     * Connect DG_Ymodem to the Fragment
    */
    public void setYmodem(DG_Ymodem modem){
        mYmodem = modem;
        mYmodem.setFragment(this);

    }

}
