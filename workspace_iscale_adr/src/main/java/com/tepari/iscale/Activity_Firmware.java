package com.tepari.iscale;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tepari.tpc.R;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;


/**
 * Created by 2fDesign1 on 16/11/2016.
 */
/*
public class Activity_Firmware extends Activity {*/
    public class Activity_Firmware extends Activity {

    // Show download progress
    private ProgressBar spinner;
    static final int MAX = 10;
    TextView statusLabel;
    TextView instructionsLabel;

    private ImageButton btn_connect_wifi;

 //   private char firmwareReady = 0;
    private char downloadsComplete = 0;

    // Firmware URL

    private static final String EG_B0 = "https://www.trisport.co.nz/EGFirmware/EG_B0.bin";
//    private static final String EG_B1 = "https://www.trisport.co.nz/EGFirmware/EG_B1.bin";
 //   private static final String EG_B2 = "https://www.trisport.co.nz/EGFirmware/EG_B2.bin";
    private static final String UPGRADE_B1 = "https://www.trisport.co.nz/EGFirmware/UPGRADE_B1.bin";
    private static final String UPGRADE_B2 = "https://www.trisport.co.nz/EGFirmware/UPGRADE_B2.bin";
    private static final String PLIST = "https://www.trisport.co.nz/EGFirmware/EGFirmware.plist";
/*    private static final String EG_B1 = "https://www.trisport.co.nz/EGFirmwareTest/EG_B1.bin";//1.3.09
    private static final String EG_B2 = "https://www.trisport.co.nz/EGFirmwareTest/EG_B2.bin";
    private static final String PLIST = "https://www.trisport.co.nz/EGFirmwareTest/EGFirmware.plist";*/

    // Firmware buffer
    byte[] firmwareB1Raw = new byte[64000];
    byte[] firmwareB2Raw = new byte[64000];
    byte[] firmwareB0Raw = new byte[128000];

    DownloadWebpageTask task;
    FileSettings setting = new FileSettings();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firmware);

        //--- Set progress bar max to 10 ---
      /*  progressBar = (ProgressBar) findViewById(R.id.progressBarHorizontal);
        progressBar.setMax(MAX);
*/
        //--- Show spinner while downloading firmware from internet ---
        spinner = (ProgressBar) findViewById(R.id.firmwareProgressBar);

        //--- Show the network status ---
        statusLabel = (TextView) findViewById(R.id.statusLabel);

        //--- Show instructions to upgrade the firmware ---
        instructionsLabel = (TextView) findViewById(R.id.instructionsLabel);

        connect();     // Connect to the internet

        // Open the Wifi setting
        btn_connect_wifi = (ImageButton) findViewById(R.id.btn_connect_wifi);

        btn_connect_wifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("btn_connect_wifi", "pressed");
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        });
    }

    @Override
    public void onBackPressed() {
           if (getFragmentManager().getBackStackEntryCount() == 0) {
        this.finish();
        } else {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //In case the task is currently running
        // Kill AsyncTask
        disconnect();
    }

    // Discontinues communication between internet and Android
    private void disconnect() {
        task.cancel(true);
        task = null;
    //    progressBar.setProgress(0);
        spinner.setProgress(0);
    }

    // Connect to the internet
    private int connect() {

        // Check to see whether a network connection is available
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()){
            // Start fetching data
            Log.d("connect", "Fetching First Data");

        /*    mProgressStatus = 1;
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setProgress(0);
*/
            spinner.setVisibility(View.VISIBLE);
            spinner.setProgress(0);

            task = new DownloadWebpageTask();
  //          task.setProgressBar(progressBar);
            task.execute(UPGRADE_B1, UPGRADE_B2, PLIST, EG_B0);    // open two websites at once
        }
        else{
            // display error
            statusLabel.setText("Network Error");
        }
        return 0;
    }



    // Uses AsyncTask to create a task away from the main UI thread. This task takes a
    // URL string and uses it to create an HttpUrlConnection. Once the connection
    // has been established, the AsyncTask downloads the contents of the webpage as
    // an InputStream. Finally, the InputStream is converted into a string, which is
    // displayed in the UI by the AsyncTask's onPostExecute method.
    private class DownloadWebpageTask extends AsyncTask<String, Integer, Boolean> {

     /*   ProgressBar bar;
        public void setProgressBar(ProgressBar bar){
            this.bar = bar;
        }
*/
        private String firmwareVersionNo;

        @Override
        protected void onPreExecute() {

            Log.i("AsyncTask", "onPreExecute");
        }

        @Override
        protected Boolean doInBackground(String... urls) {

            int totalSize;
            // params comes from the execute() call: params[0] is the url.
            try {
                Log.d("AsyncTask", urls[0] + " + " + urls[1] + " + " + urls[2] + " + " + urls[3]);
                firmwareVersionNo = downloadUrl3(urls[2]);      // EGFirmware.plist
                totalSize = downloadUrl0(urls[3]);              // EG_B0.bin
                publishProgress(totalSize);
                totalSize = downloadUrl1(urls[0]);              // UPGRADE_B1.bin
                publishProgress(totalSize);
                totalSize = downloadUrl2(urls[1]);              // UPGRADE_B2.bin
                publishProgress(totalSize);

                return TRUE;
            } catch (Exception e) {
                Log.e("Activity_Firmware", "doInBackground: Unable to retrieve web page. URL may be invalid.");
                return FALSE;
            }
        }

        protected void onProgressUpdate(Integer... values){
            super.onProgressUpdate();
           /* if (this.bar != null){
                bar.setProgress(values[0]);
            }*/
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(Boolean result) {

            // Download succeeded
            if (result == TRUE) {

                statusLabel.setText("Software version " + firmwareVersionNo + " ready");

                spinner.setVisibility(View.INVISIBLE);
                spinner.setProgress(100);

                // show upgrade instruction, button and progress bar
                instructionsLabel.setVisibility(View.VISIBLE);

                // Add a fragment for Go button and progress bar
                addFragment();
            }
            else {
                // Download failed
                statusLabel.setText("Firmware failed to download");
            }
        }
    }

    // Given a URL, establishes an HttpUrlConnection and retrieves
// the web page content as a InputStream, which it returns as
// a string.
    private int downloadUrl0(String myurl) throws IOException {
        InputStream is = null;
        HttpURLConnection conn = null;

        try {
            URL url = new URL(myurl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            final int response = conn.getResponseCode();
            Log.d("downloadUrl", "The response is: " + response);

         /*   Handler refresh = new Handler(Looper.getMainLooper());
            refresh.post(new Runnable() {
                public void run()
                {
                    statusLabel.setText("The response is: " + response);
                }
            });*/

            is = conn.getInputStream();

            // Convert the InputStream into a byte array
            int noBytesReadB0 = readItB0(is, firmwareB0Raw);
            downloadsComplete++;

            return MAX / 2;

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } finally {
            try {
                if (is != null) {
                    is.close();
                    Log.i("downloadUrl0", "is: closed");
                }
                if (conn != null) {
                    conn.disconnect();
                    Log.i("downloadUrl0", "conn: closed");
                }
            } catch (IOException e) {
                Log.e("downloadUrl0", "Fail: IOException");
            } catch (Exception e) {
                Log.e("downloadUrl0", "Fail: Exception");
            }
        }
    }

    // Given a URL, establishes an HttpUrlConnection and retrieves
// the web page content as a InputStream, which it returns as
// a string.
    private int downloadUrl1(String myurl) throws IOException {
        InputStream is = null;
        HttpURLConnection conn = null;

        try {
            URL url = new URL(myurl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            final int response = conn.getResponseCode();
            Log.d("downloadUrl", "The response is: " + response);

         /*   Handler refresh = new Handler(Looper.getMainLooper());
            refresh.post(new Runnable() {
                public void run()
                {
                    statusLabel.setText("The response is: " + response);
                }
            });*/

            is = conn.getInputStream();

            // Convert the InputStream into a byte array
            int noBytesReadB1 = readIt(is, firmwareB1Raw);
            downloadsComplete++;

            return MAX / 2;

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } finally {
            try {
                if (is != null) {
                    is.close();
                    Log.i("downloadUrl1", "is: closed");
                }
                if (conn != null) {
                    conn.disconnect();
                    Log.i("downloadUrl1", "conn: closed");
                }
            } catch (IOException e) {
                Log.e("downloadUrl1", "Fail: IOException");
            } catch (Exception e) {
                Log.e("downloadUrl1", "Fail: Exception");
            }
        }
    }

    // Second website
    private int downloadUrl2(String myurl) throws IOException {
        InputStream is = null;
        HttpURLConnection conn = null;

        try {
            URL url = new URL(myurl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            final int response = conn.getResponseCode();
            Log.d("downloadUrl", "The response is: " + response);

            is = conn.getInputStream();

            // Convert the InputStream into a byte array
            int noBytesReadB2 = readIt(is, firmwareB2Raw);
            downloadsComplete++;

            if(downloadsComplete == 3){
                downloadsComplete = 0;
                // Save firmware into the phone directory
                setting.saveFirmwareFileB0(firmwareB0Raw, this);
                setting.saveFirmwareFileB1(firmwareB1Raw, this);
                setting.saveFirmwareFileB2(firmwareB2Raw, this);
            }

            return MAX;

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } finally {
            try {
                if (is != null) {
                    is.close();
                    Log.i("downloadUrl2", "is: closed");
                }
                if (conn != null) {
                    conn.disconnect();
                    Log.i("downloadUrl2", "conn: closed");
                }
            } catch (IOException e) {
                Log.e("downloadUrl2", "Fail: IOException");
            } catch (Exception e) {
                Log.e("downloadUrl2", "Fail: Exception");
            }
        }
    }


    // Extract Firmware version from plist
    private String downloadUrl3(String myurl) throws IOException, XmlPullParserException {
        InputStream is = null;
        HttpURLConnection conn = null;

        // Instantiate the parser
        String plist = null;

        try {
            URL url = new URL(myurl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            final int response = conn.getResponseCode();
            Log.d("downloadUrl", "The response is: " + response);

            is = conn.getInputStream();
            //plist = xmlParser(is);
            // Save firmware version number
            plist = setting.saveFirmwareInfo(is, this);

            Log.d("downloadUrl3", "The plist is: " + plist);

            return plist;

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } finally {
            try {
                if (is != null) {
                    is.close();
                    Log.i("downloadUrl3", "is: closed");
                }
                if (conn != null) {
                    conn.disconnect();
                    Log.i("downloadUrl3", "conn: closed");
                }
            } catch (IOException e) {
                Log.e("downloadUrl3", "Fail: IOException");
            } catch (Exception e) {
                Log.e("downloadUrl3", "Fail: Exception");
            }
        }

    }

    // Reads an InputStream and converts it to a byte array.
    // The file has extra space at the end?
    private int readIt(InputStream stream, byte[] firmwareRaw) throws IOException, UnsupportedEncodingException {
        int read = 0;   // no of bytes read. -1 if end of stream
        int offset = 0;

        // Read no of bytes from the input stream until the end of the stream is reached.
        while(read != -1) {
            offset += read;
            read = stream.read(firmwareRaw, offset, 64000-offset);
        }
        Log.d("readIt", "No of bytes read: " + offset);
        return read;
    }

    // Reads an InputStream and converts it to a byte array.
    // The file has extra space at the end?
    private int readItB0(InputStream stream, byte[] firmwareRaw) throws IOException, UnsupportedEncodingException {
        int read = 0;   // no of bytes read. -1 if end of stream
        int offset = 0;

        // Read no of bytes from the input stream until the end of the stream is reached.
        while(read != -1) {
            offset += read;
            read = stream.read(firmwareRaw, offset, 128000-offset);
        }
        Log.d("readIt", "No of bytes read: " + offset);
        return read;
    }

    // Firmware Upgrading fragment
    private void addFragment() {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        Fragment_DgUpgradeProtocol fragment = new Fragment_DgUpgradeProtocol();
        fragmentTransaction.add(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
    }

    /* Called from Fragment_DgUpgradeProtocol
     * to change the textView
     */
    public void changeStatusTextView(String status){
        statusLabel.setText(status);
    }


    public void changeInstrTextView(String instruction){
        instructionsLabel.setText(instruction);
    }
}
