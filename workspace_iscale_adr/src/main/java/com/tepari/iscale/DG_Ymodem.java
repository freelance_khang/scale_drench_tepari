package com.tepari.iscale;

import android.content.Context;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;

/**
 * Created by 2fDesign1 on 1/12/2016.
 */

public class DG_Ymodem {

    private static final short FILE_NAME_LENGTH = 16;
    private static final short PACKET_HEADER = 3;
    private static final short PACKET_SIZE = 128;
    public static final short PACKET_1K_SIZE = 1024;
    private static final short PACKET_TRAILER = 2;
    private static final short PACKET_OVERHEAD = (PACKET_HEADER + PACKET_TRAILER);

    private static final byte NUL = 0x00;
    private static final byte SOH = 0x01;
    private static final byte STX = 0x02;
    private static final byte EOT = 0x04;

    Fragment_DgUpgradeProtocol mFragment;
    private Context mContext;

    public static byte blkNumber = 0x01;
//    public long sizeData = 51200;
    public static final int DATA_SIZE = 105472;
 //   public long sizeData = DATA_SIZE;
    public long firmwareB0Length = 105472;
    public long firmwareB1Length = 51200;
    public long firmwareB2Length = 51200;

    public long maxBlkNumberB0 = firmwareB0Length/PACKET_1K_SIZE;
    public long maxBlkNumberB1 = firmwareB1Length/PACKET_1K_SIZE;

    long addressData = 0;

    public void setFragment(Fragment_DgUpgradeProtocol fragment) {
        mFragment = fragment;
    }

    public DG_Ymodem(Context context){
        mContext = context;
    }

    byte[] Ymodem_transmit_initialPacket(final byte[] sendFileName, long sizeFile){
        try {

            byte FileName [] = new byte[FILE_NAME_LENGTH];
            boolean CRC16_F;
            byte[] packet_data = new byte[PACKET_1K_SIZE + PACKET_OVERHEAD];
            long tempCRC;
            byte[] arr_combined;
            byte tempMSB;
            byte tempLSB;
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            // Copy the file name
            for (char i = 0; i < (FILE_NAME_LENGTH - 1); i++) {
                FileName[i] = sendFileName[i];
            }
            CRC16_F = true;

            // Prepare first block
            Ymodem_PrepareInitialPacket(packet_data, FileName, sizeFile);

            // Send packet
            if (CRC16_F) {
                tempCRC = Cal_CRC16(packet_data, PACKET_SIZE);

                tempMSB = (byte) (tempCRC >> 8);
                tempLSB = (byte) (tempCRC & 0xFF);

                out.write(packet_data, 0, PACKET_SIZE + PACKET_HEADER);
                out.write(tempMSB);
                out.write(tempLSB);
                arr_combined = out.toByteArray();
                // Reset array
                out.reset();

                return arr_combined;
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    byte[] Ymodem_transmit_dataPacket(byte[] buf, long sizeData){
        try {
            boolean CRC16_F;
            byte[] packet_data = new byte[PACKET_1K_SIZE + PACKET_OVERHEAD];
            long tempCRC;
            byte[] arr_combined = null;
            byte tempMSB;
            byte tempLSB;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            CRC16_F = true;

            byte[] buf_ptr = buf;

            long pktSize;

            // Here 1024 bytes package is used to send the packets
            Ymodem_PreparePacket(buf_ptr, packet_data, blkNumber, sizeData, addressData);

            // Send next packet
            if (sizeData >= PACKET_1K_SIZE)
                pktSize = PACKET_1K_SIZE;       // 1024
            else
                pktSize = PACKET_SIZE;          // 128

            // Send CRC or Check Sum based on CRC16_F
            if(CRC16_F){
                tempCRC = Cal_CRC16(packet_data, pktSize);
                tempMSB = (byte) (tempCRC >> 8);
                tempLSB = (byte) (tempCRC & 0xFF);

                out.write(packet_data, 0, (int)pktSize + PACKET_HEADER);
                out.write(tempMSB);
                out.write(tempLSB);
                arr_combined = out.toByteArray();

                // Reset array
                out.reset();

                Log.d("CRC: ", "tempMSB-" +tempMSB + "tempLSB-" +tempLSB);
            }
            // If the packet is sent successfully,
            // send the next packet
            if (sizeData > pktSize){
                addressData += pktSize;
                sizeData -= pktSize;
            }
            else {
                addressData += pktSize;
                sizeData = 0;
            }
            blkNumber++;
            return arr_combined;

        }catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }


    byte[] Ymodem_transmit_LastPacket(){
        try {

            byte[] packet_data = new byte[PACKET_1K_SIZE + PACKET_OVERHEAD];
            long tempCRC;
            byte[] arr_combined;
            byte tempMSB;
            byte tempLSB;
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            Ymodem_PrepareLastPacket(packet_data);

            // Send packet
            tempCRC = Cal_CRC16(packet_data, PACKET_SIZE);

            tempMSB = (byte) (tempCRC >> 8);
            tempLSB = (byte) (tempCRC & 0xFF);

            out.write(packet_data, 0, PACKET_SIZE + PACKET_HEADER);
            out.write(tempMSB);
            out.write(tempLSB);
            arr_combined = out.toByteArray();

            // Reset array
            out.reset();

            return arr_combined;

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /*
     * Prepare the first block
     * {0x01, 0x00, 0xff, filename, 0x00, sizeoffile, 0x00 .. 0x00}
     * 128 + 3 Bytes
     */
    void Ymodem_PrepareInitialPacket (byte[] data, final byte[] fileName, long length){
        int i, j;
        byte[] file_ptr = null;
    try {
        // Make the first three packet
        data[0] = SOH; // SOH
        data[1] = NUL;
        data[2] = (byte) 0xff;

        // Send file name
        for (i = 0; (fileName[i] != '\0') && (i < FILE_NAME_LENGTH); i++) {
            data[i + PACKET_HEADER] = fileName[i];
            //    Log.d("Ymodem_InitialPacket", "data[" + i + "]: " + data[i]);
        }

        // Send NUL
        data[i + PACKET_HEADER] = 0x00;
    ///    Log.d("Ymodem_InitialPacket", "data[" + i + "]: " + data[i]);

        // Send file size in ASCII
        String l = String.valueOf(length);
        file_ptr = l.getBytes("UTF-8");

        for (j = 0, i = i + PACKET_HEADER + 1; j < file_ptr.length; ) {
            data[i++] = file_ptr[j++];
         //   Log.d("Ymodem_InitialPacket", "data[" + i + "]: " + data[i]);
        }

        // Zero padding the rest
        for (j = i; j < PACKET_SIZE + PACKET_HEADER; j++) {
            data[j] = 0;
        }

        // dummy data
         /*   char[] dummyData = {0x01, 0x00, 0xff, 0x45, 0x47, 0x2e, 0x62,
                                0x69, 0x6e, 0x00, 0x35, 0x31, 0x32, 0x30, 0x30, 0x20, 0x31,
                                0x32, 0x31, 0x36, 0x37, 0x30, 0x30, 0x34, 0x32, 0x32,
                                0x20, 0x30, 0x20, 0x30, 0x20, 0x31, 0x20, 0x35, 0x31, 0x32,
                                0x30, 0x30};
            for(int x = 0; x < 15; x++){
                data[x] = (byte)(dummyData[x]&0x00ff);
            }*/
    } catch (Exception e){
        Log.e("PrepareInitialPacket", "Error");
        e.printStackTrace();
    }

    }

    /*
     * Calculate CRC16 for YModem Packet
     * Return CRC
     */
    long Cal_CRC16 (final byte[] data, long size){
        long crc = 0;
        long index = 3; // Ignore first three bytes
        final long dataEnd = size;
        while (index < dataEnd+3) {
            crc = UpdateCRC16(crc, data[(int) index++]);
        }
        crc = UpdateCRC16(crc, (byte) 0);
        crc = UpdateCRC16(crc, (byte) 0);
        return (crc & 0xFFFF);

    }

    /*
     * Update CRC16 for input byte
     * Return computed CRC
     */
    long UpdateCRC16 (long crcIn, byte bytes){
        long crc = crcIn;
        long in = ((bytes & 0xFF)|0x0100) & 0x1FF;      // Set 9th bit high
        do{
            crc = (crc << 1) & 0x1FFFF;                 // Shift crc 1 bit to the left
            in = (in << 1) & 0x1FFFF;                   // Shift in 1 bit to the left
            if ((in & 0x0100) > 0)                      // If 9th bit is high
                crc = (1 + crc) & 0x1FFFF;              // 1st bit is high otherwise low
            if ((crc & 0x10000) > 0)                    // If 17th bit is high
                crc = (crc ^ 0x1021) & 0x1FFFF;
        }
        while ((in & 0x10000) == 0);                    // Repeat while 17th bit is not set

        return (crc & 0xFFFF);

    }

    private void Ymodem_PreparePacket(byte[] SourceBuf, byte[] data, byte pktNo, long sizeBlk, long address) {
     //   private void Ymodem_PreparePacket(byte[] SourceBuf, byte[] data, byte pktNo, long sizeBlk, long address) {
        short i, size, packetSize;
        long sizeL;
        byte[] file_ptr;

        // Make first three packet
        packetSize = sizeBlk >= PACKET_1K_SIZE ? PACKET_1K_SIZE : PACKET_SIZE;
        sizeL = sizeBlk < (long) packetSize ?  sizeBlk : (long) packetSize;
        size = (short)(sizeL&0xFFFF);
        //size = (short) sizeBlk < packetSize ? (short) sizeBlk : packetSize;
        if (packetSize == PACKET_1K_SIZE)
            data[0] = STX;
        else
            data[0] = SOH;
        data[1] = pktNo;
        data[2] = (byte)((~pktNo) & 0xFF);  // inversion is done in int so need to downcast
        file_ptr = SourceBuf;

     /*  Log.d("Ymodem_PreparePacket", "data[0]: " + data[0]);
        Log.d("Ymodem_PreparePacket", "data[1]: " + data[1]);
        Log.d("Ymodem_PreparePacket", "data[2]: " + data[2]);*/
        // Filename packet has valid data
        for (i = PACKET_HEADER; i < size + PACKET_HEADER; i++, address++){
            data[i] = (byte)(file_ptr[(int)address] & 0xFF);
        //    Log.d("Ymodem_PreparePacket", "data[" + i + "]: " + data[i]);
        }
        if (size <= packetSize){
            for (i = (short) (size + PACKET_HEADER); i < packetSize + PACKET_HEADER; i++){
                data[i] = 0x1A; // EOF (0x1A) or 0x00
            }

        }
    }

    /*
     * Prepare the last block
     * {0x01, 0x00, 0xff, 0x00, .. 0x00}
     * 128B + 3B
     */
    void Ymodem_PrepareLastPacket (byte[] data){
        int i;
        try {
            // Make the first three packet
            data[0] = SOH; // SOH
            data[1] = NUL;
            data[2] = (byte) 0xff;

        /*    Log.d("Ymodem_PrepareLast", "data[0]: " + data[0]);
            Log.d("Ymodem_PrepareLast", "data[1]: " + data[1]);
            Log.d("Ymodem_PrepareLast", "data[2]: " + data[2]);*/
            // Send file name
            for (i = PACKET_HEADER; i < (PACKET_SIZE + PACKET_HEADER); i++) {
                data[i] = 0x00;
             //   Log.d("Ymodem_PrepareLast", "data[" + i + "]: " + data[i]);
            }

        } catch (Exception e){
            Log.e("PrepareLastPacket", "Error");
            e.printStackTrace();
        }

    }
}
