package com.tepari.iscale.wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;

import com.tepari.iscale.TePari_Application;


public class ConnectWifiTask extends AsyncTask<String, Void, Boolean> {
    private static final int RUN_CONNECT_TO_CAMERA_TASK_SUCCESS     = 0;
    private static final int CONNECT_TO_NETWORK_FAILED = 1;
    private static final int ENABLE_NETWORK_INTERVAL = 5;


    private Context mContext;
    private WifiBr br;
    private Object wifiLockObject = new Object();

    String TAG = ConnectWifiTask.class.getSimpleName();

    Listener listener;

    public ConnectWifiTask(Context context) {
        this.mContext = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        br = new WifiBr();
        IntentFilter i = new IntentFilter();
        i.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        mContext.registerReceiver(br, i);
    }

    @Override
    protected void onPostExecute(Boolean connected) {
        super.onPostExecute(connected);

        try {
            mContext.unregisterReceiver(br);
        } catch (Exception e){}

        if (listener != null) {
            listener.onFinish(connected);
            listener = null;
        }
    }

    @Override
    protected Boolean doInBackground(String... strings) {
        String deviceSsid = strings[0];

        WifiManager mWifiManager = (WifiManager) TePari_Application.getApplication().getApplicationContext().getSystemService (Context.WIFI_SERVICE);
        int deviceNetworkId = NetworkHelper.findSavedNetworkId(mWifiManager, deviceSsid);

        // add network
        if (deviceNetworkId == NetworkHelper.NETWORK_NOT_FOUND) {
            deviceNetworkId = NetworkHelper.addOpenNetwork(mWifiManager, deviceSsid);
            Log.d(TAG, "Add device network DONE, new network id: " + deviceNetworkId);

            boolean saveDeviceNetworkSuccess = mWifiManager.saveConfiguration();
            Log.d(TAG, "Save new device network DONE, success: " + saveDeviceNetworkSuccess);
        }

//        boolean enableNetworkSuccess = NetworkHelper.enableNetwork(mWifiManager, deviceNetworkId);
//        Log.d(TAG, "Change wifi network DONE, success: " + enableNetworkSuccess);
//        boolean connected = true;

        boolean connected = false;
        if (deviceNetworkId > -1) {
            int newNetworkId = -1;
            int retries = 2;

            do {
                if (isCancelled()) {
                    break;
                }

                if (!NetworkHelper.isConnectingToSsid(mWifiManager, deviceSsid)) {
                    // If found device SSID, connect to it
                    boolean enableNetworkSuccess = NetworkHelper.enableNetwork(mWifiManager, newNetworkId);
                    Log.d(TAG, "Enable device network done, success: " + enableNetworkSuccess);
                    Log.d(TAG, ">>> current connection ip: " + (mWifiManager.getConnectionInfo() != null ? mWifiManager.getConnectionInfo().getIpAddress() : ""));

                    // need to wait for the network to be up
                    synchronized (wifiLockObject) {
                        try {
                            wifiLockObject.wait(15000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            if (isCancelled()) {
                                Log.d(TAG, "connectToDeviceWifi is cancelled, disable network: " + newNetworkId);
                                mWifiManager.disableNetwork(newNetworkId);
                                break;
                            }
                        }
                    }
                } else {
                    Log.d(TAG, "connectToDeviceWifi, app already connected to selected SSID");
                }

                /*
                 * This loop make sure app get IP address successfully from DHCP.
                 */
                int waiting_retries = 20;
                do {
                    if ((mWifiManager.getConnectionInfo() != null &&
                            mWifiManager.getConnectionInfo().getIpAddress() != 0) &&
                            mWifiManager.getDhcpInfo() != null && mWifiManager.getDhcpInfo().ipAddress != 0) {

                        //We're connected but don't have any IP yet
                        Log.d(TAG, "IP:" + NetworkHelper.getCurrentIpAddress(mWifiManager));

                        Log.d(TAG, "SV:" + ((mWifiManager.getDhcpInfo().serverAddress & 0xFF000000) >> 24) + "." +
                                ((mWifiManager.getDhcpInfo().serverAddress & 0x00FF0000) >> 16) + "." +
                                ((mWifiManager.getDhcpInfo().serverAddress & 0x0000FF00) >> 8) + "." +
                                (mWifiManager.getDhcpInfo().serverAddress & 0x000000FF));

                        Log.d(TAG, "Checking ip address, current ssid: " + NetworkHelper.getCurrentWifiSsid(mWifiManager));
                        if (NetworkHelper.isConnectingToSsid(mWifiManager, deviceSsid)) {
                            connected = true;
                            break;
                        } else {
                            // Re-enable network
                            if (waiting_retries % ENABLE_NETWORK_INTERVAL == 0) {
                                newNetworkId = NetworkHelper.getAddedNetworkId(TePari_Application.getApplication().getApplicationContext(), deviceSsid);
                                Log.d(TAG, "Connected to a normal network -> re-enable expected network: " + deviceSsid + ", id: " + newNetworkId);
                                boolean enableCamNetworkSuccess = NetworkHelper.enableNetwork(mWifiManager, newNetworkId, false);
                                Log.d(TAG, "re-enableNetwork: " + newNetworkId + " : " + enableCamNetworkSuccess);
                            }
                        }
                    }

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                while (waiting_retries-- > 0 && !isCancelled());
            }
            while (retries-- > 0 && !isCancelled());
        }

        // sleep here for 3 seconds to make sure app can reconnect device tcp
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "Connecting to device network success: " + connected);
        return connected;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    private class WifiBr extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            if (wifiManager != null && wifiManager.getDhcpInfo() != null) {
                Log.d(TAG, "Current wifi: " + wifiManager.getConnectionInfo().getSSID()
                        + ", Ip address: " +  NetworkHelper.getCurrentIpAddress(wifiManager));
                if (wifiManager.getDhcpInfo().ipAddress != 0) {
                    synchronized (wifiLockObject) {
                        wifiLockObject.notify();
                    }
                }
            }
        }
    }

    public interface Listener {
        void onFinish(boolean connected);
    }
}
