package com.tepari.iscale.wifi;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.util.Log;


import com.tepari.iscale.Helper.Constants;
import com.tepari.iscale.TePari_Application;
import com.tepari.tpc.R;

import java.util.List;

public class NetworkHelper {
    public static String TAG = NetworkHelper.class.getSimpleName();
    public static final int NETWORK_NOT_FOUND = -1;

    public static boolean isOpenWifi(String capability) {
        final String[] securityModes = {"WEP", "WPA", "WPA2"};
        for (int i = securityModes.length - 1; i >= 0; i--) {
            if (capability.contains(securityModes[i])) {
                return false;
            }
        }

        return true;
    }

    public static boolean isConnectingToSsid(WifiManager wifiManager, String ssid) {
        if (wifiManager.getConnectionInfo() != null) {
            String currentSsidNoQuote = convertToNoQuotedString(getCurrentWifiSsid(wifiManager));
            return ssid.equals(currentSsidNoQuote);
        }

        return false;
    }

    public static String getCurrentWifiSsid(WifiManager wifiManager) {
        String currSsid = "";
        if (wifiManager != null) {
            WifiInfo netInfo = wifiManager.getConnectionInfo();
            if (netInfo != null) {
                currSsid = convertToNoQuotedString(netInfo.getSSID());
            }
        }
        return currSsid;
    }

    public static String convertToNoQuotedString(String string) {
        String no_quoted_str = string;
        if (string != null && string.indexOf("\"") == 0 && string.lastIndexOf("\"") == string.length() - 1) {
            no_quoted_str = string.substring(1, string.length() - 1);
        }
        return no_quoted_str;
    }

    public static String convertToQuotedString(String string) {
        return "\"" + string + "\"";
    }

    public static int findSavedNetworkId(WifiManager wifiManager, String ssid) {
        int networkId = NETWORK_NOT_FOUND;

        Log.d(TAG, "Find saved network id for ssid: " + ssid);
        List<WifiConfiguration> savedWifiNetworks = wifiManager.getConfiguredNetworks();
        if (savedWifiNetworks != null) {
            for (WifiConfiguration savedWifiNetwork : savedWifiNetworks) {
//                Log.d(TAG, "Saved WiFi network: " + savedWifiNetwork.SSID + ", network id: " + savedWifiNetwork.networkId);
                if (!TextUtils.isEmpty(savedWifiNetwork.SSID)) {
                    String networkSsidNoQuotes = NetworkHelper.convertToNoQuotedString(savedWifiNetwork.SSID);
                    if (networkSsidNoQuotes.equals(ssid)) {
                        networkId = savedWifiNetwork.networkId;
                        Log.d(TAG, "Found SSID in saved networks, networkId: " + networkId);
                    }
                }
            }
        }

        if (networkId == NETWORK_NOT_FOUND)
            Log.d(TAG, "Not Found SSID in saved networks");

        return networkId;
    }

    public static int addOpenNetwork(WifiManager wifiManager, String ssid) {
        WifiConfiguration wifiConf = new WifiConfiguration();
        wifiConf.SSID = convertToQuotedString(ssid);
        wifiConf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        wifiConf.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
        return wifiManager.addNetwork(wifiConf);
    }

    public static boolean enableNetwork(WifiManager wifiManager, int networkId) {
        return enableNetwork(wifiManager, networkId, true);
    }

    public static boolean enableNetwork(WifiManager wifiManager, int networkId, boolean shouldDisconnect) {
        boolean success = false;
        if (wifiManager != null) {
            /*
             * Must call disconnect() before enableNetwork() then reconnect() after enableNetwork() to make sure
             * the Wi-Fi network is force enabled, bypass the Internet connection check of Android system.
             */
            if (shouldDisconnect) {
                wifiManager.disconnect();
            }
            success = wifiManager.enableNetwork(networkId, true);
            wifiManager.reconnect();
        }
        return success;
    }

    public static int getAddedNetworkId(Context context, String ssidNoQuotes) {
        int newCameraNwId = NETWORK_NOT_FOUND;
        if (context != null) {
            WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            if (wifiManager != null) {
                List<WifiConfiguration> updatedWifiNetworks = wifiManager.getConfiguredNetworks();
                if (updatedWifiNetworks != null) {
                    for (WifiConfiguration wifiConfiguration : updatedWifiNetworks) {
                        if (!TextUtils.isEmpty(wifiConfiguration.SSID)) {
                            String networkSsidNoQuotes = convertToNoQuotedString(wifiConfiguration.SSID);
                            if (networkSsidNoQuotes.equals(ssidNoQuotes)) {
                                newCameraNwId = wifiConfiguration.networkId;
                                break;
                            }
                        }
                    }
                }
            }
        }
        return newCameraNwId;
    }

    public static String getCurrentIpAddress(WifiManager wifiManager) {
        int ipAddress = wifiManager.getDhcpInfo() != null ? wifiManager.getDhcpInfo().ipAddress : 0;

        return ((ipAddress & 0x000000FF) ) + "." +
                ((ipAddress & 0x0000FF00) >> 8) + "." +
                ((ipAddress & 0x00FF0000) >> 16) + "." +
                ((ipAddress & 0xFF000000) >> 24);
    }

    public static boolean isConnectingToScaleWifi() {
        Context context = TePari_Application.getApplication().getApplicationContext();
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

        if (!wifiManager.isWifiEnabled())
            return false;

        String ipAddress = NetworkHelper.getCurrentIpAddress(wifiManager);
        String targetIP = Constants.getConnectionDetails(context).first;
        String ipPrefix = targetIP.substring(0, targetIP.lastIndexOf("."));
        if (ipAddress != null && ipAddress.contains(ipPrefix)) {
            return true;
        }

        return false;
    }
}
