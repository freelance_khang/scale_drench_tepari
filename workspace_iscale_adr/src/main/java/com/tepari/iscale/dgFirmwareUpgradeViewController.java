package com.tepari.iscale;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import com.tepari.tpc.BuildConfig;

import java.util.List;
import java.util.Objects;

import static android.content.Context.WIFI_SERVICE;
//import static com.crashlytics.android.Crashlytics.TAG;

/**
 * Created by 2fDesign1 on 30/11/2016.
 */

public class dgFirmwareUpgradeViewController {
    Context mContext;
    String networkSSID1 = "\"tepari_EG_upgrade\"";
    String networkSSID2 = "tepari_EG_upgrade";

    public dgFirmwareUpgradeViewController(Context context){
        mContext = context;
    }

    public Boolean currentWifiEquals_upgrade(){
        // If Build is Android 8.1 and up
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            if (isLocnEnabled(mContext) == false) {
                // If location service is not enabled, launch  a dialog
                AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
                alertDialog.setTitle("Turn on the Location Services");
                alertDialog.setMessage("Turn on the location services and add Te Pari Dosing Gun app in the App permissions Location menu");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.d("Location services", "Enable");
                                // Open location services setting
                                mContext.startActivity(new Intent(Settings.ACTION_SECURITY_SETTINGS));
                            }
                        });
                alertDialog.show();
            } else {
                ConnectivityManager connManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                if (networkInfo.isConnected()) {
                    WifiManager wifiManager = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                    WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                    String ssid = wifiInfo.getSSID();
                    String name = networkInfo.getExtraInfo();
                    if (ssid.equals(networkSSID1) || ssid.equals(networkSSID2)) {
                        return true;
                    }
                }
            }
        }
        else    // Below Android 8.1
        {
            ConnectivityManager connManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (networkInfo.isConnected()) {
                WifiManager wifiManager = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                String ssid = wifiInfo.getSSID();
                String name = networkInfo.getExtraInfo();
                if (ssid.equals(networkSSID1) || ssid.equals(networkSSID2)) {
                    return true;
                }
            }
        }

        Log.d("SSID", "no SSID!");
        return false;
    }

    public void protocolNewData(String data){
        Log.d("protocolNewData", data);
        // parse string for "Upgrade Firmware"

    }
    // Call this function only if running on Android 9+ to detect if location is enabled,
    // prior to attempting to read the WIFI SSID
    public static boolean isLocnEnabled(Context context) {
        List locnProviders = null;
        try {
            LocationManager lm =(LocationManager) context.getApplicationContext().getSystemService(Activity.LOCATION_SERVICE);
            locnProviders = lm.getProviders(true);

            return (locnProviders.size() != 0);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (BuildConfig.DEBUG) {
                if ((locnProviders == null) || (locnProviders.isEmpty()))
                    Log.d("isLocnEnabled", "Location services disabled");
                else
                    Log.d("isLocnEnabled", "locnProviders: " + locnProviders.toString());
            }
        }
        return(false);
    }
}
