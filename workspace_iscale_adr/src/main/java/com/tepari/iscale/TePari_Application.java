package com.tepari.iscale;

import android.content.Context;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.Logger;
//import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tepari.tpc.R;

import java.util.HashMap;

public class TePari_Application extends MultiDexApplication {
    //public class TePari_Application extends Application { // REMOVE ME

    private static TePari_Application sApplication;
    private static FirebaseAnalytics sFirebaseAnalytics;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    private static final String PROPERTY_ID = "UA-50289470-5";

    public enum TrackerName {
        APP_TRACKER,       // Tracker used only in this app.
        GLOBAL_TRACKER,    // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

//    HashMap<TrackerName, Tracker> mTrackers = new HashMap<>();

    public TePari_Application() {
        super();

        sApplication = this;

//        sFirebaseAnalytics = FirebaseAnalytics.getInstance(sApplication.getApplicationContext());
    }

    public static TePari_Application getApplication() {
        return sApplication;
    }

    public static FirebaseAnalytics getFirebaseAnalytics() {
        return sFirebaseAnalytics;
    }

    public static Context getAppContext() {
        return sApplication.getApplicationContext();
    }

//    public synchronized Tracker getTracker(TrackerName trackerId) {
//        if (!mTrackers.containsKey(trackerId)) {
//            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
//            analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
//            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID)
//                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
//                    : analytics.newTracker(R.xml.ecommerce_tracker);
//            t.enableAdvertisingIdCollection(true);
//            mTrackers.put(trackerId, t);
//        }
//        return mTrackers.get(trackerId);
//    }
}