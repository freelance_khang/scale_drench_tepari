package com.tepari.iscale.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.tepari.iscale.model.StreamModel;
import com.tepari.iscale.model.UploadModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by KeHo on 10/26/15.
 */
public class TepariDatabase extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 3;

    // Database Name
    private static final String DATABASE_NAME = "Tepari";

    // Contacts table name
    private static final String TABLE_TEPARI_FILE = "TepariFile";

    // FileUpload Table Columns names
    private static final String KEY_EID = "eid";//Primary Key
    private static final String KEY_FILE_NAME = "name";

    private static final String KEY_COLUMN_NAME = "column";
    private static final String KEY_COLUMN_VALUE = "column_value";

    private static final String KEY_WEIGHT = "weight";
    private static final String KEY_FIELD1 = "field1";
    private static final String KEY_FIELD2 = "field2";
    private static final String KEY_FIELD3 = "field3";
    private static final String KEY_FIELD4 = "field4";
    private static final String KEY_FIELD5 = "field5";
    private static final String KEY_FIELD6 = "field6";
    private static final String KEY_FIELD7 = "field7";
    private static final String KEY_FIELD8 = "field8";
    private static final String KEY_FIELD9 = "field9";
    private static final String KEY_FIELD10 = "field10";

    private static final String TABLE_STREAM = "TepariStream";
    private static final String KEY_ID = "id";
    private static final String KEY_TYPE = "type";//download/stream
    private static final String KEY_DATE = "date";


    public TepariDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TEPARI_FILE_TABLE = "CREATE TABLE " + TABLE_TEPARI_FILE + "("
                + KEY_EID + " TEXT PRIMARY KEY,"
                + KEY_FILE_NAME + " TEXT,"
                + KEY_COLUMN_NAME + " TEXT,"
                + KEY_COLUMN_VALUE + " TEXT,"
                + KEY_WEIGHT + " TEXT,"
                + KEY_FIELD1 + " TEXT,"
                + KEY_FIELD2 + " TEXT,"
                + KEY_FIELD3 + " TEXT,"
                + KEY_FIELD4 + " TEXT,"
                + KEY_FIELD5 + " TEXT,"
                + KEY_FIELD6 + " TEXT,"
                + KEY_FIELD7 + " TEXT,"
                + KEY_FIELD8 + " TEXT,"
                + KEY_FIELD9 + " TEXT,"
                + KEY_FIELD10 + " TEXT" + ")";
        db.execSQL(CREATE_TEPARI_FILE_TABLE);

        String CREATE_TEPARI_STREAM_TABLE = "CREATE TABLE " + TABLE_STREAM + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_COLUMN_VALUE + " TEXT,"
                + KEY_EID + " TEXT KEY,"
                + KEY_WEIGHT + " TEXT,"
                + KEY_TYPE + " TEXT,"
                + KEY_DATE + " TEXT"
                + ")";
        db.execSQL(CREATE_TEPARI_STREAM_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        Log.i("DB", "Upgrade DB");
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEPARI_FILE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STREAM);

        // Create tables again
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEPARI_FILE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STREAM);

        // Create tables again
        onCreate(db);
    }

    public void saveTepariFile(String fileName, String columnName, String columnValue, String eid) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        UploadModel uploadModel = getTepariFile(eid);
        if(uploadModel == null) {
            values.put(KEY_EID, eid);
        }

        values.put(KEY_FILE_NAME, fileName);
        values.put(KEY_COLUMN_NAME, columnName);
        values.put(KEY_COLUMN_VALUE, columnValue);

        if(uploadModel == null) {
            // Inserting Row
            db.insert(TABLE_TEPARI_FILE, null, values);
        } else {
            db.update(TABLE_TEPARI_FILE, values, KEY_EID + " = ?",
                    new String[] { eid });
        }
        //Delete Record
        db.delete(TABLE_STREAM, KEY_EID + " = ?",
                new String[]{eid});
        db.close(); // Closing database connection
    }

    // Getting single contact
    public UploadModel getTepariFile(String eid) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_TEPARI_FILE, new String[] { KEY_EID,
                        KEY_FILE_NAME, KEY_COLUMN_NAME, KEY_COLUMN_NAME, KEY_WEIGHT, KEY_FIELD1, KEY_FIELD2, KEY_FIELD3, KEY_FIELD4, KEY_FIELD5, KEY_FIELD6, KEY_FIELD7, KEY_FIELD8, KEY_FIELD9, KEY_FIELD10 }, KEY_EID + "=?",
                new String[] { eid }, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            UploadModel uploadModel = new UploadModel(cursor.getString(1), eid);
            uploadModel.setListColumnName(cursor.getString(2));
            uploadModel.setListColumnValue(cursor.getString(3));
            uploadModel.setWeight(cursor.getString(4));
            uploadModel.setField1(cursor.getString(5));
            uploadModel.setField2(cursor.getString(6));
            uploadModel.setField3(cursor.getString(7));
            uploadModel.setField4(cursor.getString(8));
            uploadModel.setField5(cursor.getString(9));
            uploadModel.setField6(cursor.getString(10));
            uploadModel.setField7(cursor.getString(11));
            uploadModel.setField8(cursor.getString(12));
            uploadModel.setField9(cursor.getString(13));
            uploadModel.setField10(cursor.getString(14));

            return uploadModel;
        }
        // return contact
        return null;
    }

    public UploadModel getTepariFileByFileName(String fileName) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_TEPARI_FILE, new String[] { KEY_EID,
                        KEY_FILE_NAME, KEY_COLUMN_NAME, KEY_COLUMN_VALUE, KEY_WEIGHT, KEY_FIELD1, KEY_FIELD2, KEY_FIELD3, KEY_FIELD4, KEY_FIELD5, KEY_FIELD6, KEY_FIELD7, KEY_FIELD8, KEY_FIELD9, KEY_FIELD10 }, KEY_FILE_NAME + "=?",
                new String[] { fileName }, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            UploadModel uploadModel = new UploadModel(cursor.getString(1), cursor.getString(0));
            uploadModel.setListColumnName(cursor.getString(2));
            uploadModel.setListColumnValue(cursor.getString(3));
            uploadModel.setWeight(cursor.getString(4));
            uploadModel.setField1(cursor.getString(5));
            uploadModel.setField2(cursor.getString(6));
            uploadModel.setField3(cursor.getString(7));
            uploadModel.setField4(cursor.getString(8));
            uploadModel.setField5(cursor.getString(9));
            uploadModel.setField6(cursor.getString(10));
            uploadModel.setField7(cursor.getString(11));
            uploadModel.setField8(cursor.getString(12));
            uploadModel.setField9(cursor.getString(13));
            uploadModel.setField10(cursor.getString(14));

            return uploadModel;
        }
        // return contact
        return null;
    }

    public void saveStreamRecord(UploadModel uploadModel, String eid, String weight) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String dateFormat = sdf.format(new Date());

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_EID, eid);
        values.put(KEY_WEIGHT, weight);
        values.put(KEY_DATE, dateFormat);
        values.put(KEY_TYPE, "stream");
        values.put(KEY_COLUMN_VALUE, uploadModel.getListColumnValue() + "," + weight + "," + dateFormat);
        db.insert(TABLE_STREAM, null, values);
        db.close(); // Closing database connection
    }

    public List<UploadModel> getTepariFileList() {
        List<UploadModel> contactList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT " + KEY_FILE_NAME + ", " + KEY_EID + " FROM " + TABLE_TEPARI_FILE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                UploadModel contact = new UploadModel(cursor.getString(0), cursor.getString(1));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }

    public List<StreamModel> getStreamModelList(String eid) {
        List<StreamModel> contactList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT eid, weight, date, column_value FROM " + TABLE_STREAM + " WHERE eid = '" + eid + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                StreamModel contact = new StreamModel();
                contact.setEid(cursor.getString(0));
                contact.setWeight(cursor.getString(1));
                contact.setDate(cursor.getString(2));
                contact.setListColumnValue(cursor.getString(3));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }

    public void deleteTepariFile(String fileName) {
        UploadModel uploadModel = getTepariFileByFileName(fileName);
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TEPARI_FILE, KEY_FILE_NAME + " = ?",
                new String[]{fileName});

        if(uploadModel != null) {
            String eid = uploadModel.getEid();

            db.delete(TABLE_STREAM, KEY_EID + " = ?",
                    new String[]{eid});
        }

        db.close();
    }
}
